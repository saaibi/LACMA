export const datailMethods = {
  1: {
    data:
      "Seleccionar las 2 cajas correspondientes a la misma dilución que presenten entre 30 y 300 unidades formadoras de colonias (UFC).Contar todas las UFC de cada caja. Hallar la media aritmética de los 2 valores y multiplicarla por el factor de dilución"
  },
  2: {
    data:
      "Si las cajas de dos diluciones consecutivas presentan recuentos menores de 30 y mayores de 300 UFC, contar las cuatro cajas, calcular el recuento para cada una de las diluciones y sacar el promedio entre las dos."
  },
  3: {
    data:
      "Si el número de UFC de las placas de dos diluciones consecutivas están dentro del rango de 30 y 300 unidades formadoras de colonias, computar el recuento para cada una de las diluciones e informar el promedio de los dos valores obtenidos, si al relacionar el número mayor sobre el número menor da como resultado menor 2 se reporta el promedio de los dos valores o si al relacionar el número mayor sobre el número menor da como resultado mayor o igual a 2 se reporta el menor valor."
  },
  4: {
    data:
    "Si ninguna de las placas tiene entre 30 y 300 UFC sino conteos superiores a 300 UFC las cajas se dividen en secciones radiales de 2, 4, 6 u 8 partes y se cuenta una o más secciones. Luego se promedia y se multiplica por el factor adecuado, con el fin de conseguir un número estimado de UFC presentes. Se halla la media aritmética entre las dos placas, se multiplica por el factor de dilución y se registra como un recuento estimado. "
  },
  5: {
    data:
    "Si no hay UFC en las cajas correspondiente a la dilución de mayor concentración, informar el recuento como menor de 1 multiplicando por el factor de dilución más concentrada. "
  },
  6: {
    data:
    "Si al relacionar el número mayor sobre el número menor da como resultado mayor o igual a 2 se reporta el menor valor"
  },
  7: {
    data:
      "Si en las placas de la dilución menos concentrada se encuentran más de 200 UFC en una sección correspondiente a una octava parte (1/8) de la caja, multiplicar 200 x 8 = 1.600 y éste valor por el inverso de la dilución y expresar el valor como superior al recuento estimado."
  }
};


export const colourOptions = [
  { value: 'ocean', label: 'Ocean', color: '#00B8D9', isFixed: true },
  { value: 'blue', label: 'Blue', color: '#0052CC', isDisabled: true },
  { value: 'purple', label: 'Purple', color: '#5243AA' },
  { value: 'red', label: 'Red', color: '#FF5630', isFixed: true },
  { value: 'orange', label: 'Orange', color: '#FF8B00' },
  { value: 'yellow', label: 'Yellow', color: '#FFC400' },
  { value: 'green', label: 'Green', color: '#36B37E' },
  { value: 'forest', label: 'Forest', color: '#00875A' },
  { value: 'slate', label: 'Slate', color: '#253858' },
  { value: 'silver', label: 'Silver', color: '#666666' },
];