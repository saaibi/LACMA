import React from 'react';
import _ from 'lodash';
import { Route, Redirect } from 'react-router-dom';

export const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={props => {
      const userLocal = localStorage.getItem('user');
      if (userLocal) {
        const auth = rest.rols
          ? _.includes(
              _.isArray(rest.rols) ? rest.rols : [rest.rols],
              JSON.parse(userLocal).rol
            )
          : true;
        return auth ? (
          <Component {...props} />
        ) : (
          <Redirect to={{ pathname: '/', state: { from: props.location } }} />
        );
      }
      return (
        <Redirect
          to={{ pathname: '/login', state: { from: props.location } }}
        />
      );
    }}
  />
);
