import { defineAction } from '../utils/defineAction';
import { REQUEST, SUCCESS, FAILURE } from './commonStates';

const accionsHttp = [REQUEST, SUCCESS, FAILURE];

export const METHOD_GETALL = defineAction('METHOD_GETALL', accionsHttp);
export const METHOD_GETBYID = defineAction('METHOD_GET', accionsHttp);
export const METHOD_CREATE = defineAction('METHOD_CREATE', accionsHttp);
export const METHOD_UPDATE = defineAction('METHOD_UPDATE', accionsHttp);
export const METHOD_DELETE = defineAction('METHOD_DELETE', accionsHttp);
