import { defineAction } from '../utils/defineAction';
import { REQUEST, SUCCESS, FAILURE } from './commonStates';

const accionsHttp = [REQUEST, SUCCESS, FAILURE];

export const RESULT_CREATE_PDF = defineAction('RESULT_CREATE_PDF', accionsHttp);
