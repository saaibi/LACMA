import React from "react";
import { render } from "react-dom";
import { Provider } from "react-redux";
import moment from "moment";
import { createMuiTheme, ThemeProvider } from "@material-ui/core";
import { teal, blue } from "@material-ui/core/colors";

import "materialize-css/dist/css/materialize.css";
import "materialize-css/dist/js/materialize.min.js";
import "../style/index.scss";

import App from "./App";
import store from "./store";

moment.locale("es");

// setup fake backend
import { configureFakeBackend } from "./utils/fake-backend";
configureFakeBackend();

const theme = createMuiTheme({
  palette: {
    primary: teal,
    secondary: blue
  }
});

const AppLPA = () => (
  <Provider store={store}>
    <ThemeProvider theme={theme}>
      <App />
    </ThemeProvider>
  </Provider>
);

render(<AppLPA />, document.getElementById("app"));
