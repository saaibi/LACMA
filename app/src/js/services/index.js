import axios from 'axios';

import { apiUrl } from '../utils/http'

export async function makeRequestAsync(sufix = "/", method = "GET", payload = {}, headers = false) {
    try {

        const general = { method };

        if (method === "POST" || method === "PUT") {
            general.method = method;
            general.data = payload;
        }

        if (method === "DELETE" && !_.isEmpty(payload) ) {
            general.data = payload;
        }

        if (headers) {
            general.headers = { 'content-type': 'multipart/form-data' };
        }

        general.config = { headers: { "Content-Type": "application/json" } };

        const url = `${apiUrl}${sufix}`
        const response = await axios(url, general);
        if (response.data.error) {
            console.error(response)
            throw response.data.error;
        }
        return response;
    } catch (error) {
        console.error(error);
        return Promise.reject(error);
    }
}