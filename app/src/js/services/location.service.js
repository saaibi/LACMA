export const onClickBTN = () => {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(
      p => geoSuccess(p),
      () => console.log
    );
  } else {
    console.error('Geolocation is not supported by this browser.');
  }
};

const geoSuccess = position => {
  const {
    coords: { latitude, longitude }
  } = position;
  codeLatLng(latitude, longitude);
};

const codeLatLng = (lat, lng) => {
  const geocoder = new google.maps.Geocoder();
  const latlng = new google.maps.LatLng(lat, lng);

  geocoder.geocode({ latLng: latlng }, (results, status) => {
    if (status == google.maps.GeocoderStatus.OK) {
      if (results[1]) {
        const address = results[0].formatted_address;
        console.log('address = ' + address);
        const value = address.split(',');
        const count = value.length;
        console.log('country name is: ' + value[count - 1]);
        console.log('state name is: ' + value[count - 2]);
        console.log('city name is: ' + value[count - 3]);
      } else {
        console.log('No results found');
      }
    } else {
      console.log('Geocoder failed due to: ' + status);
    }
  });
};
