const initialState = {
    version: '',
    analyst: '',
    coordinator: '',
    typeSample: '',
    responsibleSample: null,
    quantity: '',
    lote: '',
    typeContainer: '',
    color: '',
    smell: '',
    appearance: '',
    temperature: '',
    manufacturingDate: null,
    expirationDate: null,
    receiptDate: null,
    startDateAnalysis: null,
    endDateAnalysis: null,
    reportDate: null,
    agreedDeliveryDate: null,
    deliveryDate: null,
    takeSampleDate: null,
    products: null,
    client: null
}


export default initialState;