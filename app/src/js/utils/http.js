// const server = 'http://localhost:3000';
const server = 'https://laboratorio-lacma.herokuapp.com';
const apiPath = '/api';

export const apiUrl = `${server}${apiPath}`;
