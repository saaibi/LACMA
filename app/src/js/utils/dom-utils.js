import React from 'react';
import moment from 'moment';
import colHoly from 'colombia-holidays';

export const screenSize = () => {
  let w = window,
    d = document,
    documentElement = d.documentElement,
    body = d.querySelector('body'),
    width = w.innerWidth || documentElement.clientWidth || body.clientWidth,
    height = w.innerHeight || documentElement.clientHeight || body.clientHeight;

  return { width, height };
};

export const getElementSize = elem => {
  if (elem) {
    let width =
        document.getElementById(elem).offsetWidth ||
        elem.innerWidth ||
        elem.clientWidth,
      height =
        document.getElementById(elem).offsetHeight ||
        elem.innerHeight ||
        elem.clientHeight;

    return { width, height };
  }
};

export const scrollToTop = (scroll_duration = 800) => {
  let time_interval = 15;
  let scroll_step = -window.scrollY / (scroll_duration / time_interval);

  let scroll_interval = setInterval(() => {
    if (window.scrollY !== 0) {
      window.scrollBy(0, scroll_step);
    } else clearInterval(scroll_interval);
  }, time_interval);
};

export const fireClick = node => {
  if (document.createEvent) {
    const evt = document.createEvent('MouseEvents');
    evt.initEvent('click', true, false);
    node.dispatchEvent(evt);
  } else if (document.createEventObject) {
    node.fireEvent('onclick');
  } else if (typeof node.onclick === 'function') {
    node.onclick();
  }
};

export const isNumber = value =>
  !Number.isNaN(Number(value)) && Number.isFinite(Number(value)) ? true : false;

export const openModal = id => {
  $(`#${id}`).modal('open');
};

export const closeModal = id => {
  $(`#${id}`).modal('close');
};

export const toast = data => M.toast({ classes: 'rounded', ...data });

let dates = [];
let year = '';
export const holiday = date => {
  if (_.isEmpty(dates) || moment(date).year() !== year) {
    dates = colHoly.getColombiaHolidaysByYear(moment(date).year());
    year = moment(date).year();
  }
  return _.map(dates, date =>
    moment(date.holiday).format('DD/MM/YYYY')
  ).includes(moment(date).format('DD/MM/YYYY'));
};

export const formSwal = `<div class="row">
<div class="file-field input-field">
    <div class="btn">
        <span>Archivo</span>
        <input accept=".xlsx" id="fileName" type="file">
    </div>
    <div class="file-path-wrapper">
        <input accept=".xlsx" placeholder="ej.xlsx" class="file-path validate" type="text">
        <span class="helper-text">Archivo debe tener una pestaña (Base de datos)</span>
    </div>
</div>
<div class="input-field">
    <label>
        <input id="saveData" type="checkbox" />
        <span>Guardar Base de Datos</span>
      </label>
</div>
</div>`;

export const formSwal2 = `<div class="row">
<div class="input-field">
      <i class="material-icons prefix">mode_edit</i>
      <textarea id="conclusion_id" class="materialize-textarea"></textarea>
      <label for="conclusion_id" class="active">Conclusión</label>
    </div>
<div class="input-field">
      <i class="material-icons prefix">sort</i>
      <textarea id="suggestions_id" class="materialize-textarea"></textarea>
      <label for="suggestions_id" class="active">Sugerencias</label>
    </div>
</div>`;

export const errors = error => _.map(error.errors, 'message').join('</br>')|| error.message || error;
