import Swal from 'sweetalert2/dist/sweetalert2';
import { formSwal2 } from './dom-utils';

export const swalAlert = alert =>
  Swal.mixin(alert.mixin || {}).fire(alert.options || alert);

export const swalAlert2 = alert =>
  new Promise(resolve => {
    Swal.mixin(alert.mixin || {})
      .fire(alert.options || alert)
      .then(result => {
        resolve({ ...result, alert, Swal }); // ¡Todo salió bien!
      });
  });

export const alertConSug = data => ({
  title: 'Conclusión y Sugerencias',
  html: formSwal2,
  focusConfirm: false,
  showCloseButton: true,
  confirmButtonText: 'Guardar',
  onRender: e => {
    document.getElementById('conclusion_id').value = data.conclusion || '';
    document.getElementById('suggestions_id').value = data.sugerencias || '';
  },
  preConfirm: () => ({
    conclusion: document.getElementById('conclusion_id').value,
    suggestions: document.getElementById('suggestions_id').value
  })
});

export const alertDelete = () => ({
  title: '¿Estás seguro?',
  text: '¡No podrás revertir esto!',
  icon: 'warning',
  confirmButtonText: '¡Sí, elimínelo!',
  cancelButtonText: 'No, cancelar!',
  showCloseButton: true,
  showCancelButton: true,
  reverseButtons: true
});
