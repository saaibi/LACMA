import React from "react";

import Input from "../../../../Common/Input";

const Fields = props => (
  <div>
    <Input
      id="caja_1"
      name="caja1"
      text="Caja 1"
      type="number"
      autoFocus={true}
      className="col s12 m6"
      icon="dashboard"
      classNameIcon="prefix"
      value={props.data.caja1}
      classNameLabel={props.data.caja1 ? "active" : ""}
      onChange={props.handleChange}
    />

    <Input
      id="caja_2"
      name="caja2"
      text="Caja 2"
      type="number"
      className="col s12 m6"
      icon="dashboard"
      classNameIcon="prefix"
      value={props.data.caja2}
      classNameLabel={props.data.caja2 ? "active" : ""}
      onChange={props.handleChange}
    />

  </div>
);

export default Fields;
