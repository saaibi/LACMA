import React from 'react';

import Fields from './Partials/Fields';

const Form = (props) => (
	<div className="row">
		<Fields {...props} />
	</div>
);

export default Form;
