import React from "react";

import Input from "../../../../../Common/Input";

const Form = props => (
  <div className="row">
    <Input
      id="dilucion_01"
      name="caja1"
      text="Dilución 1"
      type="number"
      autoFocus={true}
      className="col s12 m4"
      icon="ac_unit"
      classNameIcon="prefix"
      classNameLabel={props.data.caja1 ? "active" : ""}
      value={props.data.caja1}
      onChange={e => props.handleChange(e, "exampleTwo")}
    />

    <Input
      id="dilucion_02"
      name="caja2"
      text="Dilución 2"
      type="number"
      className="col s12 m4"
      icon="ac_unit"
      classNameIcon="prefix"
      classNameLabel={props.data.caja2 ? "active" : ""}
      value={props.data.caja2}
      onChange={e => props.handleChange(e, "exampleTwo")}
    />

    <Input
      disabled
      id="dilution_factor_001"
      name="dilutionFactor1"
      text="Factor de dilución"
      type="number"
      defaultValue={1}
      className="col s12 m4"
      icon="looks_one"
      classNameIcon="prefix"
      onChange={e => props.handleChange(e, "exampleTwo")}
    />

    <Input
      id="dilucion_03"
      name="caja3"
      text="Dilución 1"
      type="number"
      autoFocus={true}
      className="col s12 m4"
      icon="ac_unit"
      classNameIcon="prefix"
      value={props.data.caja3}
      onChange={e => props.handleChange(e, "exampleTwo")}
    />

    <Input
      id="dilucion_04"
      name="caja4"
      text="Dilución 2"
      type="number"
      className="col s12 m4"
      icon="ac_unit"
      classNameIcon="prefix"
      value={props.data.caja4}
      onChange={e => props.handleChange(e, "exampleTwo")}
    />

    <Input
      id="dilution_factor_002"
      name="dilutionFactor2"
      text="Factor de dilución"
      type="number"
      disabled
      defaultValue={2}
      className="col s12 m4"
      icon="looks_two"
      classNameIcon="prefix"
      onChange={e => props.handleChange(e, "exampleTwo")}
    />
  </div>
);

export default Form;
