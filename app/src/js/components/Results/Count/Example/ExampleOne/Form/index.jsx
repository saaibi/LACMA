import React from "react";

import Input from "../../../../../Common/Input";

const Form = props => (
  <div className="row">
    <Input
      id="caja_03"
      name="caja1"
      text="Caja 1"
      type="number"
      className="col s12 m4"
      icon="filter_1"
      classNameIcon="prefix"
      classNameLabel={props.data.caja1 ? "active" : ""}
      value={props.data.caja1}
      onChange={props.handleChange}
    />

    <Input
      id="caja_04"
      name="caja2"
      text="Caja 2"
      type="number"
      className="col s12 m4"
      icon="filter_2"
      classNameIcon="prefix"
      classNameLabel={props.data.caja2 ? "active" : ""}
      value={props.data.caja2}
      onChange={props.handleChange}
    />

    <Input
      disabled
      id="dilution_factor_005"
      name="dilutionFactor1"
      text="Factor de dilución"
      type="number"
      defaultValue={1}
      className="col s12 m4"
      icon="looks_one"
      classNameIcon="prefix"
      onChange={e => props.handleChange(e, "exampleTwo")}
    />
  </div>
);

export default Form;
