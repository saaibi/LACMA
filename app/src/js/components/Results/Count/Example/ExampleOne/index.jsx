import React, { Fragment } from "react";
import Form from "./Form";
import Panel from "../../../../Common/Panel";

const ExampleOne = props => {
  return (
    <Fragment>
      <Form {...props} />
      <Panel
        title="Resultado"
        panelContent={
          <span>
            <p>
              <b>Media aritmética: </b>
              {`(${props.data.caja1} + ${props.data.caja2} / 2) X 10 `}
            </p>
            <p>
              <b>Reporte total: </b> {props.result} UFC/g
            </p>
          </span>
        }
      />
    </Fragment>
  );
};

export default ExampleOne;

{
  /* <span class="fraction">
<span class="numerator">
  <span class="block">
    <span class="paren">(</span>
    <span class="block" >
      <span>3</span>
      <var>x</var>
      <span class="binary-operator">+</span>
      <span>9</span>
    </span>
    <span class="paren">)</span>
  </span>
</span>
<span class="denominator hasCursor">
  <span class="block">
    <span class="paren">(</span>
    <span class="block">
      <span>2</span>
      <span class="binary-operator">−</span>
      <var>x</var>
    </span>
    <span class="paren">)</span>
  </span>
  <sup class="">
    <span>2</span>
  </sup>
  <span class="cursor">​</span>
</span>
<span>&nbsp;</span>
</span> */
}
