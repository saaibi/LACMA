import React, { Fragment } from 'react';
import Form from './Form';
import Panel from '../../../../Common/Panel';

const ExampleOne = props => {
  const relacion = parseFloat(
    (props.resultCaja2 / props.resultCaja1 || 0).toFixed(2)
  );
  return (
    <Fragment>
      <Form {...props} />
      <Panel
        title="Resultado"
        panelContent={
          <span>
            <p>
              <b>Dilución 1: </b>
              {`(${props.data.caja1} x ${props.data.seccionRadial1}) = `}
              {props.data.caja1 * props.data.seccionRadial1}
            </p>
            <p>
              <b>Dilución 2: </b>
              {`(${props.data.caja2} x ${props.data.seccionRadial2}) = `}
              {props.data.caja2 * props.data.seccionRadial2}
            </p>
            <p>
              <b>Media aritmética: </b>
              {`(${props.data.caja1 * props.data.seccionRadial1 || 0} + ${props
                .data.caja2 * props.data.seccionRadial2 || 0} / 2) X 1000 `}
            </p>
            <p>
              <b>Recuento estimado: </b>
              {((props.data.caja1 * props.data.seccionRadial1 +
                props.data.caja2 * props.data.seccionRadial2) /
                2) *
                1000 || 0}{' '}
              UFC/g
            </p>
          </span>
        }
      />
    </Fragment>
  );
};

export default ExampleOne;
