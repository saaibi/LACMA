import React from "react";

import Input from "../../../../../Common/Input";
import Select from "../../../../../Common/Select";

const Form = props => (
  <div className="row">
    <Input
      id="dilucion_05"
      name="caja1"
      text="Dilución 1"
      type="number"
      autoFocus={true}
      className="col s12 m6"
      icon="ac_unit"
      classNameIcon="prefix"
      classNameLabel={props.data.caja1 ? "active" : ""}
      value={props.data.caja1}
      onChange={e => props.handleChange(e, "exampleTwo")}
    />
    <Select
      id="seccione_radial_01"
      name="seccionRadial1"
      label="Seccion radial"
      icon="blur_off"
      className="col s12 m6"
      classNameIcon="prefix"
      defaultText="Selecciona"
      defaultValue={"1"}
      onChange={e => props.handleSelect(e)}
      items={[
        { _id: "2", name: "1/2" },
        { _id: "4", name: "1/4" },
        { _id: "6", name: "1/6" },
        { _id: "8", name: "1/8" }
      ]}
    />
    <Input
      id="dilucion_06"
      name="caja2"
      text="Dilución 2"
      type="number"
      className="col s12 m6"
      icon="ac_unit"
      classNameIcon="prefix"
      classNameLabel={props.data.caja2 ? "active" : ""}
      value={props.data.caja2}
      onChange={e => props.handleChange(e, "exampleTwo")}
    />
    <Select
      id="seccione_radial_02"
      name="seccionRadial2"
      label="Seccion radial"
      icon="blur_off"
      className="col s12 m6"
      classNameIcon="prefix"
      defaultText="Selecciona"
      defaultValue={"1"}
      onChange={e => props.handleSelect(e)}
      items={[
        { _id: "2", name: "1/2" },
        { _id: "4", name: "1/4" },
        { _id: "6", name: "1/6" },
        { _id: "8", name: "1/8" }
      ]}
    />
    <Input
      disabled
      id="dilution_factor_003"
      name="dilutionFactor1"
      text="Factor de dilución"
      type="number"
      defaultValue={3}
      className="col s12"
      icon="looks_one"
      classNameIcon="prefix"
      onChange={e => props.handleChange(e, "exampleTwo")}
    />
  </div>
);

export default Form;
