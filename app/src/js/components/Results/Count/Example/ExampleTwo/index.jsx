import React, { Fragment } from "react";
import Form from "./Form";
import Panel from "../../../../Common/Panel";

const ExampleOne = props => {
  return (
    <Fragment>
      <Form {...props} />
      <Panel
        title="Resultado Caja 1y2"
        panelContent={
          <span>
            <p>
              <b>Media aritmética: </b>
              {`(${props.data.caja1} + ${props.data.caja2} / 2) X 10 `}
            </p>
            <p>
              <b>Reporte: </b> {props.resultCaja1 || 0} UFC/g
            </p>
          </span>
        }
      />
      <Panel
        title="Resultado Caja 3y4"
        panelContent={
          <span>
            <p>
              <b>Media aritmética: </b>
              {`(${props.data.caja3 || 0} + ${props.data.caja4 ||
                0} / 2) X 100 `}
            </p>
            <p>
              <b>Reporte: </b> {props.resultCaja2 || 0} UFC/g
            </p>
          </span>
        }
      />
      <Panel
        title="Total"
        panelContent={
          <span>
            <p>
              <b>Media aritmética: </b>
              {`(${props.resultCaja1 || 0} + ${props.resultCaja2 || 0} / 2) `}
            </p>
            <p>
              <b>Reporte total: </b>
              {(props.resultCaja1 + props.resultCaja2) / 2} UFC / g o mL
            </p>
          </span>
        }
      />
    </Fragment>
  );
};

export default ExampleOne;
