import React from "react";

import Input from "../../../../../Common/Input";

const Form = props => (
  <div className="row">
    <Input
      id="caja_05"
      name="caja1"
      text="Caja 1"
      type="number"
      autoFocus={true}
      className="col s12 m4"
      icon="filter_1"
      classNameIcon="prefix"
      classNameLabel={props.data.caja1 ? "active" : ""}
      value={props.data.caja1}
      onChange={e => props.handleChange(e, "exampleTwo")}
    />

    <Input
      id="caja_06"
      name="caja2"
      text="Caja 2"
      type="number"
      className="col s12 m4"
      icon="filter_2"
      classNameIcon="prefix"
      classNameLabel={props.data.caja2 ? "active" : ""}
      value={props.data.caja2}
      onChange={e => props.handleChange(e, "exampleTwo")}
    />

    <Input
      disabled
      id="dilution_factor_001"
      name="dilutionFactor1"
      text="Factor de dilución"
      type="number"
      defaultValue={1}
      className="col s12 m4"
      icon="looks_one"
      classNameIcon="prefix"
      onChange={e => props.handleChange(e, "exampleTwo")}
    />

    <Input
      id="caja_07"
      name="caja3"
      text="Caja 3"
      type="number"
      autoFocus={true}
      className="col s12 m4"
      icon="filter_3"
      classNameIcon="prefix"
      value={props.data.caja3}
      onChange={e => props.handleChange(e, "exampleTwo")}
    />

    <Input
      id="caja_08"
      name="caja4"
      text="Caja 4"
      type="number"
      className="col s12 m4"
      icon="filter_4"
      classNameIcon="prefix"
      value={props.data.caja4}
      onChange={e => props.handleChange(e, "exampleTwo")}
    />

    <Input
      id="dilution_factor_002"
      name="dilutionFactor2"
      text="Factor de dilución"
      type="number"
      disabled
      defaultValue={2}
      className="col s12 m4"
      icon="looks_two"
      classNameIcon="prefix"
      onChange={e => props.handleChange(e, "exampleTwo")}
    />
  </div>
);

export default Form;
