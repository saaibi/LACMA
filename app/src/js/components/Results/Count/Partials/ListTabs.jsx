import React from "react";

const ListTabs = ({ setMethod }) => {
  return (
    <ul className="tabs tabs-fixed-width">
      <li className="tab">
        <a href="#test1" onClick={() => setMethod(1)}>
          Ejemplo 1
        </a>
      </li>
      <li className="tab">
        <a href="#test2" onClick={() => setMethod(2)}>
          Ejemplo 2
        </a>
      </li>
      <li className="tab">
        <a href="#test3" onClick={() => setMethod(3)}>
          Ejemplo 3
        </a>
      </li>
      <li className="tab">
        <a href="#test4" onClick={() => setMethod(4)}>
          Ejemplo 4
        </a>
      </li>
      <li className="tab">
        <a href="#test5" onClick={() => setMethod(5)}>
          Ejemplo 5
        </a>
      </li>
    </ul>
  );
};

export default ListTabs;
