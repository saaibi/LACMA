import React, { Fragment, PureComponent } from 'react';
import { Collapsible } from 'react-materialize';
import inRange from 'lodash/inRange';

import { isNumber } from '../../../utils/dom-utils';
import { datailMethods } from '../../../data/select';

import CollapsibleItem from '../../Common/Collapsible/CollapsibleItems';
import ExampleOne from './Example/ExampleOne';
import ExampleTwo from './Example/ExampleTwo';
import ExampleThree from './Example/ExampleThree';
import ExampleFour from './Example/ExampleFour';
import Form from './Form';
import ListTabs from './Partials/ListTabs';

class ReCount extends PureComponent {
  state = {
    method: 1,
    caja1: '',
    caja2: '',
    caja3: '',
    caja4: '',
    seccionRadial1: '',
    seccionRadial2: ''
  };

  componentDidMount() {
    const instance = M.Tabs.init(document.querySelector('.tabs'), {});
    instance.updateTabIndicator();
  }

  handleChange = e => {
    e.preventDefault();
    const { value, name } = e.target;
    const number = isNumber(parseInt(value)) ? parseInt(value) : '';
    this.setState({ [name]: number });
    this.determineMethod(name, number);
  };

  static getDerivedStateFromProps(props, state) {
    const { caja1, caja2, caja3, caja4 } = state;
    return {
      resultCaja1: ((caja1 + caja2) / 2) * 10,
      resultCaja2: ((caja3 + caja4) / 2) * 100,
      result: ((caja1 + caja2) / 2) * 10
    };
  }

  handleChange2 = (e, type) => {
    e.preventDefault();
    const { value, name } = e.target;
    this.setState({
      [name]: isNumber(parseInt(value)) ? parseInt(value) : ''
    });
  };

  handleSelect = e => {
    e.preventDefault();
    const { value, name } = e.target;
    this.setState({
      [name]: isNumber(parseInt(value)) ? parseInt(value) : ''
    });
  };

  determineMethod = (name, value) => {
    const instance = M.Tabs.getInstance(document.querySelector('.tabs'));
    let { caja1, caja2 } = this.state;
    switch (name) {
      case 'caja1':
        caja1 = value;
        break;
      case 'caja2':
        caja2 = value;
        break;
      default:
        break;
    }

    if (this.isBetween(caja1) && this.isBetween(caja2)) {
      instance.select('test1');
      this.setState({ result: ((caja1 + caja2) / 2) * 10 });
    } else if (
      (this.lessThan30(caja1) && this.lessThan30(caja2)) ||
      (this.moreThan300(caja1) && this.moreThan300(caja2))
    ) {
      instance.select('test2');
    }

    // console.log(parseInt(caja1));
  };

  isBetween = num => inRange(parseInt(num), 30, 300);
  lessThan30 = num => parseInt(num) < 30;
  moreThan300 = num => parseInt(num) > 300;

  setMethod = num => {
    this.setState({ method: num });
  };

  render() {
    const {
      method,
      result,
      resultCaja1,
      resultCaja2,
      seccionRadial1,
      seccionRadial2,
      caja1,
      caja2,
      caja3,
      caja4
    } = this.state;
    return (
      <Fragment>
        <Collapsible popout>
          <CollapsibleItem
            header="Determinar qué método utilizar"
            icon="assignment"
            content={
              <Form data={{ caja1, caja2 }} handleChange={this.handleChange} />
            }
          />
        </Collapsible>

        <div className="card">
          <div className="card-content">
            <span className="card-title">Ejemplo {method}</span>
            {<p>{datailMethods[method].data}</p>}
          </div>
          <div className="card-tabs">
            <ListTabs setMethod={this.setMethod} />
          </div>
          <div className="card-content grey lighten-4">
            <div id="test1">
              <ExampleOne
                result={result}
                data={{ caja1, caja2 }}
                handleChange={this.handleChange2}
              />
            </div>
            <div id="test2">
              <ExampleTwo
                result={result}
                resultCaja1={resultCaja1}
                resultCaja2={resultCaja2}
                data={{ caja1, caja2, caja3, caja4 }}
                handleChange={this.handleChange2}
              />
            </div>
            <div id="test3">
              <ExampleThree
                result={result}
                resultCaja1={resultCaja1}
                resultCaja2={resultCaja2}
                data={{ caja1, caja2, caja3, caja4 }}
                handleChange={this.handleChange2}
              />
            </div>
            <div id="test4">
              <ExampleFour
                data={{
                  caja1,
                  caja2,
                  caja3,
                  caja4,
                  seccionRadial1,
                  seccionRadial2
                }}
                handleChange={this.handleChange2}
                handleSelect={this.handleSelect}
              />
            </div>
            <div id="test5">
              <p>
                <b>Reportar el recuento como: </b>
                Menor de 10 UFC / g o mL ({'<'} 10 UFC/gó mL), o menor de 1 X
                10^1 UFC / g o mL ({'<'} 1 X 10^1 UFC / g o mL).
              </p>
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
}

export default ReCount;
