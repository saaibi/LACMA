import React from 'react';

import { StyleSheet, Image, View } from '@react-pdf/renderer';

import logo from '../../../../../public/images/logo.jpg';

const styles = StyleSheet.create({
  container: {},
  image: {
    marginRight: 40,
    bottom: 5
  }
});

export default props => (
  <View {...props} style={styles.container}>
    <Image src={logo} style={styles.image} />
  </View>
);
