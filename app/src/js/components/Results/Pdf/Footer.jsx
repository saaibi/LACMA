import React from 'react';

import { StyleSheet, Image, View } from '@react-pdf/renderer';

import footer from '../../../../../public/images/footer.jpg';

const styles = StyleSheet.create({
  container: {},
  image: {
    width: '100%',
  },
  imgFooter: {
    position: 'absolute',
    right: 0,
    left: 0,
    bottom: 0
  },
  footer: {
    position: 'absolute',
    fontSize: 12,
    bottom: 25,
    left: 35,
    right: 0,
    textAlign: 'center',
    color: 'grey',
    fontFamily: 'Times-Roman'
  }
});

export default props => (
  <View {...props} style={styles.container}>
    <Image src={footer} style={styles.image} />
  </View>
);
