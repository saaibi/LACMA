import React, { PureComponent } from 'react';
import _ from 'lodash';
import moment from 'moment';
import { Document, Page, View, Image } from '@react-pdf/renderer';

import Header from './Header';
import Footer from './Footer';

import logo from '../../../../../public/images/logolacma.jpg';
import {
  styles,
  Item,
  Item2,
  Item3,
  Pages,
  Signature,
  Subtitle,
  SubtitleTwo,
  ViewMethods
} from './Content';

const IN = 'INFORME DE RESULTADOS',
  C = 'CONCLUSION',
  R = 'REFERENCIA',
  S = 'SUGERENCIA',
  CA = 'CARACTERÍSTICAS ORGANOLÉPTICAS';

class MyDocument extends PureComponent {
  static defaultProps = {
    signature: {}
  };

  state = {
    dateToday: ''
  };

  componentDidMount() {
    const date = moment(Date.now());
    this.setState({
      dateToday: `${date.date()} de ${date.format('MMMM')} de ${date.year()}`
    });
  }

  render() {
    const { dateToday } = this.state;
    const { sample, dataCS } = this.props;
    const {
      signature: { nameA, nameC, firmaA, firmaC }
    } = this.props;
    const showSample = { ...sample };
    const showClient = { ...sample.client };
    const showProduct = { ...sample.products };
    const responsibleSample =
      sample.responsibleSample &&
      `${sample.responsibleSample.firstName} ${sample.responsibleSample.lastName}`;
    return (
      <Document
        title="LAC-PS-FR-033"
        author="LACMA"
        keywords="LACMA, resultados, laboratorio"
        subject="Resultados de la muestra"
      >
        <Page size="LETTER" style={styles.page}>
          <Header />
          <View style={{ ...styles.container }}>
            {/* Registro */}
            <Image
              src={logo}
              style={{ ...styles.text, width: '20%', borderBottom: 0 }}
            />
            <Subtitle style={{ width: '80%', paddingTop: 15, borderBottom: 0 }}>
              {IN}
            </Subtitle>
            <Item style={{ width: '20%' }}>CODIGO:</Item>
            <Item style={{ width: '16%' }}>{showSample.code}</Item>
            <Item style={{ width: '16%' }}>VERSIÓN:</Item>
            <Item style={{ width: '16%' }}>{showSample.version}</Item>
            <Item style={{ width: '16%' }}>PÁGINA</Item>
            <Item style={{ width: '16%' }}>{'  '}</Item>
            <Pages
              style={{
                width: '100%',
                textAlign: 'right',
                paddingRight: 34,
                marginTop: -16,
                border: 0
              }}
            />
            <Signature style={{ width: '100%' }} fixed />

            <SubtitleTwo style={{ width: '66.668%', border: 0 }} />
            <SubtitleTwo style={{ width: '16.666%', border: 0 }}>
              {R}
            </SubtitleTwo>
            <Item style={{ width: '16.666%', marginVertical: 10 }}>{}</Item>

            {/* Cliente */}
            <Item2 style={{ width: '36%', borderBottom: 0 }}>
              Empresa Solicitante
            </Item2>
            <Item2 style={{ width: '16%', borderBottom: 0 }}>NIT</Item2>
            <Item2 style={{ width: '48%', borderBottom: 0 }}>Contacto</Item2>
            <Item2 style={{ width: '36%', borderTop: 0 }}>
              {showClient.company || '  '}
            </Item2>
            <Item2 style={{ width: '16%', borderTop: 0 }}>
              {showClient.nit}
            </Item2>
            <Item2 style={{ width: '48%', borderTop: 0 }}>
              {showClient.contact}
            </Item2>
            <Item2 style={{ width: '36%', borderBottom: 0, borderTop: 0 }}>
              Dirección
            </Item2>
            <Item2 style={{ width: '16%', borderBottom: 0, borderTop: 0 }}>
              Teléfono
            </Item2>
            <Item2 style={{ width: '16%', borderBottom: 0, borderTop: 0 }}>
              Fax
            </Item2>
            <Item2 style={{ width: '32%', borderBottom: 0, borderTop: 0 }}>
              E-mail
            </Item2>
            <Item2 style={{ width: '36%', borderTop: 0 }}>
              {showClient.address || '  '}
            </Item2>
            <Item2 style={{ width: '16%', borderTop: 0 }}>
              {showClient.phone}
            </Item2>
            <Item2 style={{ width: '16%', borderTop: 0 }}>
              {showClient.fax}
            </Item2>
            <Item2 style={{ width: '32%', borderTop: 0 }}>
              {showClient.email}
            </Item2>

            {/* Fechas */}
            <Item2 style={{ width: '20%', borderBottom: 0, borderTop: 0 }}>
              Fecha y Hora de recibo
            </Item2>
            <Item2 style={{ width: '16%', borderBottom: 0, borderTop: 0 }}>
              Fecha y Hora inicio de Análisis
            </Item2>
            <Item2 style={{ width: '16%', borderBottom: 0, borderTop: 0 }}>
              Fecha y Hora final de Análisis
            </Item2>
            <Item2 style={{ width: '16%', borderBottom: 0, borderTop: 0 }}>
              Fecha elaboración de informe
            </Item2>
            <Item2 style={{ width: '16%', borderBottom: 0, borderTop: 0 }}>
              Fecha pactada de entrega
            </Item2>
            <Item2 style={{ width: '16%', borderBottom: 0, borderTop: 0 }}>
              Fecha de entrega
            </Item2>
            <Item2 style={{ width: '20%', borderTop: 0 }}>
              {showSample.receiptDate &&
                moment(showSample.receiptDate).format('DD/MM/YYYY HH:mm')}
            </Item2>
            <Item2 style={{ width: '16%', borderTop: 0 }}>
              {showSample.startDateAnalysis &&
                moment(showSample.startDateAnalysis).format('DD/MM/YYYY HH:mm')}
            </Item2>
            <Item2 style={{ width: '16%', borderTop: 0 }}>
              {showSample.endDateAnalysis &&
                moment(showSample.endDateAnalysis).format('DD/MM/YYYY HH:mm')}
            </Item2>
            <Item2 style={{ width: '16%', borderTop: 0 }}>
              {showSample.reportDate &&
                moment(showSample.reportDate).format('L')}
            </Item2>
            <Item2 style={{ width: '16%', borderTop: 0 }}>
              {showSample.agreedDeliveryDate &&
                moment(showSample.agreedDeliveryDate).format('L')}
            </Item2>
            <Item2 style={{ width: '16%', borderTop: 0 }}>
              {(showSample.deliveryDate &&
                moment(showSample.deliveryDate).format('L')) ||
                '  '}
            </Item2>

            {/* Muestra */}
            <View style={{ ...styles.methods2, width: '20%' }}>
              <Item2
                style={{
                  width: '100%',
                  borderBottom: 0,
                  borderTop: 0,
                  borderRight: 0
                }}
              >
                Fecha y Hora de Toma
              </Item2>
              <Item2 style={{ width: '100%', borderTop: 0, borderRight: 0 }}>
                {(showSample.takeSampleDate &&
                  moment(showSample.takeSampleDate).format(
                    'DD/MM/YYYY HH:mm'
                  )) ||
                  '  '}
              </Item2>
              <Item2
                style={{
                  width: '100%',
                  borderBottom: 0,
                  borderTop: 0,
                  borderRight: 0
                }}
              >
                Responsable Toma de Muestra
              </Item2>
              <Item2 style={{ width: '100%', borderTop: 0 }}>
                {responsibleSample || '  '}
              </Item2>
            </View>
            <View style={{ ...styles.methods, width: '80%' }}>
              <Item2 style={{ width: '100%', borderBottom: 0, borderTop: 0 }}>
                Tipo de muestra
              </Item2>
              <Item2
                style={{
                  width: '100%',
                  height: '80%',
                  borderBottom: 0,
                  borderTop: 0
                }}
              >
                {showSample.typeSample || ' '}
              </Item2>
            </View>

            {/* Detalles */}
            <Item2
              style={{
                width: '20%',
                borderBottom: 0,
                borderTop: 0,
                borderRight: 0
              }}
            >
              Lote
            </Item2>
            <Item2 style={{ width: '16%', borderBottom: 0 }}>
              Fecha de Fabricación
            </Item2>
            <Item2 style={{ width: '16%', borderBottom: 0 }}>
              Fecha de Vencimiento
            </Item2>
            <Item2 style={{ width: '48%', borderBottom: 0 }}>
              Cantidad de muestra
            </Item2>
            <Item2 style={{ width: '20%', borderTop: 0 }}>
              {showSample.lote}
            </Item2>
            <Item2 style={{ width: '16%', borderTop: 0 }}>
              {(showSample.manufacturingDate &&
                moment(showSample.manufacturingDate).format('L')) ||
                '  '}
            </Item2>
            <Item2 style={{ width: '16%', borderTop: 0 }}>
              {(showSample.expirationDate &&
                moment(showSample.expirationDate).format('L')) ||
                '  '}
            </Item2>
            <Item2 style={{ width: '48%', borderTop: 0 }}>
              {showSample.quantity}
            </Item2>

            <Item2
              style={{
                width: '36%',
                borderBottom: 0,
                borderTop: 0,
                paddingTop: 2
              }}
            >
              Tipo de Envase-Empaque
            </Item2>
            <Item2
              style={{
                width: '64%',
                borderBottom: 0,
                borderTop: 0,
                paddingTop: 2
              }}
            >
              Condiciones de Llegada al laboratorio Temperatura ºC
            </Item2>
            <Item2 style={{ width: '36%', borderTop: 0 }}>
              {showSample.typeContainer}
            </Item2>
            <Item2 style={{ width: '64%', borderTop: 0 }}>
              {showSample.temperature ? showSample.temperature + '°' : ' '}
            </Item2>

            {/* Características Organolépticas */}
            <Signature style={{ width: '100%' }}>{'  '}</Signature>
            <Item style={{ width: '100%', border: 0 }}>{CA}</Item>
            <Signature style={{ width: '100%' }}>{'  '}</Signature>

            <Item3 style={{ width: '20%' }}>ASPECTO</Item3>
            <Item3 style={{ width: '80%' }}>{showSample.appearance}</Item3>
            <Item3 style={{ width: '20%', borderTop: 0 }}>COLOR</Item3>
            <Item3 style={{ width: '80%', borderTop: 0 }}>
              {showSample.color}
            </Item3>
            <Item3 style={{ width: '20%', borderTop: 0 }}>OLOR</Item3>
            <Item3 style={{ width: '80%', borderTop: 0 }}>
              {showSample.smell}
            </Item3>

            {/* Productos */}
            <SubtitleTwo style={{ width: '20%', backgroundColor: '#e0e0e0' }}>
              PRODUCTO
            </SubtitleTwo>
            <Item
              style={{
                width: '80%',
                marginVertical: 10,
                backgroundColor: '#e0e0e0'
              }}
            >
              {showProduct.name || '  '}
            </Item>
            {/* <SubtitleTwo style={{ width: '100%', border: 0 }} /> */}

            <Item style={{ width: '20%', backgroundColor: '#e0e0e0' }}>
              AREA
            </Item>
            <Item style={{ width: '16%', backgroundColor: '#e0e0e0' }}>
              PARÁMETRO
            </Item>
            <Item style={{ width: '16%', backgroundColor: '#e0e0e0' }}>
              RESULTADO
            </Item>
            <Item style={{ width: '16%', backgroundColor: '#e0e0e0' }}>
              LIMITE MINIMO
            </Item>
            <Item style={{ width: '16%', backgroundColor: '#e0e0e0' }}>
              LIMITE MAXIMO
            </Item>
            <Item style={{ width: '16%', backgroundColor: '#e0e0e0' }}>
              METODO
            </Item>
            <ViewMethods methods={showProduct.methods} />

            {/* Conclusion y Sugerencias */}
            <Signature style={{ width: '100%' }}>{'  '}</Signature>
            <Subtitle style={{ width: '80%' }}>{C}</Subtitle>
            <Item3
              style={{
                width: '80%',
                borderTop: 0,
                paddingBottom: 2
              }}
            >
              {dataCS.conclusion || '  '}
            </Item3>
            {dataCS.sugerencias && (
              <Subtitle style={{ width: '80%', borderTop: 0 }}>{S}</Subtitle>
            )}
            {dataCS.sugerencias && (
              <Item3 style={{ width: '80%', borderTop: 0, paddingBottom: 2 }}>
                {dataCS.sugerencias}
              </Item3>
            )}
            <Signature style={{ width: '100%' }}>{'  '}</Signature>

            {/* Firmas */}
            <Image src={firmaA} style={{ width: '30%', maxHeight: 60 }} />
            <Signature style={{ width: '40%' }} />
            <Image src={firmaC} style={{ width: '30%', maxHeight: 60 }} />
            <Signature style={{ width: '100%' }}>{'  '}</Signature>

            <Signature style={{ width: '25%' }}>{nameA}</Signature>
            <Signature style={{ width: '50%' }} />
            <Signature style={{ width: '25%' }}>{nameC}</Signature>

            <Signature style={{ width: '30%' }}>
              Analista (Microbiología ó Fisicoquímico) {'                   '}
              Realiza
            </Signature>
            <Signature
              style={{ width: '45%', textAlign: 'center', paddingTop: 10 }}
            >
              Medellín, {dateToday}
            </Signature>
            <Signature style={{ width: '25%' }}>
              Coordinadora LACMA {'                                          '}
              Aprueba
            </Signature>

            <Signature style={{ width: '30%' }} />
            <Signature style={{ width: '45%', textAlign: 'center' }}>
              ----------- Fin de reporte -----------
            </Signature>
            <Signature style={{ width: '25%' }} />

            <Signature style={{ width: '100%' }}>{'  '}</Signature>
            <Signature style={{ width: '75%' }} />
            <Signature style={{ width: '25%', fontSize: 6 }}>
              FECHA PUBLICACION
            </Signature>
            <Signature style={{ width: '75%' }} />
            <Signature style={{ width: '25%', fontSize: 6 }}>
              27 de Septiembre de 2013
            </Signature>
            <Signature style={{ width: '100%' }}>{'  '}</Signature>
            <Footer />
          </View>
        </Page>
      </Document>
    );
  }
}

export default MyDocument;
