import React from 'react';
import { Text, View, StyleSheet } from '@react-pdf/renderer';

// import Roboto from '../../../../../public/fonts/Roboto-Regular.ttf';
// import Arial from '../../../../../public/fonts/arial.ttf';
// Font.register({
//   family: 'Roboto',
//   src: Roboto
// });

// Create styles
export const styles = StyleSheet.create({
  page: {
    padding: 30
  },
  container: {
    flex: 1,
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'center'
  },
  methods: {
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap'
  },
  methods2: {
    display: 'flex',
    flexDirection: 'column',
    flexWrap: 'wrap'
  },
  subtitle: {
    fontSize: 16,
    fontFamily: 'Helvetica-Bold'
  },
  subtitleTwo: {
    marginVertical: 10,
    textAlign: 'left',
    paddingLeft: 3
  },
  textLeft: {
    textAlign: 'left',
    paddingLeft: 3,
    paddingTop: 2
  },
  textLeft2: {
    textAlign: 'left',
    paddingLeft: 3
  },
  signature: {
    fontSize: 8
  },
  text: {
    textAlign: 'center',
    // alignItems: 'center',
    // alignContent: 'center',
    // alignItems: 'stretch',
    // borderBottomWidth: 0,
    borderWidth: 1,
    paddingTop: 5,
    fontSize: 8,
    fontFamily: 'Helvetica',
    borderColor: 'black',
    borderStyle: 'solid'
  }
});

export const Pages = ({ style, props }) => (
  <Text
    {...props}
    style={{ ...styles.text, ...style }}
    render={({ pageNumber, totalPages }) =>
      pageNumber > 1 ? null : `${pageNumber} de ${totalPages}`
    }
    fixed
  />
);

export const Item = ({ children, style, ...props }) => (
  <Text style={{ ...styles.text, ...style }} {...props}>
    {children}
  </Text>
);

export const Item2 = ({ children, style, ...props }) => (
  <Text style={{ ...styles.text, ...styles.textLeft, ...style }} {...props}>
    {children}
  </Text>
);

export const Item3 = ({ children, style, ...props }) => (
  <Text style={{ ...styles.text, ...styles.textLeft2, ...style }} {...props}>
    {children}
  </Text>
);

export const Signature = ({ children, style, ...props }) => (
  <Text {...props} style={{ ...styles.signature, ...style }}>
    {children}
  </Text>
);

export const Subtitle = ({ children, style, ...props }) => (
  <Text style={{ ...styles.text, ...styles.subtitle, ...style }} {...props}>
    {children}
  </Text>
);

export const SubtitleTwo = ({ children, style, ...props }) => (
  <Text
    {...props}
    style={{
      ...styles.text,
      ...styles.subtitleTwo,
      ...style
    }}
  >
    {children}
  </Text>
);

export const ViewMethods = ({ methods }) =>
  _.map(methods || [], (method, key) => (
    <View key={key} style={{ ...styles.methods, width: '100%' }}>
      <Item style={{ width: '20%', padding: 3, borderTop: 0 }}>
        {method.area}
      </Item>
      <Item style={{ width: '16%', padding: 3, borderTop: 0 }}>
        {method.parameter}
      </Item>
      <Item style={{ width: '16%', padding: 3, borderTop: 0 }}>
        {method.result}
      </Item>
      <Item style={{ width: '16%', padding: 3, borderTop: 0 }}>
        {method.limitMin}
      </Item>
      <Item style={{ width: '16%', padding: 3, borderTop: 0 }}>
        {method.limitMax}
      </Item>
      <Item style={{ width: '16%', padding: 3, borderTop: 0 }}>
        {method.name}
      </Item>
    </View>
  ));
