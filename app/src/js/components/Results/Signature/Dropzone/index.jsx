import React, { useEffect, useState } from 'react';

import { useDropzone } from 'react-dropzone';

export default React.memo(({ img, onDropAccepted }) => {
  const [files, setFiles] = useState([img || {}]);

  const { getRootProps, getInputProps } = useDropzone({
    accept: 'image/jpeg, image/png',
    multiple: false,
    onDrop: acceptedFiles => {
      setFiles(
        acceptedFiles.map(file =>
          Object.assign(file, {
            preview: URL.createObjectURL(file)
          })
        )
      );
    },
    onDropAccepted: files => {
      onDropAccepted(
        Object.assign(files[0], {
          preview: URL.createObjectURL(files[0])
        })
      );
    }
  });

  const thumbs = files.map(file => (
    <div className="thumb" key={file.name}>
      <img src={file.preview} />
    </div>
  ));

  useEffect(
    () => () => {
      // Make sure to revoke the data uris to avoid memory leaks
      files.forEach(file => URL.revokeObjectURL(file.preview));
    },

    [files]
  );

  return (
    <section>
      <div
        {...getRootProps({
          className: 'dropzone'
        })}
      >
        <input {...getInputProps()} />
        <p>
          Arrastre y suelte archivo aquí, o haga clic para seleccionar archivo
        </p>
      </div>
      <aside className="thumbscontainer">{thumbs}</aside>
    </section>
  );
});
