import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import Dropzone from './Dropzone';
import Panel from '../../Common/Panel';

import noImage from '../../../../../public/images/no-image.png';
import Input from '../../Common/Input';
import Button from '../../Common/Button';

const Signature = React.memo(
  ({ dataSig, signature, uploadImage, loadSignature, handleClick }) => {
    return (
      <div className="row">
        <div className="col s12 m6">
          <Panel
            className="card"
            panelContent={
              <Fragment>
                <Dropzone
                  img={{ preview: dataSig.firmaA || noImage, name: 'analista_001' }}
                  onDropAccepted={e => uploadImage('firmaA', e, 'analista')}
                />
                <Input
                  id="name_analista"
                  name="nameA"
                  text="Nombre Analista"
                  autoFocus={true}
                  icon="assignment_ind"
                  classNameIcon="prefix"
                  className=""
                  value={signature.nameA || ''}
                  onChange={loadSignature}
                />
              </Fragment>
            }
            title="Analista"
            subTitle="(Microbiología ó Fisicoquímico)"
          />
        </div>
        <div className="col s12 m6">
          <Panel
            className="card"
            title="Coordinador@ LACMA"
            panelContent={
              <Fragment>
                <Dropzone
                  img={{ preview: dataSig.firmaC || noImage, name: 'coordinador@_001' }}
                  onDropAccepted={e => uploadImage('firmaC', e, 'coordinador')}
                />
                <Input
                  id="name_coordinador"
                  name="nameC"
                  text="Nombre Coordinador"
                  autoFocus={true}
                  icon="assignment_ind"
                  classNameIcon="prefix"
                  className=""
                  value={signature.nameC || ''}
                  onChange={loadSignature}
                />
              </Fragment>
            }
          />
        </div>
        <div className="input-field col s12 align-center">
          <Button
            className="waves-effect waves-light blue lighten-1 btn"
            texto="Guardar"
            icon="send"
            classNameIcon="right"
            onClick={handleClick}
          />
        </div>
      </div>
    );
  }
);

Signature.defaultProps = {
  uploadImage: () => null
};

Signature.propTypes = {
  uploadImage: PropTypes.func
};

export default Signature;
