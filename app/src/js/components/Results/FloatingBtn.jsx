import React from 'react';
import { PDFDownloadLink } from '@react-pdf/renderer';
import MyDocument from './Pdf';

export default React.memo(({ onClick, signature, sample }) => {
  return (
    <div className="fixed-action-btn">
      <a className="btn-floating btn-large waves-effect waves-light">
        <i className="large material-icons">add</i>
      </a>
      <ul>
        {/* <PDFDownloadLink
          document={<MyDocument signature={signature} sample={sample} />}
          fileName="LAC-PS-FR-033.pdf"
        >
          {({ blob, url, loading, error }) => {
            console.log(blob, url);
            return loading ? (
              'Loading document...'
            ) : (
              <li>
                <a
                  data-delay="50"
                  data-position="right"
                  data-tooltip="Descargar PDF"
                  className="btn-floating blue tooltipped"
                >
                  <i className="material-icons">file_download</i>
                </a>
              </li>
            );
          }}
        </PDFDownloadLink> */}
        <li>
          <a
            data-delay="50"
            data-position="right"
            data-tooltip="Conclusión y Sugerencias"
            onClick={() => onClick('conSug')}
            className="btn-floating blue tooltipped"
          >
            <i className="material-icons">chat</i>
          </a>
        </li>
        <li>
          <a
            data-delay="50"
            data-position="right"
            data-tooltip="Cargar Muestra"
            onClick={() =>
              onClick('modalSample', {
                header: 'Muestras',
                contentModal: 'sample'
              })
            }
            className="btn-floating amber tooltipped"
          >
            <i className="material-icons">assignment</i>
          </a>
        </li>
        <li>
          <a
            data-delay="50"
            data-position="right"
            data-tooltip="Cambiar Firmas"
            onClick={() =>
              onClick('modalDropzone', {
                header: 'Firmas',
                contentModal: 'dropzone',
                fullscreen: true
              })
            }
            className="btn-floating blue-grey tooltipped"
          >
            <i className="material-icons">image</i>
          </a>
        </li>
        <li>
          <a
            data-delay="50"
            data-position="right"
            data-tooltip="Recuento de microorganismos"
            className="btn-floating red tooltipped"
            onClick={() =>
              onClick('modalCalculos', {
                header: 'Recuento de microorganismos',
                contentModal: 'calculos',
                fullscreen: true
              })
            }
          >
            <i className="material-icons">gradient</i>
          </a>
        </li>
      </ul>
    </div>
  );
});
