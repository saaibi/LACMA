import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Editor } from "@tinymce/tinymce-react";

import { basicEditor } from "../../data/editor";

export class Result extends Component {
  //   static propTypes = {
  //     prop: PropTypes
  //   };

  handleEditorChange = e => {
    console.log("Content was updated:", e.target.getContent());
  };

  render() {
    return (
      <div>
        <hr />
        <Editor
          initialValue="<p>This is the initial content of the editor</p>"
          init={basicEditor}
          onChange={this.handleEditorChange}
        />
      </div>
    );
  }
}

const mapStateToProps = state => ({});

export default connect(mapStateToProps)(Result);
