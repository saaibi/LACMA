import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import SelectInput from 'react-select';

import { sampleActions } from '../../../actions/sample.actions';
import { defaultConfig } from '../../Common/Select/selecttInput';

const customStyles = {
  menu: provided => ({
    ...provided,
    position: 'static'
  })
};

class SelectProduct extends PureComponent {
  handleSample = e => {
    if (e) {
      const { _id } = e;
      this.props.dispatch(
        sampleActions.getByIdSampleClientProductsMethods(_id)
      );
    }
  };

  render() {
    const { samples } = this.props;
    return (
      <div className="row">
        <div className="container">
          <SelectInput
            {...defaultConfig}
            styles={customStyles}
            className="basic-single  select_screen"
            defaultValue={!_.isEmpty(samples.sample) ? samples.sample : false}
            defaultMenuIsOpen
            name="sample"
            id="sample"
            placeholder="Selecione una Muestra"
            options={samples.samples}
            getOptionLabel={op => op.code}
            onChange={e => this.handleSample(e)}
          />
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  samples: state.sample
});

export default connect(mapStateToProps)(SelectProduct);
