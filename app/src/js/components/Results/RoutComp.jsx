import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import _ from 'lodash';
import { pdfjs } from 'react-pdf';
import { PDFViewer } from '@react-pdf/renderer';
import ResizeDetector from 'react-resize-detector';
pdfjs.GlobalWorkerOptions.workerSrc = `//cdnjs.cloudflare.com/ajax/libs/pdf.js/${pdfjs.version}/pdf.worker.js`;

import FloatingBtn from './FloatingBtn';
import ReCount from './Count';
import Signature from './Signature';
import Sample from './Sample';
import AppHeader from '../AppHeader';
import Modal from '../Common/Modal';
import MyDocument from './Pdf';

import {
  screenSize,
  openModal,
  toast,
  closeModal
} from '../../utils/dom-utils';
// import { onClickBTN } from '../../services/location.service';

import firma1 from '../../../../public/images/signatures/analista.jpg';
import firma2 from '../../../../public/images/signatures/coordinador.jpg';
import Progress from '../Common/Utils/Progress';
import { sampleActions } from '../../actions/sample.actions';
import Button from '../Common/Button/Button';
import { swalAlert, alertConSug } from '../../utils/alert';

class AppResult extends PureComponent {
  state = {
    width: 0,
    height: 0,
    dataCS: {},
    dataSig: {
      firmaA: firma1,
      firmaC: firma2,
      nameA: 'MANUELA GONZÁLEZ SÁNCHEZ',
      nameC: 'MAYRA ALEJANDRA FUENTES V'
    },
    signature: {
      nameA: 'MANUELA GONZÁLEZ SÁNCHEZ',
      nameC: 'MAYRA ALEJANDRA FUENTES V'
    },
    modal: {}
  };

  componentDidMount() {
    this.props.dispatch(sampleActions.getAllSample());
    this.setDataAnalist();
    // $('.fixed-action-btn').floatingActionButton({ hoverEnabled: true });
  }

  componentDidUpdate() {
    $('select').formSelect();
    $('.fixed-action-btn').floatingActionButton({ hoverEnabled: true });
    $('.tooltipped').tooltip({ delay: 50 });
  }

  setStateSignature = (name, vale) => {
    this.setState(({ signature }) => ({
      signature: { ...signature, [name]: vale }
    }));
  };

  setDataAnalist() {
    const { signature, dataSig } = this.state;
    const { img, firstName, lastName, username } = JSON.parse(
      localStorage.getItem('user')
    );
    let imageUrl = dataSig.firmaC;
    const analista = `${firstName || signature.nameA} ${lastName}`;
    if (username == 'admin') {
      return;
    }
    if (img) {
      const bytes = new Uint8Array(img.data.data);
      const stringChar = bytes.reduce(
        (data, byte) => `${data}${String.fromCharCode(byte)}`,
        ''
      );
      const base64String = window.btoa(stringChar);
      imageUrl = `data:${img.contentType};base64,${base64String}`;
    }
    this.setState(() => ({
      signature: {
        ...signature,
        firmaA: imageUrl,
        nameA: analista
      },
      dataSig: { ...dataSig, firmaA: imageUrl, nameA: analista }
    }));
  }

  uploadImage = (name, img, id) => {
    // const formData = new FormData();
    // formData.append('file', img);
    this.setStateSignature(name, img.preview);
    // this.props.dispatch(sampleActions.uploadSignature(formData, id));
  };

  loadSignature = e => {
    const { name, value } = e.target;
    this.setStateSignature(name, value);
  };

  handleClickSignature = () => {
    const { signature } = this.state;
    if (_.isEmpty(signature)) {
      return toast({ html: 'Subir Firmas' });
    }
    this.setState(({ dataSig }) => ({ dataSig: { ...dataSig, ...signature } }));
    closeModal('modalResult');
  };

  handleChange = (name, value) => {
    this.setState(() => ({ [name]: value }));
  };

  handleClick = async (id, dataModal) => {
    const { dataCS } = this.state;
    if (id.includes('modal')) {
      this.handleChange('modal', dataModal);
      openModal('modalResult');
    } else {
      const { value: data } = await swalAlert(alertConSug(dataCS));
      if (data) {
        this.handleChange('dataCS', {
          sugerencias: data.suggestions || null,
          conclusion: data.conclusion || null
        });
      }
    }
  };

  onResize = width => {
    this.setState({ width, height: screenSize().height });
  };

  render() {
    const { width, height, dataSig, signature, dataCS, modal } = this.state;
    const { samples } = this.props;

    if (_.isEmpty(samples.samples)) {
      return samples.isLoading ? (
        <Progress type="circle" />
      ) : (
        <Button text="Crear Muestra para continuar" to="/samples" />
      );
    }

    const modalSwitch = id =>
      ({
        calculos: <ReCount />,
        dropzone: (
          <Signature
            dataSig={dataSig}
            signature={signature}
            uploadImage={this.uploadImage}
            loadSignature={this.loadSignature}
            handleClick={this.handleClickSignature}
          />
        ),
        sample: <Sample samples={samples} />
      }[id] || '');

    return (
      <div className="container">
        <AppHeader name="RESULTADOS" />
        <PDFViewer width={width} height={height}>
          <MyDocument
            signature={dataSig}
            dataCS={dataCS}
            sample={samples.sample}
          />
        </PDFViewer>
        {/* <button onClick={() => onClickBTN()}>City </button> */}
        <Modal
          id="modalResult"
          {...modal}
          content={modalSwitch(modal.contentModal)}
        />
        <ResizeDetector handleWidth handleHeight onResize={this.onResize} />
        <FloatingBtn onClick={this.handleClick} />
      </div>
    );
  }
}
const mapStateToProps = state => ({
  samples: state.sample
});

const connectedResult = connect(mapStateToProps)(AppResult);
export { connectedResult as AppResult };

{
  /* 
        
          onDocumentLoadSuccess = ({ numPages }) => {
    this.setState({ numPages });
  };

  onItemClick = ({ pageNumber }) => this.setState({ pageNumber });
        <Document file={samplePDF} onLoadSuccess={this.onDocumentLoadSuccess}>
          {Array.from(new Array(numPages), (el, index) => (
            <Page
              key={`page_${index + 1}`}
              pageNumber={index + 1}
              width={width}
            />
          ))}
        </Document>
        <p>
          Page {pageNumber} of {numPages}
        </p> */
}
