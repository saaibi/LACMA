import React, { Fragment } from 'react';

import Email from '../../../../Common/Input';
import Password from '../../../../Common/Input';

const Fields = ({ handleSubmit, handleChange }) => (
  <Fragment>
    <Email
      id="email"
      name="email"
      text="Usuario"
      type="text"
      className="col s6 offset-s3"
      icon="email"
      classNameIcon="prefix"
      onChange={handleChange}
    />
    <Password
      id="password"
      name="password"
      text="Contraseña"
      type="password"
      className="col s6 offset-s3"
      autoFocus={true}
      onKeyUp={e => {
        if (e.keyCode === 13) {
			console.log(e)
          handleSubmit(e);
        }
      }}
      icon="fingerprint"
      classNameIcon="prefix"
      onChange={handleChange}
    />
  </Fragment>
);

export default Fields;
