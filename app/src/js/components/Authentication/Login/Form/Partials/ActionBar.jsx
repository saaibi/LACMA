import React from 'react';
import { Link } from 'react-router-dom';

import Save from '../../../../Common/Button';
import Progress from '../../../../Common/Utils/Progress';

const ActionBar = ({ handleSubmit, isLoading }) => (
  <div className="container">
    <div className="row">
      <div className="input-field col align-center  s12">
        {isLoading ? (
          <Progress type="circular" />
        ) : (
          <Save
            className="waves-effect waves-light btn"
            texto="Login"
            icon="send"
            classNameIcon="right"
            onClick={handleSubmit}
          />
        )}
      </div>
    </div>
  </div>
);

export default ActionBar;
