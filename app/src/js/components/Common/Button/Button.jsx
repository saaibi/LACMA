import React from 'react';
import { Link } from 'react-router-dom';

export default ({ text, to }) => (
  <div className="center-align">
    <Link className="waves-effect waves-light blue lighten-1 btn" to={to}>
      {text}
    </Link>
  </div>
);
