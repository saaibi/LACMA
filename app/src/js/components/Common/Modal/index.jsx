import React from 'react';
import PropTypes from 'prop-types';
import { Modal } from 'react-materialize';

const Modals = ({ id, header, content, fullscreen }) => {
  return (
    <Modal
      id={id}
      header={header}
      className={fullscreen ? 'modal-fullscreen' : ''}
    >
      {content}
    </Modal>
  );
};

Modals.defaultProps = {
  header: 'LACMA'
};

Modals.propTypes = {
  header: PropTypes.string
};

export default Modals;
