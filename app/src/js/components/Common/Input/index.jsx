import React from 'react';
import PropTypes from 'prop-types';

import Icons from '../Icons';

const Input = ({
  id,
  name,
  value,
  text,
  type,
  autoFocus,
  className,
  defaultValue,
  classNameInput,
  classNameLabel,
  placeholder,
  onChange,
  onSelect,
  onBlur,
  onKeyUp,
  icon,
  helper,
  disabled,
  classNameIcon
}) => {
  return (
    <div className={`input-field ${className}`}>
      {icon && <Icons className={classNameIcon} icon={icon} />}
      <input
        id={id}
        name={name}
        value={value}
        disabled={disabled}
        defaultValue={defaultValue}
        type={type}
        autoFocus={autoFocus}
        className={classNameInput}
        placeholder={placeholder}
        onChange={onChange}
        onSelect={onSelect}
        onBlur={onBlur}
        onKeyUp={onKeyUp}
      />
      <label htmlFor={id} className={classNameLabel}>
        {text}
      </label>
      {helper && <span className="helper-text">{helper}</span>}
    </div>
  );
};

Input.defaultProps = {
  className: 'col s12',
  classNameInput: 'validate',
  classNameLabel: 'active',
  type: 'text',
  helper: '',
  autoFocus: false,
  disabled: false
};

Input.propTypes = {
  name: PropTypes.string.isRequired,
  onChange: PropTypes.func,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  icon: PropTypes.string,
  classNameIcon: (props, propName, componentName) => {
    if (props.icon && !props.classNameIcon) {
      return new Error(
        `Invalid prop '${propName}' supplied to '${componentName}' .Validation failed`
      );
    }
  },
  placeholder: PropTypes.string
};

export default Input;
