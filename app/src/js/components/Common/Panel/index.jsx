import React from "react";
import PropTypes from "prop-types";

const Panel = ({ title, subTitle, panelContent, className }) => {
  return (
    <div className={`card-panel ${className} `}>
      <span className="card-title">{title}</span>
      {subTitle}
      {panelContent}
    </div>
  );
};

Panel.defaultProps = {
  title: "Panel",
  className: "",
  subTitle: ""
};

Panel.propTypes = {
  title: PropTypes.string,
  subTitle: PropTypes.string,
  className: PropTypes.string
};

export default Panel;
