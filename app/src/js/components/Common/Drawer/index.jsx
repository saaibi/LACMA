import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';

const useStyles = makeStyles({
  list: {
    width: 300
  },
  fullList: {
    width: 'auto'
  }
});

export default ({ children, openDrawer, aling, handleClickOpen }) => {
  const [open, setOpen] = useState(false);
  const [anchor, setAnchor] = useState('left');
  const classes = useStyles();

  useEffect(() => {
    setOpen(openDrawer);
    if (aling) {
      setAnchor(aling);
    }
  }, [openDrawer]);

  const handleClose = () => {
    setOpen(false);
    handleClickOpen();
  };

  return (
    <Drawer open={open} anchor={anchor} onClose={handleClose}>
      <div className={classes.list} role="presentation">
        {children}
      </div>
    </Drawer>
  );
};
