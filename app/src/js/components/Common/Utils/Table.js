import React,{Fragment} from 'react';
import { defaultThemes } from 'react-data-table-component';
import CircularProgress from '@material-ui/core/CircularProgress';
import Checkbox from '@material-ui/core/Checkbox';
import ArrowDownward from '@material-ui/icons/ArrowDownward';
import memoize from 'memoize-one';
import Input from '../Input';
import Button from '../Button';

export const paginationOptions = {
  rowsPerPageText: 'Filas por página',
  rangeSeparatorText: 'de'
};

export const customStyles = {
  cells: {
    style: {
      '&:not(:last-of-type)': {
        borderRightStyle: 'solid',
        borderRightWidth: '1px',
        borderRightColor: defaultThemes.default.divider.default
      }
    }
  }
};

export const NoData = () => (
  <div style={{ padding: '24px' }}>No hay registros para mostrar</div>
);

export const Progress = () => (
  <div style={{ padding: '24px' }}>
    <CircularProgress size={75} />
  </div>
);

const sortIcon = <ArrowDownward />;
const selectProps = { indeterminate: isIndeterminate => isIndeterminate };

export const defaultConfigTable = {
  sortIcon: sortIcon,
  selectableRowsComponent: Checkbox,
  selectableRowsComponentProps: selectProps
};


export const FilterComponent = memoize(({ filterText, onFilter, onClear }) => (
  <Fragment>
    <Input
      id="search"
      text="Filtro"
      className="col s12 m6"
      autoFocus={true}
      name="search"
      value={filterText}
      onChange={onFilter}
      icon="filter_list"
      classNameIcon="prefix"
    />
    <Button
      icon="clear"
      onClick={onClear}
      classNameIcon="X"
      className="btn-floating btn-small waves-effect waves-light"
    />
  </Fragment>
));