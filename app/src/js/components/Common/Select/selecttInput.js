const customStyles = {
  container: provided => ({
    ...provided,
    zIndex: 2
  })
};

const theme = theme => ({
  ...theme,
  borderRadius: 0,
  colors: {
    ...theme.colors,
    primary: '#009688',
    primary25: '#b2dfdb',
    primary50: '#4db6ac',
    danger: '#f44336'
  }
});
export const defaultConfig = {
  theme: theme,
  styles: customStyles,
  isDisabled: false,
  isLoading: false,
  isRtl: false,
  isMulti: false,
  isClearable: true,
  isSearchable: true,
  classNamePrefix: 'select',
  placeholder: 'Selecione..',
  noOptionsMessage: () => 'No hay opciones',
  getOptionValue: op => op._id,
  getOptionLabel: op => op.name
};
