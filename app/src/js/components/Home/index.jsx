import React, { Component } from 'react';
import { connect } from 'react-redux';

import Screen from './Screen';
import { sampleActions } from '../../actions/sample.actions';

class HomePage extends Component {
  componentDidMount() {
    const { _id } = JSON.parse(localStorage.getItem('user'));
    this.props.dispatch(sampleActions.getAllSamplesByAnaly({ _id }));
    $('.sidenav').sidenav({
      edge: 'left'
    });
  }

  render() {
    return (
      <div className="container home-component">
        <Screen />
      </div>
    );
  }
}

const mapStateToProps = state => ({});

const connectedHome = connect(mapStateToProps)(HomePage);
export { connectedHome as HomePage };
