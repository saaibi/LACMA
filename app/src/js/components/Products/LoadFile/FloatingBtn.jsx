import React from "react";

export default React.memo(({ onClick }) => {
  return (
    <div className="fixed-action-btn products">
      <a className="btn-floating btn-large waves-effect waves-light">
        <i className="large material-icons">add</i>
      </a>
      <ul>
        <li>
          <a
            data-delay="50"
            data-position="right"
            data-tooltip="Cargar Base de Datos, (Excel)"
            className="btn-floating red tooltipped"
            onClick={() => onClick("madalFile")}
          >
            <i className="material-icons">file_upload</i>
          </a>
        </li>
      </ul>
    </div>
  );
});
