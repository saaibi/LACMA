import React, { useMemo, useEffect, useCallback, useState } from 'react';
import DataTable from 'react-data-table-component';
import memoize from 'memoize-one';

import ActionBarGrid from './Partials/ActionBarGrid';
import View from '../View';
import {
  NoData,
  Progress,
  customStyles,
  paginationOptions,
  FilterComponent,
  defaultConfigTable
} from '../../Common/Utils/Table';
import Button from '../../Common/Button';

const contextActions = memoize(deleteHandler => (
  <Button
    className="btn-floating waves-effect waves-light red"
    icon="delete_forever"
    classNameIcon="prefix"
    onClick={deleteHandler}
  />
));

export default ({ data, pending, handleButtonClick }) => {
  const [items, setItems] = useState([]);
  const [filterText, setFilterText] = useState('');
  const [resetPagiTog, setResetPagiTog] = useState(false);

  const handleClear = () => {
    if (filterText) {
      setResetPagiTog(!resetPagiTog);
      setFilterText('');
    }
  };

  const subHeaderComponentMemo = useMemo(() => {
    return (
      <FilterComponent
        onFilter={e => setFilterText(e.target.value)}
        onClear={handleClear}
        filterText={filterText}
      />
    );
  }, [filterText, resetPagiTog]);

  useEffect(() => {
    const dataFilter = _.filter(data, i =>
      i.name.toLowerCase().includes(filterText.toLowerCase())
    );
    setItems(dataFilter);
    $('select').formSelect();
  }, [filterText, data]);

  const columns = useMemo(
    () => [
      {
        name: 'Nombre de Producto',
        selector: 'name',
        wrap: true,
        grow: 3,
        sortable: true
      },
      // {
      //   name: 'Codigo',
      //   selector: 'code',
      //   center: true,
      //   sortable: true
      // },
      {
        name: 'Numero de Metodos',
        cell: row => row.methods.length,
        sortable: true,
        center: true
      },
      {
        cell: row => (
          <ActionBarGrid row={row} handleButtonClick={handleButtonClick} />
        ),
        name: 'Acciones',
        ignoreRowClick: true,
        allowOverflow: true,
        style: {
          position: 'sticky',
          right: 0
        },
        button: true
      }
    ],
    []
  );

  const [selectedRows, setSelectedRows] = useState([]);
  const [toggleCleared, setToggleCleared] = useState(false);
  const handleChange = useCallback(
    ({ allSelected, selectedCount, selectedRows }) => {
      const resData = _.map(selectedRows, '_id');
      setSelectedRows(resData);
    },
    []
  );
  const deleteAll = () => {
    handleButtonClick({ action: 'deleteAll', rows: selectedRows });
    setToggleCleared(!toggleCleared);
    handleClear();
  };

  return (
    <DataTable
      {...defaultConfigTable}
      pagination
      highlightOnHover
      striped
      persistTableHead
      subHeader
      selectableRows
      expandableRows
      expandOnRowClicked
      defaultSortField="name"
      data={items}
      columns={columns}
      progressPending={pending}
      customStyles={customStyles}
      clearSelectedRows={toggleCleared}
      onSelectedRowsChange={handleChange}
      paginationResetDefaultPage={resetPagiTog}
      contextActions={contextActions(deleteAll)}
      paginationComponentOptions={paginationOptions}
      subHeaderComponent={subHeaderComponentMemo}
      noDataComponent={<NoData />}
      progressComponent={<Progress />}
      expandableRowsComponent={<View />}
    />
  );
};
