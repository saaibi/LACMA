import React, { Component } from 'react';
import { Helmet } from 'react-helmet';

import AppHeader from '../AppHeader';
import Product from '.';

class AppProducts extends Component {
  render() {
    return (
      <div className="container">
        <Helmet titleTemplate="LACMA - %s">
          <title>PRODUCTOS</title>
        </Helmet>
        <AppHeader name="PRODUCTOS" />
        <Product />
      </div>
    );
  }
}

export { AppProducts };
