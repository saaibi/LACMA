import React, { PureComponent } from 'react';

import AsyncSelect from 'react-select/async';
import { colourOptions } from '../../../../data/select';
import { defaultConfig } from '../../../Common/Select/selecttInput';

const customStyles = {
  container: provided => ({
    ...provided,
    zIndex: 3,
    marginLeft: '45px'
  }),
  control: provided => ({
    ...provided,
    backgroundColor: 'transparent'
  }),
  placeholder: (provided, state) => ({
    ...provided,
    display: 'none'
  }),
  indicatorsContainer: () => null
};

export default class AsyncMulti extends PureComponent {
  state = {
    inputValue: ''
  };

  filterColors = inputValue => {
    const { methods } = this.props;
    return _.filter(methods, i => {
      return i.name.toLowerCase().includes(inputValue.toLowerCase());
    });
  };

  promiseOptions = inputValue =>
    new Promise(resolve => {
      const { methods } = this.props;
      setTimeout(() => {
        const data = this.filterColors(inputValue);
        console.log(methods);
        resolve(methods);
      }, 1000);
    });

  onChange = (data, { name, option, action }) => {
    console.log(name, _.map(data, '_id'));
  };

  render() {
    const { defaultValue } = this.props;
    return (
      <div className="input-field col s12">
        <i className="material-icons prefix">slow_motion_video</i>
        <AsyncSelect
          {...defaultConfig}
          isMulti
          cacheOptions
          defaultOptions
          name="methods"
          closeMenuOnSelect={false}
          className="basic-multi-select select_methods"
          styles={customStyles}
          getOptionLabel={op => op.parameter}
          defaultValue={defaultValue}
          loadOptions={this.promiseOptions}
          onChange={this.onChange}
        />
        <label htmlFor="first_name" className="active">
          Parámetro
        </label>
      </div>
    );
  }
}
