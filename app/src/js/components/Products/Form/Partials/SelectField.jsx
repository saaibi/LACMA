import React, { useState, useCallback, useEffect } from 'react';
import SelectInput from 'react-select';
import makeAnimated from 'react-select/animated';
import _ from 'lodash';

const animatedComponents = makeAnimated();

import { defaultConfig } from '../../../Common/Select/selecttInput';
import Progress from '../../../Common/Utils/Progress';

const customStyles = {
  container: provided => ({
    ...provided,
    zIndex: 2,
    marginLeft: '45px'
  }),
  control: provided => ({
    ...provided,
    backgroundColor: 'transparent'
  }),
  placeholder: (provided, state) => ({
    ...provided,
    display: 'none'
  }),
  indicatorsContainer: () => null
};

export default ({
  product,
  methods,
  isLoading,
  methodsCreate,
  loadMethods
}) => {
  // const [items, setItems] = useState([]);
  // useEffect(() => {
  //   setItems(methods);
  // }, [methods]);

  // const [defaultValue, setFefaultValue] = useState([]);
  // useEffect(() => {
  //   setFefaultValue(methodsCreate);
  // }, [methodsCreate]);

  const handleChange = useCallback((state, { name }) => {
    console.log(name, _.map(state, '_id'));
  }, []);

  console.log(
    isLoading,
    _.map(product.methods, _id => _.find(methods, { _id }))
  );
  return (
    <div className="input-field col s12">
      <i className="material-icons prefix">slow_motion_video</i>
      {/* {isLoading ? (
        <Progress />
      ) : ( */}
      <SelectInput
        {...defaultConfig}
        styles={customStyles}
        className="basic-multi-select select_methods"
        components={animatedComponents}
        defaultValue={_.map(product.methods, _id => _.find(methods, { _id }))}
        getOptionLabel={op => op.parameter}
        closeMenuOnSelect={false}
        isMulti
        name="methods"
        id="methods_2"
        options={methods}
        onChange={loadMethods}
      />
      {/* )} */}
      <label htmlFor="first_name" className="active">
        Parámetro
      </label>
    </div>
  );
};
