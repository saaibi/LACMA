import React, { Fragment } from 'react';
import SelectInput from 'react-select';
import makeAnimated from 'react-select/animated';
import _ from 'lodash';

const animatedComponents = makeAnimated();

import Input from '../../../Common/Input';
import Button from '../../../Common/Button';
import { openModal } from '../../../../utils/dom-utils';
import { defaultConfig } from '../../../Common/Select/selecttInput';
import Progress from '../../../Common/Utils/Progress';

const customStyles = {
  container: provided => ({
    ...provided,
    zIndex: 2,
    marginLeft: '45px'
  }),
  control: provided => ({
    ...provided,
    backgroundColor: 'transparent'
  }),
  placeholder: (provided, state) => ({
    ...provided,
    display: 'none'
  }),
  indicatorsContainer: () => null
};

const Fields = React.memo(
  ({
    loadProduct,
    loadMethods,
    methods,
    product,
    isLoading,
    handleClickOpen,
    methodsCreate
  }) => {
    return (
      <Fragment>
        <Input
          id="name_product"
          name="name"
          text="Nombre Producto"
          autoFocus={true}
          icon="assignment_ind"
          classNameIcon="prefix"
          value={product.name || ''}
          onChange={loadProduct}
        />

        {/* <Input
      id="code"
      name="code"
      text="Code"
      icon="line_style"
      classNameIcon="prefix"
      value={product.code || ""}
      onChange={loadProduct}
    /> */}

        {isLoading ? (
          <Progress />
        ) : (
          <div className="input-field col s12">
            <i className="material-icons prefix">slow_motion_video</i>
            <SelectInput
              {...defaultConfig}
              styles={customStyles}
              className="basic-multi-select select_methods"
              components={animatedComponents}
              defaultValue={methodsCreate}
              isLoading={isLoading}
              getOptionLabel={op => op.parameter}
              closeMenuOnSelect={false}
              isMulti
              name="methods"
              id="methods"
              options={methods}
              onChange={loadMethods}
            />
            <label htmlFor="first_name" className="active">
              Parámetro
            </label>
          </div>
        )}

        <div className="input-field col align-center float-initial">
          <Button
            className="waves-effect waves-light blue lighten-1 btn"
            texto="Crear Metodo"
            icon="slow_motion_video"
            classNameIcon="right"
            onClick={handleClickOpen}
          />
        </div>
      </Fragment>
    );
  }
);

export default Fields;
