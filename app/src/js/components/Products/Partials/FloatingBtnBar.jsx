import React from 'react';

export default React.memo(({ onClick }) => {
  return (
    <div className="nav-content">
      <a
        data-delay="50"
        data-position="top"
        data-tooltip="Crear Nuevo Producto"
        className="btn-floating btn-large halfway-fab orange tooltipped"
        style={{ left: '10px' }}
        onClick={onClick}
      >
        <i className="material-icons">add_circle_outline</i>
      </a>
    </div>
  );
});
