import React, { Fragment, useState, useEffect } from 'react';
import SelectInput from 'react-select';
import makeAnimated from 'react-select/animated';

const animatedComponents = makeAnimated();

import Input from '../../../Common/Input';
import { defaultConfig } from '../../../Common/Select/selecttInput';

const customStyles = {
  container: provided => ({
    ...provided,
    zIndex: 2,
    marginLeft: '45px'
  }),
  control: provided => ({
    ...provided,
    backgroundColor: 'transparent'
  }),
  placeholder: (provided, state) => ({
    ...provided,
    display: 'none'
  }),
  indicatorsContainer: () => null
};

export default ({ product, methods, loadProduct, loadMethods }) => {
  return (
    <Fragment>
      <Input
        id="name_product_2"
        name="name"
        text="Nombre Producto"
        icon="assignment_ind"
        classNameIcon="prefix"
        value={product.name || ''}
        onChange={loadProduct}
      />

      <div className="input-field col s12">
        <i className="material-icons prefix">slow_motion_video</i>
        <SelectInput
          {...defaultConfig}
          styles={customStyles}
          className="basic-multi-select select_methods"
          components={animatedComponents}
          defaultValue={_.map(product.methods, _id => _.find(methods, { _id }))}
          getOptionLabel={op => op.parameter}
          closeMenuOnSelect={false}
          isMulti
          name="methods"
          id="methods_2"
          options={methods}
          onChange={loadMethods}
        />
        <label htmlFor="first_name" className="active">
          Parámetro
        </label>
      </div>

      {/* <Input
      id="code_2"
      name="code"
      text="Code"
      icon="line_style"
      classNameIcon="prefix"
      value={product.code || ''}
      onChange={loadProduct}
    /> */}
    </Fragment>
  );
};
