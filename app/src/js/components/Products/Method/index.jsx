import React from 'react';

import Fields from './Form/Partials/Fields';
import ActionBar from './Form/Partials/ActionBar';

const Form = props => (
  <div className="row">
    <Fields {...props} />
    <ActionBar {...props} />
  </div>
);

export default Form;
