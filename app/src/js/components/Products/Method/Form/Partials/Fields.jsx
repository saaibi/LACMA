import React from 'react';

import Input from '../../../../Common/Input';

const Fields = ({method, loadMethod}) => (
  <div>
    <Input
      id="name_method"
      name="name"
      text="Nombre"
      autoFocus={true}
      icon="assignment_ind"
      classNameIcon="prefix"
      value={method.name || ''}
      onChange={loadMethod}
    />

    <Input
      id="area_method"
      name="area"
      text="Area"
      icon="insert_drive_file"
      classNameIcon="prefix"
      value={method.area || ''}
      onChange={loadMethod}
    />
    <Input
      id="parameter"
      name="parameter"
      text="Parametro"
      icon="description"
      classNameIcon="prefix"
      value={method.parameter || ''}
      onChange={loadMethod}
    />
    <Input
      id="result"
      name="result"
      text="Resultado"
      icon="assessment"
      classNameIcon="prefix"
      value={method.result || ''}
      onChange={loadMethod}
    />
    <Input
      id="limit_min"
      name="limitMin"
      text="Limite Minimo"
      icon="remove"
      classNameIcon="prefix"
      value={method.limitMin || ''}
      onChange={loadMethod}
    />
    <Input
      id="limit_max"
      name="limitMax"
      text="Limite Maximo"
      icon="add"
      classNameIcon="prefix"
      value={method.limitMax || ''}
      onChange={loadMethod}
    />
    <Input
      id="description"
      name="description"
      text="Descripción"
      icon="art_track"
      classNameIcon="prefix"
      value={method.description || ''}
      onChange={loadMethod}
    />
  </div>
);

export default Fields;
