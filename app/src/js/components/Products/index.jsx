import React, { PureComponent, Fragment } from 'react';
import { connect } from 'react-redux';
import XLSX from 'xlsx';
import { saveAs } from 'file-saver';
import Helmet from 'react-helmet';

import { productActions } from '../../actions/product.actions';

import Modal from '../Common/Modal';
import Method from './Method';
import Form from './Form';
import Table from './Table';
import View from './View';
import Edit from './Edit';
import Progress from '../Common/Utils/Progress';
import { methodActions } from '../../actions/method.actions';
import { toast, formSwal } from '../../utils/dom-utils';
import { swalAlert, alertDelete } from '../../utils/alert';
import FloatingBtn from './LoadFile/FloatingBtn';
import FloatingBtnBar from './Partials/FloatingBtnBar';
import AppHeader from '../AppHeader';
import Drawer from '../Common/Drawer';

const Header = React.memo(({ onClick }) => (
  <Fragment>
    <Helmet titleTemplate="LACMA - %s">
      <title>PRODUCTOS</title>
    </Helmet>
    <AppHeader
      name="PRODUCTOS"
      floatingContent={<FloatingBtnBar onClick={() => onClick()} />}
    />
  </Fragment>
));

class AppProducts extends PureComponent {
  constructor(props) {
    super(props);
  }

  state = {
    product: {
      methods: []
    },
    method: {},
    modal: {},
    productEdit: {},
    methodsCreate: [],
    isLoadMethod: false,
    isLoadEdit: false,
    openDrawer: false,
    openDrawer2: false
  };

  componentDidMount() {
    this.props.dispatch(productActions.getAllProductMethods());
    this.props.dispatch(methodActions.getAllMethods());
  }

  componentDidUpdate() {
    $('select').formSelect();
    $('.fixed-action-btn').floatingActionButton({ hoverEnabled: true });
    $('.tooltipped').tooltip({ delay: 50 });
  }

  static getDerivedStateFromProps(props, state) {
    const { isLoadMethod, isLoadEdit, methodsCreate, product } = state;
    const {
      products: {
        isSavemethod,
        isLoadProduct,
        method,
        error: { src }
      },
      products
    } = props;

    if (!!products.product && !_.isEqual(isLoadProduct, isLoadEdit)) {
      console.log('####');
      return {
        productEdit: products.product,
        isLoadEdit: isLoadProduct
      };
    }

    if (!_.isEqual(isLoadMethod, isSavemethod)) {
      console.log('$$$$$');
      return {
        isLoadMethod: isSavemethod,
        methodsCreate: [...methodsCreate, method],
        product: {
          ...product,
          methods: [...product.methods, method._id]
        }
      };
    }

    return null;
  }

  loadProduct = e => {
    const { product } = this.state;
    const { name, value } = e.target;
    this.setState({
      product: {
        ...product,
        [name]: value
      }
    });
  };

  loadMethods = (data, { name, option, action }) => {
    const { product } = this.state;
    this.setState({
      product: {
        ...product,
        [name]: _.map(data, '_id')
      },
      methodsCreate: data ? data : []
    });
  };

  setProdctEdit = (name, value) => {
    this.setState(({ productEdit }) => ({
      productEdit: {
        ...productEdit,
        [name]: value
      }
    }));
  };

  loadProductEdit = e => {
    const { name, value } = e.target;
    this.setProdctEdit(name, value);
  };

  loadMethodsEdit = (data, { name, option, action }) => {
    this.setProdctEdit(name, _.map(data, '_id'));
  };

  optionsProduct = async e => {
    const { action, row, rows } = e;
    const { dispatch } = this.props;
    switch (action) {
      case 'delete':
        const { value: deleteOne } = await swalAlert(alertDelete());
        if (deleteOne) dispatch(productActions.deleteProduct([row._id]));
        break;
      case 'deleteAll':
        const { value: deleteMany } = await swalAlert(alertDelete());
        if (deleteMany) dispatch(productActions.deleteProduct(rows));
        break;
      case 'edit':
        this.setState(() => ({
          modal: {
            headerModal: 'Editar Producto',
            contentModal: 'edit'
          },
          product_id: row._id
        }));
        dispatch(productActions.getProductById(row._id));
        $('#modalProduct').modal('open');
        break;
      case 'view':
        this.setState(() => ({
          modal: {
            headerModal: 'Ver Producto',
            contentModal: 'view'
          },
          product_id: row._id
        }));
        dispatch(productActions.getByIdProductMethods(row._id));
        $('#modalProduct').modal('open');
        break;
      default:
        break;
    }
  };

  createProduct = e => {
    e.preventDefault();
    const { product } = this.state;
    if (!product.name) {
      return toast({ html: 'Nombre de Producto es requerido' });
    } else if (_.isEmpty(product.methods)) {
      return toast({ html: 'Selecciona al menos un método' });
    }
    this.props.dispatch(productActions.createProduct(product));
    this.setState(() => ({
      product: { methods: [] },
      methodsCreate: [],
      openDrawer: false
    }));
  };

  updateProduct = e => {
    e.preventDefault();
    const { productEdit, product_id } = this.state;
    delete productEdit._id;
    // delete productEdit.methods;
    this.props.dispatch(productActions.updateProduct(product_id, productEdit));
    $('#modalProduct').modal('close');
  };

  loadMethod = e => {
    const { method } = this.state;
    const { name, value } = e.target;
    this.setState({
      method: {
        ...method,
        [name]: value
      }
    });
  };

  createMethod = e => {
    e.preventDefault();
    const { method } = this.state;
    if (!method.name) {
      return toast({ html: 'Nombre del Metodo es requerido' });
    }
    this.props.dispatch(methodActions.createMethod(method));
    // $('#madalMethod').modal('close');
    this.setState(() => ({ method: {}, openDrawer2: false }));
  };

  loadFile = async e => {
    const { value: formValues } = await swalAlert({
      title: 'Seleccionar Archivo',
      html: formSwal,
      focusConfirm: false,
      confirmButtonText: 'Enviar',
      preConfirm: () => {
        return {
          fileName: document.getElementById('fileName').files[0],
          save: document.getElementById('saveData').checked
        };
      }
    });
    if (formValues && formValues.fileName) {
      const formData = new FormData();
      _.forIn(formValues, function(value, key) {
        formData.append(key, value);
      });
      this.props.dispatch(
        productActions.createProductFile(formData, formValues.save)
      );
    } else {
      toast({ html: 'Por favor cargar un archivo' });
    }
  };

  readExcel = async () => {
    const { value: file } = await swalAlert({
      title: 'Selecciona Archivo',
      input: 'file',
      inputAttributes: {
        accept: '.xlsx',
        'aria-label': 'Upload your profile picture'
      }
    });

    if (file) {
      const reader = new FileReader();
      reader.onload = e => {
        const data = e.target.result;
        // const data = new Uint8Array(e.target.result);
        // const wb = XLSX.read(data, { type: 'array', raw: true });
        const wb = XLSX.read(data, { type: 'binary' });
        const ws_name = wb.SheetNames[0];
        const ws = wb.Sheets[ws_name];
        // const json = XLSX.utils.sheet_to_json(ws);
        // const ws = XLSX.utils.json_to_sheet(json);
        // ws.A4 = { t: 's',v: 'HI' };
        XLSX.utils.book_append_sheet(wb, ws);
        const wbout = XLSX.write(wb, { bookType: 'xlsx', type: 'binary' });
        const blob = this.s2ab(wbout);
        saveAs(
          new Blob([blob], { type: 'application/octet-stream' }),
          'lacma.xlsx'
        );

        console.log(ws);
        console.log(wb);
      };
      reader.onerror = ex => console.log(ex);
      // reader.readAsArrayBuffer(file);
      reader.readAsBinaryString(file);
      // reader.readAsDataURL(file);
    }
  };

  s2ab = s => {
    const buf = new ArrayBuffer(s.length);
    const view = new Uint8Array(buf);
    for (var i = 0; i < s.length; i++) view[i] = s.charCodeAt(i) & 0xff;
    return buf;
  };

  writeExcel = () => {
    const wb = XLSX.utils.book_new();
    const ws_name = 'Test Sheet';
    wb.Props = {
      Title: 'SheetJS Tutorial',
      Subject: 'Test',
      Author: 'Saaibi Florez',
      CreatedDate: new Date()
    };
    const ws = XLSX.utils.json_to_sheet(
      [
        { S: 1, h: 2, e: 3, e_1: 4, t: 5, J: 6, S_1: 7 },
        { S: 2, h: 3, e: 4, e_1: 5, t: 6, J: 7, S_1: 8 }
      ],
      { header: ['S', 'h', 'e', 'e_1', 't', 'J', 'S_1'] }
    );
    XLSX.utils.book_append_sheet(wb, ws, ws_name);
    const wbout = XLSX.write(wb, { bookType: 'xlsx', type: 'binary' });
    const blob = this.s2ab(wbout);
    saveAs(new Blob([blob], { type: 'application/octet-stream' }), 'test.xlsx');
  };

  handleClickOpen = () => {
    this.setState(state => ({ openDrawer: !state.openDrawer }));
  };
  handleClickOpen2 = () => {
    this.setState(state => ({ openDrawer2: !state.openDrawer2 }));
  };

  render() {
    const {
      product,
      method,
      methodsCreate,
      modal,
      openDrawer,
      openDrawer2,
      productEdit
    } = this.state;
    const { products } = this.props;

    // if (_.isEmpty(products.products)) {
    //   return <Progress type="circle" />;
    // }
    const modalSwitch = id =>
      ({
        view: <View data={products.product} />,
        load: <Progress type="circle" />,
        edit: (
          <Edit
            product={productEdit}
            methods={products.methods}
            isLoading={products.isLoading}
            loadProduct={this.loadProductEdit}
            loadMethods={this.loadMethodsEdit}
            updateProduct={this.updateProduct}
          />
        )
      }[id] || '');

    return (
      <div className="container">
        <Header onClick={this.handleClickOpen} />
        <div className="row">
          <div className="col s12 border-primary">
            <Table
              data={products.products}
              pending={products.isLoading}
              handleButtonClick={this.optionsProduct}
            />
          </div>
          <Drawer
            openDrawer={openDrawer}
            aling="left"
            handleClickOpen={this.handleClickOpen}
          >
            <Form
              createProduct={this.createProduct}
              loadProduct={this.loadProduct}
              loadMethods={this.loadMethods}
              methods={products.methods}
              methodsCreate={methodsCreate}
              product={product}
              isLoading={products.isLoading}
              handleClickOpen={this.handleClickOpen2}
            />
          </Drawer>
          <Drawer
            openDrawer={openDrawer2}
            aling="right"
            handleClickOpen={this.handleClickOpen2}
          >
            <Method
              method={method}
              createMethod={this.createMethod}
              loadMethod={this.loadMethod}
            />
          </Drawer>
          <Modal
            id="modalProduct"
            fullscreen
            header={modal.headerModal}
            content={modalSwitch(
              products.isLoading ? 'load' : modal.contentModal
            )}
          />
          <FloatingBtn onClick={id => this.loadFile(id)} />
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  products: state.product
});

export default connect(mapStateToProps)(AppProducts);
