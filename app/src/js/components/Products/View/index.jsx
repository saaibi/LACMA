import React, { useState, Fragment } from "react";

export default ({ data }) => {
  const { todos, setTodos } = useState([]);

  return (
    <Fragment>
      <ul className="collection with-header">
        <li className="collection-header">
          <h4>{data.name}</h4>
          <h5>Metodos</h5>
        </li>
        {_.map(data.methods || [], (method, index) => (
          <Fragment key={index}>
            <li className="collection-item">
              <strong>Nombre: </strong>
              {method.name} <br></br>
              <strong>Area: </strong> {method.area}
              <br></br>
              <strong>Parametro: </strong>
              {method.parameter}
              <br></br>
              <strong>Result: </strong>
              {method.result}
              <br></br>
              <strong>Limite Minimo: </strong>
              {method.limitMin}
              <br></br>
              <strong>Limite Maximo: </strong>
              {method.limitMax}
              <br></br>
              <strong>Descripcion: </strong>
              {method.description}
            </li>
          </Fragment>
        ))}
      </ul>
    </Fragment>
  );
};
