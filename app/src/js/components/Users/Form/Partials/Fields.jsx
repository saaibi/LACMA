import React, { Fragment } from 'react';

import Input from '../../../Common/Input';
import Select from '../../../Common/Select';
import Button from '../../../Common/Button';
import { openModal } from '../../../../utils/dom-utils';

const Fields = ({ user, loadUsers }) => (
  <Fragment>
    <Input
      id="firts_name"
      name="firstName"
      text="Nombres"
      autoFocus={true}
      icon="account_circle"
      classNameIcon="prefix"
      value={user.firstName || ''}
      onChange={loadUsers}
    />

    <Input
      id="last_name"
      name="lastName"
      text="Apellidos"
      icon="assignment_ind"
      classNameIcon="prefix"
      value={user.lastName || ''}
      onChange={loadUsers}
    />

    <Input
      id="username"
      name="username"
      text="Nombre de usuario"
      icon="people"
      classNameIcon="prefix"
      value={user.username || ''}
      onChange={loadUsers}
    />

    <Input
      id="password"
      name="password"
      text="Password"
      type="password"
      icon="vpn_key"
      classNameIcon="prefix"
      value={user.password || ''}
      onChange={loadUsers}
    />
    <Input
      id="email"
      name="email"
      text="Email"
      icon="email"
      classNameIcon="prefix"
      value={user.email || ''}
      onChange={loadUsers}
    />

    <Input
      id="phone"
      name="phone"
      text="Telefono"
      type="number"
      icon="call"
      classNameIcon="prefix"
      value={user.phone || ''}
      onChange={loadUsers}
    />

    <Input
      id="user_id"
      name="user_id"
      text="Cedula"
      type="number"
      icon="fingerprint"
      classNameIcon="prefix"
      value={user.user_id || ''}
      onChange={loadUsers}
    />

    <Select
      id="rol"
      name="rol"
      label="Rol"
      icon="poll"
      defaultText="Selecione una opcion"
      defaultValue={'1'}
      items={[
        { _id: 'analista', name: 'Analista' },
        { _id: 'coordinador', name: 'Coordinador(a)' }
      ]}
      classNameIcon="prefix"
      onChange={loadUsers}
    />

    <div className="input-field col align-center float-initial">
      <Button
        className="waves-effect waves-light blue lighten-1 btn"
        texto="Cargar Firma"
        icon="slow_motion_video"
        classNameIcon="right"
        onClick={() => openModal('madalSignature')}
      />
    </div>
  </Fragment>
);

export default Fields;
