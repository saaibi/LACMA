import React, { Fragment } from "react";

import Button from "../../../Common/Button";

export default ({ createUsers }) => (
  <div className="input-field col s12 align-center">
    <Button
      className="waves-effect waves-light blue lighten-1 btn"
      texto="Guardar"
      icon="send"
      classNameIcon="right"
      onClick={createUsers}
    />
  </div>
);
