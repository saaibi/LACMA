import React, { Component } from 'react';
import _ from 'lodash';
import { connect } from 'react-redux';

import { userActions } from '../../actions/user.actions';

import Form from './Form';
import Table from './Table';
import Edit from './Edit';
import Progress from '../Common/Utils/Progress';
import { toast } from '../../utils/dom-utils';
import Modals from '../Common/Modal';
import Dropzone from '../Results/Signature/Dropzone';

import noImage from '../../../../public/images/no-image.png';

class Users extends Component {
  state = {
    user: {},
    userEdit: {},
    modal: {},
    isLoadEdit: false
  };

  componentDidMount() {
    $('select').formSelect();
    this.props.dispatch(userActions.getAllUsers());
  }

  componentDidUpdate() {
    $('select').formSelect();
  }

  static getDerivedStateFromProps(props, state) {
    const { isLoadEdit } = state;
    const {
      users: { isLoadUser, user }
    } = props;
    if (!!user && !_.isEqual(isLoadUser, isLoadEdit)) {
      return {
        userEdit: { rol: user.rol, phone: user.phone },
        isLoadEdit: isLoadUser
      };
    }
    return null;
  }

  createUsers = e => {
    e.preventDefault();
    const {
      user,
      user: { firstName, lastName, username, password, phone, email, user_id }
    } = this.state;
    const validate = _.every(
      [firstName, lastName, username, password, phone, email, user_id],
      elem => elem && elem !== ''
    );
    if (!validate) {
      return toast({ html: 'Favor completar el formulario' });
    }
    const formData = new FormData();
    _.forIn(user, (value, key) => {
      formData.append(key, value);
    });
    this.props.dispatch(userActions.register(formData));
    this.setState(() => ({ user: {} }));
  };

  setStateSignature = (name, vale) => {
    const { user } = this.state;
    this.setState(() => ({ user: { ...user, [name]: vale } }));
  };

  uploadImage = (name, img) => {
    this.setStateSignature(name, img);
  };

  loadUsers = e => {
    const { name, value } = e.target;
    this.setStateSignature(name, value);
  };

  updateUser = e => {
    e.preventDefault();
    const { userEdit, user_id } = this.state;
    this.props.dispatch(userActions.updateUser(user_id, userEdit));
    $('#modalUsers').modal('close');
  };

  loadUsersEdit = e => {
    const { userEdit } = this.state;
    const { name, value } = e.target;
    this.setState(() => ({
      userEdit: {
        ...userEdit,
        [name]: value
      }
    }));
  };

  optionsUsers = e => {
    const { action, row } = e;
    const { dispatch } = this.props;
    switch (action) {
      case 'delete':
        dispatch(userActions.deleteUser(row._id));
        break;
      case 'edit':
        this.setState(() => ({
          modal: {
            headerModal: 'Editar Usuario',
            contentModal: 'edit'
          },
          user_id: row._id
        }));
        dispatch(userActions.getUserById(row._id));
        $('#modalUsers').modal('open');
        break;
      default:
        break;
    }
  };

  render() {
    const { user, modal, userEdit } = this.state;
    const { users } = this.props;

    // if (_.isEmpty(users.users)) {
    //   return <Progress type="circle" />;
    // }

    return (
      <div className="row">
        <div className="col s12 m4 l3">
          <Form
            createUsers={this.createUsers}
            user={user}
            loadUsers={this.loadUsers}
          />
        </div>
        <div className="col s12 m8 l9 border-primary">
          <Table
            data={users.users}
            pending={users.isLoading}
            handleButtonClick={this.optionsUsers}
          />
        </div>
        <Modals
          id="modalUsers"
          header={modal.headerModal}
          content={
            users.isLoading ? (
              <Progress type="circle" />
            ) : (
              <Edit
                user={userEdit}
                loadUsers={this.loadUsersEdit}
                updateUser={this.updateUser}
              />
            )
          }
        />
        <Modals
          id="madalSignature"
          header="Cargar Firma"
          content={
            <Dropzone
              img={{
                preview: user.signature || noImage,
                name: 'signature_001'
              }}
              onDropAccepted={e => this.uploadImage('signature', e)}
            />
          }
        />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  users: state.users
});

export default connect(mapStateToProps)(Users);
