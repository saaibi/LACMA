import React, { Fragment } from 'react';

import Input from '../../../Common/Input';
import Select from '../../../Common/Select';

export default ({ user, loadUsers }) => (
  <Fragment>
    <Input
      id="phone"
      name="phone"
      text="Telefono"
      type="number"
      icon="call"
      classNameIcon="prefix"
      classNameLabel="active"
      value={user.phone || ''}
      onChange={loadUsers}
    />
    <Select
      id="rol"
      name="rol"
      label="Rol"
      icon="poll"
      defaultText="Selecione una opcion"
      defaultValue={'1'}
      items={[
        { _id: 'analista', name: 'Analista' },
        { _id: 'coordinador', name: 'Coordinador(a)' }
      ]}
      classNameIcon="prefix"
      onChange={loadUsers}
    />
  </Fragment>
);
