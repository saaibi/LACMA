import React from 'react';

import Update from '../../../Common/Button';

export default ({ updateUser }) => (
  <div className="input-field col s6 align-center">
    <Update
      className="waves-effect waves-light blue lighten-1 btn"
      texto="Actualizar"
      icon="save"
      classNameIcon="right"
      onClick={updateUser}
    />
  </div>
);
