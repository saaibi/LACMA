import React, { Component } from 'react';
import { Helmet } from 'react-helmet';

import AppHeader from '../AppHeader';
import Users from '.';

class AppUsers extends Component {
  render() {
    return (
      <div className="container">
        <Helmet titleTemplate="LACMA - %s">
          <title>USUARIOS</title>
        </Helmet>
        <AppHeader name="USUARIOS" />
        <Users />
      </div>
    );
  }
}

export { AppUsers };
