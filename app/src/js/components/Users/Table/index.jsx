import React, { useMemo, useState, useEffect } from 'react';
import DataTable from 'react-data-table-component';

import ActionBarGrid from './Partials/ActionBarGrid';
import ExpandedComponent from './Partials/ExpandedComponent';
import {
  NoData,
  customStyles,
  Progress,
  paginationOptions
} from '../../Common/Utils/Table';

import noImage from '../../../../../public/images/no-image.png';

export default ({ data, pending, handleButtonClick }) => {
  const [items, setItems] = useState([]);

  useEffect(() => {
    const { username } = JSON.parse(localStorage.getItem('user'));
    const filteredItems = data.filter(
      item => item.username != username && item.username != 'admin'
    );
    setItems(filteredItems);
  }, [data]);

  const columns = useMemo(
    () => [
      {
        name: 'Nombre',
        selector: 'firstName',
        cell: row => `${row.firstName} ${row.lastName}`,
        wrap: true,
        sortable: true
      },
      {
        name: 'Telefono',
        selector: 'phone',
        wrap: true,
        sortable: true,
        center: true
      },
      {
        name: 'Email',
        selector: 'email',
        wrap: true,
        sortable: true
      },
      {
        name: 'Rol',
        selector: 'rol',
        sortable: true,
        center: true
      },
      {
        cell: row => (
          <ActionBarGrid row={row} handleButtonClick={handleButtonClick} />
        ),
        name: 'Acciones',
        ignoreRowClick: true,
        allowOverflow: true,
        style: {
          position: 'sticky',
          right: 0
        },
        button: true
      }
    ],
    []
  );

  return (
    <DataTable
      pagination
      highlightOnHover
      striped
      persistTableHead
      expandableRows
      expandOnRowClicked
      columns={columns}
      data={items}
      selectableRows={false}
      progressPending={pending}
      customStyles={customStyles}
      paginationComponentOptions={paginationOptions}
      progressComponent={<Progress />}
      noDataComponent={<NoData />}
      expandableRowsComponent={<ExpandedComponent img={noImage} />}
    />
  );
};
