import React, { Fragment } from 'react';
import Button from '../../../Common/Button';

const ActionBarGrid = ({ row, handleButtonClick }) => (
  <Fragment>
    <Button
      className="btn-floating btn-small pink lighten-2 "
      icon="edit"
      classNameIcon="prefix"
      onClick={e => handleButtonClick({ action: 'edit', row })}
    />

    <Button
      className="btn-floating  btn-small blue lighten-1"
      icon="delete_forever"
      classNameIcon="prefix"
      onClick={e => handleButtonClick({ action: 'delete', row })}
    />
  </Fragment>
);

export default ActionBarGrid;
