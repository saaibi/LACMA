import React, { useState, useEffect } from 'react';

export default ({ data, img }) => {
  const [signature, setSignature] = useState([img]);

  useEffect(() => {
    const { img } = data;
    if (img) {
      const blob = new Blob([new Uint8Array(img.data.data)], {
        type: img.contentType
      });
      const imageUrl = window.URL.createObjectURL(blob);
      setSignature(imageUrl);
    }
  }, [img]);

  return (
    <ul className="collection with-header">
      <li className="collection-header">
        <h4>Usuario: {`${data.firstName} ${data.lastName}`}</h4>
      </li>
      <li className="collection-item">
        <strong>Email: </strong>
        {data.email} <br></br>
        <strong>Telefono: </strong>
        {data.phone} <br></br>
        <strong>Cedula: </strong>
        {data.user_id}
        <br></br>
        <strong>User ID: </strong>
        {data.username}
      </li>
      <li className="collection-item">
        <img src={signature} style={{ width: 326, height: 73 }} alt="noImage" />
      </li>
    </ul>
  );
};
