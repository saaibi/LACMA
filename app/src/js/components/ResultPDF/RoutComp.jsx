import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import _ from 'lodash';
import ResizeDetector from 'react-resize-detector';
import { Helmet } from 'react-helmet';

import FloatingBtnBar from './Partials/FloatingBtnBar';
import AppHeader from '../AppHeader';
import Modal from '../Common/Modal';
import Form from './Form';

import {
  screenSize,
  openModal,
  toast,
  closeModal
} from '../../utils/dom-utils';

import Progress from '../Common/Utils/Progress';
import { sampleActions } from '../../actions/sample.actions';
import { resultActions } from '../../actions/result.actions';
import ReCount from '../Results/Count';

// import firmaA from '../../../../public/images/signatures/analista.jpg';
// import firmaC from '../../../../public/images/signatures/coordinador.jpg';
import Button from '../Common/Button';

class AppResultPDF extends PureComponent {
  state = {
    width: 0,
    height: 0,
    dataResult: {
      firmaA: '',
      firmaC: '',
    },
    modal: {}
  };

  componentDidMount() {
    const { dispatch } = this.props;
    const { _id } = JSON.parse(localStorage.getItem('user'));
    dispatch(sampleActions.getAllSamplesByAnaly({ _id }));
    $('.fixed-action-btn').floatingActionButton({ hoverEnabled: true });
  }

  componentDidUpdate() {
    // $('select').formSelect();
    $('.fixed-action-btn').floatingActionButton({ hoverEnabled: true });
    $('.tooltipped').tooltip({ delay: 50 });
  }

  setStateDataRes = (name, vale) => {
    this.setState(({ dataResult }) => ({
      dataResult: { ...dataResult, [name]: vale }
    }));
  };

  handleClick = async (id, dataModal) => {
    this.setState(() => ({ modal: dataModal }));
    openModal('modalResult');
  };

  handleClickPDF = () => {
    const { dataResult } = this.state;
    const { dispatch } = this.props;
    if (!dataResult.sample) {
      toast({ html: 'Selecciona una muestra' });
    } else if (!dataResult.conclusion) {
      toast({ html: 'Ingresa la Conclusión' });
    } else {
      closeModal('modalResult');
      const formData = new FormData();
      _.forIn(dataResult, (value, key) => {
        formData.append(key, value);
      });
      dispatch(resultActions.createPDF(formData));
    }
  };

  onResize = width => {
    this.setState(() => ({ width, height: screenSize().height }));
  };

  render() {
    const { height, dataResult, modal } = this.state;
    const { results, samples } = this.props;

    const modalSwitch = id =>
      ({
        calculos: <ReCount />,
        sample: (
          <Form
            data={dataResult}
            samples={samples}
            setStateDataRes={this.setStateDataRes}
            handleClickPDF={this.handleClickPDF}
          />
        )
      }[id] || '');

    return (
      <div className="container">
        <Helmet titleTemplate="LACMA - %s">
          <title>
            {results.isLoading ? 'Creado PDF....' : 'RESULTADOS PDF'}
          </title>
        </Helmet>
        <AppHeader
          name="RESULTADOS PDF"
          floatingContent={<FloatingBtnBar onClick={this.handleClick} />}
        />
        {results.isLoading && <Progress type="cilcular" />}
        <div className="input-field col s12 align-center">
          <Button
            className="waves-effect waves-light blue lighten-1 btn"
            texto="Seleccionar muestra para crear PDF"
            icon="assignment"
            classNameIcon="left"
            disabled={results.isLoading}
            onClick={() =>
              this.handleClick('modalSample', {
                header: 'Generar PDF',
                fullscreen: true,
                contentModal: 'sample'
              })
            }
          />
        </div>
        <object
          src="about:blank"
          id="resultObject"
          style={{ width: ' 100%', height: height }}
        ></object>
        <Modal
          id="modalResult"
          {...modal}
          content={modalSwitch(modal.contentModal)}
        />
        <ResizeDetector handleWidth handleHeight onResize={this.onResize} />
      </div>
    );
  }
}
const mapStateToProps = state => ({
  results: state.result,
  samples: state.sample
});

const connectedResult = connect(mapStateToProps)(AppResultPDF);
export { connectedResult as AppResultPDF };
