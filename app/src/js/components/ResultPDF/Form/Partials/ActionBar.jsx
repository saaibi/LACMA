import React, { Fragment } from 'react';

import Button from '../../../Common/Button';

export default ({ handleClickPDF, setStateDataRes }) => (
  <Fragment>
    {/* <div className="input-field col s12 m4 align-center">
      <Button
        className="waves-effect waves-light red lighten-1 btn"
        texto="Borrar Firma Analista"
        icon="delete_forever"
        classNameIcon="left"
        onClick={() => setStateDataRes('firmaA', '')}
      />
    </div> */}
    <div className="input-field col s12 align-center">
      <Button
        className="waves-effect waves-light blue lighten-1 btn"
        texto="Generar PDF"
        icon="send"
        classNameIcon="right"
        onClick={handleClickPDF}
      />
    </div>
    {/* <div className="input-field col s12 m4 align-center">
      <Button
        className="waves-effect waves-light red lighten-1 btn"
        texto="Borrar Firma Coordinador"
        icon="delete_forever"
        classNameIcon="left"
        onClick={() => setStateDataRes('firmaC', '')}
      />
    </div> */}
  </Fragment>
);
