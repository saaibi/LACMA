import React, { Fragment } from 'react';
import SelectInput from 'react-select';
import makeAnimated from 'react-select/animated';

import { defaultConfig } from '../../../Common/Select/selecttInput';
import Dropzone from '../../../Results/Signature/Dropzone';
import noImage from '../../../../../../public/images/no-image.png';
import Input from '../../../Common/Input';

const animatedComponents = makeAnimated();

const customStyles = {
  container: provided => ({
    ...provided,
    zIndex: 2,
    marginLeft: '45px'
  })
};

const Fields = ({ samples, data, setStateDataRes }) => (
  <Fragment>
    <div className="input-field col s12">
      <i className="material-icons prefix">blur_on</i>
      <SelectInput
        {...defaultConfig}
        styles={customStyles}
        className="basic-single select_methods"
        components={animatedComponents}
        defaultValue={!_.isEmpty(samples.sample) ? samples.sample : false}
        getOptionLabel={op => op.code}
        name="sample"
        id="sample_1"
        options={samples.samples}
        onChange={(data, { name }) =>
          setStateDataRes(name, data ? data._id : null)
        }
      />
      <label htmlFor="sample_1" className="active">
        Selecione una Muestra
      </label>
    </div>
    <div className="input-field col s12 m6">
      <Dropzone
        img={{ preview: data.firmaA || noImage, name: 'analista_001' }}
        onDropAccepted={img => setStateDataRes('firmaA', img)}
      />
      <Input
        id="name_analista"
        name="nameA"
        text="Nombre Analista"
        autoFocus={true}
        icon="assignment_ind"
        classNameIcon="prefix"
        helper="Firma y Nombre Opcional"
        className=""
        value={data.nameA || ''}
        onChange={({ target: { name, value } }) => setStateDataRes(name, value)}
      />
    </div>
    <div className="input-field col s12 m6">
      <Dropzone
        img={{ preview: data.firmaC || noImage, name: 'coordinador@_001' }}
        onDropAccepted={img => setStateDataRes('firmaC', img)}
      />
      <Input
        id="name_coordinador"
        name="nameC"
        text="Nombre Coordinador"
        autoFocus={true}
        icon="assignment_ind"
        helper="Firma y Nombre Opcional"
        classNameIcon="prefix"
        className=""
        value={data.nameC || ''}
        onChange={({ target: { name, value } }) => setStateDataRes(name, value)}
      />
    </div>
    <div className="input-field col s12 m6">
      <i className="material-icons prefix">mode_edit</i>
      <textarea
        id="conclusion_id"
        name="conclusion"
        className="materialize-textarea"
        value={data.conclusion || ''}
        onChange={({ target: { name, value } }) => setStateDataRes(name, value)}
      />
      <label htmlFor="conclusion_id" className="active">
        Conclusión
      </label>
    </div>
    <div className="input-field col s12 m6">
      <i className="material-icons prefix">sort</i>
      <textarea
        id="suggestions_id"
        name="suggestions"
        className="materialize-textarea"
        value={data.suggestions || ''}
        onChange={({ target: { name, value } }) => setStateDataRes(name, value)}
      />
      <label htmlFor="suggestions_id" className="active">
        Sugerencias
      </label>
    </div>
  </Fragment>
);

export default Fields;
