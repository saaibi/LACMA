import React from 'react';

export default React.memo(({ onClick, disabled }) => {
  return (
    <div className="nav-content">
      <a
        data-delay="50"
        data-position="top"
        data-tooltip="Generar PDF"
        className="btn-floating btn-large halfway-fab amber tooltipped"
        style={{ left: '10px' }}
        onClick={() =>
          onClick('modalSample', {
            header: 'Generar PDF',
            fullscreen: true,
            contentModal: 'sample'
          })
        }
      >
        <i className="material-icons">assignment</i>
      </a>
      <a
        data-delay="50"
        data-position="top"
        data-tooltip="Recuento de microorganismos"
        className="btn-floating btn-large halfway-fab red tooltipped"
        style={{ left: '70px' }}
        onClick={() =>
          onClick('modalCalculos', {
            header: 'Recuento de microorganismos',
            contentModal: 'calculos',
            fullscreen: true
          })
        }
      >
        <i className="material-icons">gradient</i>
      </a>
      <a
        id="downloadLink"
        data-position="top"
        data-tooltip="Descargar PDF"
        download="LAC-PS-FR-033.pdf"
        style={{ left: '130px' }}
        disabled={true}
        className="btn-floating btn-large halfway-fab waves-effect red tooltipped"
      >
        <i className="material-icons">file_download</i>
      </a>
    </div>
  );
});
