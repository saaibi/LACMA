import React from 'react';

export default React.memo(({ onClick }) => {
  return (
    <div className="fixed-action-btn">
      <a className="btn-floating btn-large waves-effect waves-light">
        <i className="large material-icons">add</i>
      </a>
      <ul>
        <li>
          <a
            data-delay="50"
            data-position="right"
            data-tooltip="Generar PDF"
            onClick={() =>
              onClick('modalSample', {
                header: 'Generar PDF',
                fullscreen: true,
                contentModal: 'sample'
              })
            }
            className="btn-floating amber tooltipped"
          >
            <i className="material-icons">assignment</i>
          </a>
        </li>
        <li>
          <a
            data-delay="50"
            data-position="right"
            data-tooltip="Recuento de microorganismos"
            className="btn-floating red tooltipped"
            onClick={() =>
              onClick('modalCalculos', {
                header: 'Recuento de microorganismos',
                contentModal: 'calculos',
                fullscreen: true
              })
            }
          >
            <i className="material-icons">gradient</i>
          </a>
        </li>
      </ul>
    </div>
  );
});
