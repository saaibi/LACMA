import React, { PureComponent, Fragment } from 'react';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';

import { methodActions } from '../../actions/method.actions';

import Form from '../Products/Method';
import Table from './Table';
import Edit from './Edit';
import Modals from '../Common/Modal';
import Progress from '../Common/Utils/Progress';
import { toast } from '../../utils/dom-utils';
import { swalAlert, alertDelete } from '../../utils/alert';
import FloatingBtnBar from './Partials/FloatingBtnBar';
import Drawer from '../Common/Drawer';
import AppHeader from '../AppHeader';

const Header = React.memo(({ onClick }) => (
  <Fragment>
    <Helmet titleTemplate="LACMA - %s">
      <title>METODOS</title>
    </Helmet>
    <AppHeader
      name="METODOS"
      floatingContent={<FloatingBtnBar onClick={onClick} />}
    />
  </Fragment>
));

class AppMethods extends PureComponent {
  state = {
    method: {},
    methodEdit: {},
    modal: {},
    isLoadEdit: false,
    openDrawer: false
  };

  componentDidMount() {
    this.props.dispatch(methodActions.getAllMethods());
  }

  componentDidUpdate() {
    $('select').formSelect();
    $('.tooltipped').tooltip({ delay: 50 });
  }

  static getDerivedStateFromProps(props, state) {
    const { isLoadEdit } = state;
    const {
      products: { isLoadMethod, method }
    } = props;
    if (!!method && !_.isEqual(isLoadMethod, isLoadEdit)) {
      return {
        methodEdit: method,
        isLoadEdit: isLoadMethod
      };
    }
    return null;
  }

  createMethod = e => {
    e.preventDefault();
    const { method } = this.state;
    if (!method.name) {
      return toast({ html: 'Nombre del Metodo es requerido' });
    }
    this.props.dispatch(methodActions.createMethod(method));
    this.setState(({ openDrawer }) => ({
      method: {},
      openDrawer: !openDrawer
    }));
  };

  loadMethod = e => {
    const { method } = this.state;
    const { name, value } = e.target;
    this.setState({
      method: {
        ...method,
        [name]: value
      }
    });
  };

  updateMethod = e => {
    e.preventDefault();
    const { methodEdit, method_id } = this.state;
    delete methodEdit._id;
    this.props.dispatch(methodActions.updateMethod(method_id, methodEdit));
    $('#modalMethod').modal('close');
  };

  loadMethodEdit = e => {
    const { methodEdit } = this.state;
    const { name, value } = e.target;
    this.setState(() => ({
      methodEdit: {
        ...methodEdit,
        [name]: value
      }
    }));
  };

  optionsMethods = async e => {
    const { action, row, rows } = e;
    const { dispatch } = this.props;
    switch (action) {
      case 'delete':
        const { value: deleteOne } = await swalAlert(alertDelete());
        if (deleteOne) dispatch(methodActions.deleteMethod([row._id]));
        break;
      case 'deleteAll':
        const { value: deleteMany } = await swalAlert(alertDelete());
        if (deleteMany) dispatch(methodActions.deleteMethod(rows));
        break;
      case 'edit':
        this.setState(() => ({
          modal: {
            headerModal: 'Editar Metodo',
            contentModal: 'edit'
          },
          method_id: row._id
        }));
        dispatch(methodActions.getMethodById(row._id));
        $('#modalMethod').modal('open');
        break;
      default:
        break;
    }
  };

  handleClickOpen = () => {
    this.setState(state => ({ openDrawer: !state.openDrawer }));
  };

  render() {
    const { products } = this.props;
    const { method, modal, methodEdit, openDrawer } = this.state;

    return (
      <div className="container">
        <Header onClick={this.handleClickOpen} />
        <div className="row">
          <div className="col s12 border-primary">
            <Table
              data={products.methods}
              pending={products.isLoading}
              handleButtonClick={this.optionsMethods}
            />
          </div>
          <Drawer
            openDrawer={openDrawer}
            handleClickOpen={this.handleClickOpen}
          >
            <Form
              method={method}
              createMethod={this.createMethod}
              loadMethod={this.loadMethod}
            />
          </Drawer>
          <Modals
            id="modalMethod"
            header={modal.headerModal}
            content={
              products.isLoading ? (
                <Progress type="circle" />
              ) : (
                <Edit
                  method={methodEdit}
                  loadMethod={this.loadMethodEdit}
                  updateMethod={this.updateMethod}
                />
              )
            }
          />
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  products: state.product
});

export default connect(mapStateToProps)(AppMethods);
