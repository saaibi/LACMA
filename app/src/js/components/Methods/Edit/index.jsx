import React, { Fragment } from 'react';

import Fields from './Partials/Fields';
import ActionBarEdit from './Partials/ActionBarEdit';

export default props => (
  <Fragment>
    <Fields loadMethod={props.loadMethod} method={props.method} />
    <ActionBarEdit updateMethod={props.updateMethod} />
  </Fragment>
);
