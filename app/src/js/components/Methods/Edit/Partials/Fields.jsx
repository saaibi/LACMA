import React, { Fragment } from 'react';

import Input from '../../../Common/Input';

export default ({ method, loadMethod }) => (
  <Fragment>
    <Input
      id="name_method_1"
      name="name"
      text="Nombre"
      autoFocus={true}
      icon="assignment_ind"
      classNameIcon="prefix"
      value={method.name || ''}
      onChange={loadMethod}
    />
    <Input
      id="area_method_1"
      name="area"
      text="Area"
      icon="insert_drive_file"
      classNameIcon="prefix"
      value={method.area || ''}
      onChange={loadMethod}
    />
    <Input
      id="parameter_1"
      name="parameter"
      text="Parametro"
      icon="description"
      classNameIcon="prefix"
      value={method.parameter || ''}
      onChange={loadMethod}
    />
    <Input
      id="result_1"
      name="result"
      text="Resultado"
      icon="assessment"
      classNameIcon="prefix"
      value={method.result || ''}
      onChange={loadMethod}
    />
    <Input
      id="limit_min_1"
      name="limitMin"
      text="Limite Minimo"
      icon="remove"
      classNameIcon="prefix"
      value={method.limitMin || ''}
      onChange={loadMethod}
    />
    <Input
      id="limit_max_1"
      name="limitMax"
      text="Limite Maximo"
      icon="add"
      classNameIcon="prefix"
      value={method.limitMax || ''}
      onChange={loadMethod}
    />
    <Input
      id="description_2"
      name="description"
      text="Description"
      icon="art_track"
      classNameIcon="prefix"
      value={method.description || ''}
      onChange={loadMethod}
    />
  </Fragment>
);
