import React, { useMemo, useState, useCallback, useEffect } from 'react';
import DataTable from 'react-data-table-component';
import memoize from 'memoize-one';

import ActionBarGrid from './Partials/ActionBarGrid';
import ExpandedComponent from './Partials/ExpandedComponent';
import {
  NoData,
  Progress,
  customStyles,
  paginationOptions,
  defaultConfigTable,
  FilterComponent
} from '../../Common/Utils/Table';
import Button from '../../Common/Button';

const contextActions = memoize(deleteHandler => (
  <Button
    className="btn-floating waves-effect waves-light red"
    icon="delete_forever"
    classNameIcon="prefix"
    onClick={deleteHandler}
  />
));

export default ({ data, pending, handleButtonClick }) => {
  const [items, setItems] = useState([]);
  const [filterText, setFilterText] = useState('');
  const [resetPagiTog, setResetPagiTog] = useState(false);

  const handleClear = () => {
    if (filterText) {
      setResetPagiTog(!resetPagiTog);
      setFilterText('');
    }
  };

  const subHeaderComponentMemo = useMemo(() => {
    return (
      <FilterComponent
        onFilter={e => setFilterText(e.target.value)}
        onClear={handleClear}
        filterText={filterText}
      />
    );
  }, [filterText, resetPagiTog]);

  useEffect(() => {
    $('select').formSelect();
    const dataFilter = _.filter(
      data,
      i =>
        i.name.toLowerCase().includes(filterText.toLowerCase()) ||
        i.parameter.toLowerCase().includes(filterText.toLowerCase()) ||
        i.area.toLowerCase().includes(filterText.toLowerCase())
    );
    setItems(dataFilter);
  }, [filterText, data]);

  const columns = useMemo(
    () => [
      {
        name: 'Metodo',
        selector: 'name',
        wrap: true,
        center: true,
        sortable: true
      },
      {
        name: 'Area',
        selector: 'area',
        center: true,
        wrap: true,
        sortable: true
      },
      {
        name: 'Parámetro',
        selector: 'parameter',
        wrap: true,
        sortable: true
      },
      {
        cell: row => (
          <ActionBarGrid row={row} handleButtonClick={handleButtonClick} />
        ),
        name: 'Acciones',
        ignoreRowClick: true,
        allowOverflow: true,
        style: {
          position: 'sticky',
          right: 0
        },
        button: true
      }
    ],
    []
  );

  const [selectedRows, setSelectedRows] = useState([]);
  const [toggleCleared, setToggleCleared] = useState(false);
  const handleChange = useCallback(
    ({ allSelected, selectedCount, selectedRows }) => {
      const resData = _.map(selectedRows, '_id');
      setSelectedRows(resData);
    },
    []
  );
  const deleteAll = () => {
    handleButtonClick({ action: 'deleteAll', rows: selectedRows });
    setToggleCleared(!toggleCleared);
    handleClear();
  };

  return (
    <DataTable
      {...defaultConfigTable}
      pagination
      highlightOnHover
      striped
      subHeader
      persistTableHead
      expandableRows
      selectableRows
      expandOnRowClicked
      columns={columns}
      data={items}
      progressPending={pending}
      customStyles={customStyles}
      clearSelectedRows={toggleCleared}
      onSelectedRowsChange={handleChange}
      paginationResetDefaultPage={resetPagiTog}
      contextActions={contextActions(deleteAll)}
      subHeaderComponent={subHeaderComponentMemo}
      paginationComponentOptions={paginationOptions}
      noDataComponent={<NoData />}
      progressComponent={<Progress />}
      expandableRowsComponent={<ExpandedComponent />}
    />
  );
};
