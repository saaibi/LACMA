import React, { Fragment } from 'react';
import PropTypes from 'prop-types';

import Button from '../../../Common/Button';

const ActionBarGrid = ({ row, handleButtonClick }) => (
  <Fragment>
    <Button
      className="btn-floating btn-small waves-effect waves-light blue lighten-1"
      icon="edit"
      classNameIcon="prefix"
      onClick={e => handleButtonClick({ action: 'edit', row })}
    />

    <Button
      className="btn-floating btn-small waves-effect waves-light red"
      icon="delete_forever"
      classNameIcon="prefix"
      onClick={e => handleButtonClick({ action: 'delete', row })}
    />
  </Fragment>
);

ActionBarGrid.propTypes = {
  id: PropTypes.string,
  onEdit: PropTypes.func,
  onDelete: PropTypes.func,
  onView: PropTypes.func
};

export default ActionBarGrid;
