import React from 'react';

// eslint-disable-next-line react/prop-types
export default ({ data }) => (
  <ul className="collection">
    <li className="collection-item">
      <strong>Parametro: </strong>
      {data.parameter}
    </li>
    <li className="collection-item">
      <strong>Resultado: </strong>
      {data.result}
    </li>
    <li className="collection-item">
      <strong>Limite Mínimo: </strong>
      {data.limitMin}
    </li>
    <li className="collection-item">
      <strong>Limite Máximo: </strong>
      {data.limitMax}
    </li>
    <li className="collection-item">
      <strong>Descripción: </strong>
      {data.description}
    </li>
  </ul>
);
