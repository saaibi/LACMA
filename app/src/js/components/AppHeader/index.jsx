import React from 'react';

import Marca from './Partials/Marca';

const AppHeader = ({ name, floatingContent }) => (
  <nav className="nav-extended">
    <div className="nav-wrapper">
      <Marca name={name} />
    </div>
    {floatingContent}
  </nav>
);

AppHeader.defaultProps = {
  floatingContent: ''
};

export default AppHeader;
