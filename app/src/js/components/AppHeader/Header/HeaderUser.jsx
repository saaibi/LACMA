import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

import background from '../../../../../public/images/office.jpg';
import profile from '../../../../../public/images/yuna.jpg';
import { Badge, Typography } from '@material-ui/core';

const HeaderUser = ({ menu, todaySamples }) => {
  if (menu === 'sidenav') {
    return (
      <ul id="slide-out" className="sidenav">
        <li>
          <div className="user-view">
            <div className="background">
              <img src={background} />
            </div>
            <Link to="/">
              <img className="circle" src={profile} />
            </Link>
            <a href="#name">
              <span className="white-text name">Lacma</span>
            </a>
            <a href="#email">
              <span className="white-text email">lacma@gmail.com</span>
            </a>
          </div>
        </li>
        <li>
          <Link to="/clients" className="waves-effect">
            <i className="material-icons">business_center</i>Clientes
          </Link>
        </li>
        <li>
          <Link to="/products">
            <i className="material-icons">card_travel</i>Productos
          </Link>
        </li>
        <li>
          <Link to="/methods">
            <i className="material-icons">slow_motion_video</i>Metodos
          </Link>
        </li>
        <li>
          <Link to="/results">
            <i className="material-icons">poll</i>Resultados
          </Link>
        </li>
        <li>
          <Link to="/samples" className="waves-effect">
            <i className="material-icons">content_paste</i>Crear Muestra
          </Link>
        </li>
        <li>
          <Link to="/viewsamples" className="waves-effect">
            <i className="material-icons">content_paste</i>Ver Muestras
          </Link>
        </li>
        <li>
          <Link to="/users">
            <i className="material-icons">group</i>Usuarios
          </Link>
        </li>
        <li>
          <Link to="/login">
            <i className="material-icons">system_update_alt</i>Cerrar sesión
          </Link>
        </li>
      </ul>
    );
  }
  if (menu === 'nav') {
    return (
      <ul className="right hide-on-med-and-down">
        <ul id="dropdown2" className="dropdown-content">
          <li>
            <Link to="/viewsamples">Ver</Link>
          </li>
          {/* <li className="divider"></li>
          <li>
            <Link to="/viewsamples" onClick={() => onClick('today')}>
              <Badge badgeContent={4} color="error">
                <Typography>Para Hoy</Typography>
              </Badge>
            </Link>
          </li> */}
          <li className="divider"></li>
          <li>
            <Link to="/samples">Crear</Link>
          </li>
        </ul>
        <li>
          <Link to="/clients">Clientes</Link>
        </li>
        <li>
          <Link to="/products">Productos</Link>
        </li>
        <li>
          <Link to="/methods">Metodos</Link>
        </li>
        <li>
          <Link to="/results">Resultados</Link>
        </li>
        <li>
          <a className="dropdown-trigger" href="#!" data-target="dropdown2">
            <Badge badgeContent={todaySamples || 0} color="error" showZero>
              <Typography>Muestras</Typography>
            </Badge>
            <i className="material-icons right">arrow_drop_down</i>
          </a>
        </li>
        <li>
          <Link to="/users">Usuarios</Link>
        </li>
        <li>
          <Link to="/login">Cerrar sesión</Link>
        </li>
      </ul>
    );
  }
};

HeaderUser.defaultProps = {};

HeaderUser.propTypes = {
  menu: PropTypes.oneOf(['nav', 'sidenav'])
};

export default HeaderUser;
