import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import HeaderNoUser from './HeaderNoUser';
import HeaderUser from './HeaderUser';

class Header extends Component {
  componentDidMount() {
    $('.dropdown-trigger').dropdown();
  }

  componentDidUpdate() {
    $('.dropdown-trigger').dropdown();
  }

  componentWillMount() {
    $('.dropdown-trigger').dropdown();
  }

  render() {
    const { name, isLoggedIn, todaySamples } = this.props;
    return (
      <div>
        <nav className="">
          <div className="nav-wrapper">
            <Link to="/" className="brand-logo">
              {name}
            </Link>
            <a href="#" data-target="slide-out" className="sidenav-trigger">
              <i className="material-icons">menu</i>
            </a>
            {isLoggedIn ? (
              <HeaderUser menu="nav" todaySamples={todaySamples} />
            ) : (
              <HeaderNoUser menu="nav" />
            )}
          </div>
        </nav>
        <p />
        {isLoggedIn ? (
          <HeaderUser menu="sidenav" />
        ) : (
          <HeaderNoUser menu="sidenav" />
        )}
      </div>
    );
  }
}

export default Header;
