import React, { Fragment } from 'react';

import Input from '../../../Common/Input';

export default ({ client, loadClient }) => (
  <Fragment>
    <Input
      id="company"
      name="company"
      text="Empresa Solicitante"
      autoFocus={true}
      icon="closed_caption"
      classNameIcon="prefix"
      value={client.company || ''}
      onChange={loadClient}
    />

    <Input
      id="nit"
      name="nit"
      text="Nit"
      icon="fingerprint"
      classNameIcon="prefix"
      value={client.nit || ''}
      onChange={loadClient}
    />
    <Input
      id="contact"
      name="contact"
      text="Contacto"
      icon="perm_contact_calendar"
      classNameIcon="prefix"
      value={client.contact || ''}
      onChange={loadClient}
    />
    <Input
      id="phone"
      name="phone"
      text="Telefono"
      icon="contact_phone"
      classNameIcon="prefix"
      value={client.phone || ''}
      onChange={loadClient}
    />
    <Input
      id="extension"
      type="number"
      name="extension"
      text="Extensión"
      icon="local_phone"
      classNameIcon="prefix"
      value={client.extension || ''}
      onChange={loadClient}
    />
    <Input
      id="email"
      name="email"
      text="E-mail"
      icon="email"
      classNameIcon="prefix"
      value={client.email || ''}
      onChange={loadClient}
    />
    <Input
      id="fax"
      name="fax"
      text="Fax"
      icon="print"
      classNameIcon="prefix"
      value={client.fax || ''}
      onChange={loadClient}
    />
    <Input
      id="address"
      name="address"
      text="Dirección"
      icon="location_on"
      classNameIcon="prefix"
      value={client.address || ''}
      onChange={loadClient}
    />
  </Fragment>
);
