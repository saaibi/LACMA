import React from "react";

import Button from "../../../Common/Button";

export default ({ createClient }) => (
  <div className="input-field col s12 align-center">
    <Button
      className="waves-effect waves-light blue lighten-1 btn"
      texto="Guardar"
      icon="send"
      classNameIcon="right"
      onClick={createClient}
    />
  </div>
);