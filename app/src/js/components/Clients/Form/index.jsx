import React from 'react';

import Fields from './Partials/Fields';
import ActionBar from './Partials/ActionBar';

export default props => (
  <div className="row">
    <Fields {...props} />
    <ActionBar {...props} />
  </div>
);
