import React, { Fragment } from 'react';

import Fields from './Partials/Fields';
import ActionBarEdit from './Partials/ActionBarEdit';

export default props => (
  <Fragment>
    <Fields loadClient={props.loadClient} client={props.client} />
    <ActionBarEdit updateClient={props.updateClient} />
  </Fragment>
);
