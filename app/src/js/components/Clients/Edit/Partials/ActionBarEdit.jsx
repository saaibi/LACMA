import React from 'react';

import Update from '../../../Common/Button';

const ActionBarEdit = props => (
  <div className="input-field col s6 align-center">
    <Update
      className="waves-effect waves-light blue lighten-1 btn"
      texto="Actualizar"
      icon="update"
      classNameIcon="right"
      onClick={props.updateClient}
    />
  </div>
);

export default ActionBarEdit;
