import React, { Fragment } from 'react';

import Input from '../../../Common/Input';

const Fields = ({ client, loadClient }) => (
  <Fragment>
    <Input
      id="company"
      name="company"
      value={client.company || ''}
      text="Empresa Solicitante"
      autoFocus={true}
      icon="account_circle"
      classNameIcon="prefix"
      onChange={e => loadClient(e)}
    />

    <Input
      id="contact"
      name="contact"
      value={client.contact || ''}
      text="Contacto"
      icon="perm_identity"
      classNameIcon="prefix"
      onChange={e => loadClient(e)}
    />
    <Input
      id="phone"
      name="phone"
      text="Telefono"
      value={client.phone || ''}
      icon="contact_phone"
      classNameIcon="prefix"
      onChange={e => loadClient(e)}
    />
    <Input
      id="address"
      name="address"
      value={client.address || ''}
      text="Dirección"
      icon="location_on"
      classNameIcon="prefix"
      onChange={e => loadClient(e)}
    />
    <Input
      id="extension"
      name="extension"
      value={client.extension || ''}
      text="Extension"
      icon="local_phone"
      classNameIcon="prefix"
      onChange={e => loadClient(e)}
    />
    <Input
      id="fax"
      name="fax"
      text="Fax"
      value={client.fax || ''}
      icon="print"
      classNameIcon="prefix"
      onChange={e => loadClient(e)}
    />
  </Fragment>
);

export default Fields;
