import React, { PureComponent, Fragment } from 'react';
import _ from 'lodash';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';

import { clientActions } from '../../actions/client.actions';

import Form from './Form';
import Drawer from '../Common/Drawer';
import Modal from '../Common/Modal';
import Progress from '../Common/Utils/Progress';
import { toast } from '../../utils/dom-utils';
import Table from './Table';
import { swalAlert, alertDelete } from '../../utils/alert';
import AppHeader from '../AppHeader';
import FloatingBtnBar from './Partials/FloatingBtnBar';

const Header = React.memo(({ onClick }) => (
  <Fragment>
    <Helmet titleTemplate="LACMA - %s">
      <title>ClIENTES</title>
    </Helmet>
    <AppHeader
      name="ClIENTES"
      floatingContent={<FloatingBtnBar onClick={onClick} />}
    />
  </Fragment>
));

class AppClient extends PureComponent {
  constructor(props) {
    super(props);
  }

  state = {
    client: {},
    clientEdit: {},
    modal: {},
    isLoadEdit: false,
    openDrawer: false
  };

  componentDidMount() {
    this.props.dispatch(clientActions.getAllClient());
  }

  componentDidUpdate() {
    $('select').formSelect();
    $('.tooltipped').tooltip({ delay: 50 });
  }

  static getDerivedStateFromProps(props, state) {
    const { clientEdit, isLoadEdit } = state;
    const { clients } = props;
    if (
      !!clients.client &&
      !_.isEqual(clients.client._id, clientEdit._id) &&
      !_.isEqual(clients.isLoadClient, isLoadEdit)
    ) {
      return {
        clientEdit: clients.client,
        isLoadEdit: clients.isLoadClient
      };
    }
    return null;
  }

  createClient = e => {
    e.preventDefault();
    const {
      client,
      client: { company, contact, nit, email }
    } = this.state;
    const validate = _.every(
      [company, contact, nit, email],
      elem => elem && elem !== ''
    );
    if (!validate) {
      return toast({ html: 'Favor completar el formulario' });
    }
    this.props.dispatch(clientActions.createClient(client));
    this.setState(({ openDrawer }) => ({
      client: {},
      openDrawer: !openDrawer
    }));
  };

  updateClient = e => {
    e.preventDefault();
    const { clientEdit, client_id } = this.state;
    delete clientEdit._id;
    this.props.dispatch(clientActions.updateClient(client_id, clientEdit));
    $('#modalClient').modal('close');
  };

  loadClient = event => {
    const { client } = this.state;
    const { value, name } = event.target;
    this.setState({
      client: {
        ...client,
        [name]: value
      }
    });
  };

  loadClientEdit = event => {
    const { clientEdit } = this.state;
    const { value, name } = event.target;
    this.setState({
      clientEdit: {
        ...clientEdit,
        [name]: value
      }
    });
  };

  optionsClient = async e => {
    const { action, row } = e;
    const { dispatch } = this.props;
    switch (action) {
      case 'delete':
        const { value: deleteOne } = await swalAlert(alertDelete());
        if (deleteOne) dispatch(clientActions.deleteClient(row._id));
        break;
      case 'edit':
        this.setState(() => ({
          modal: {
            headerModal: 'Editar Cliente',
            contentModal: 'edit'
          },
          client_id: row._id
        }));
        dispatch(clientActions.getById(row._id));
        $('#modalClient').modal('open');
        break;
      default:
        break;
    }
  };

  handleClickOpen = () => {
    this.setState(state => ({ openDrawer: !state.openDrawer }));
  };

  render() {
    const { modal, openDrawer, client, clientEdit } = this.state;
    const { clients } = this.props;

    // if (_.isEmpty(clients.clients)) {
    //   return <Progress type="circle" />;
    // }

    const modalSwitch = id =>
      ({
        loading: <Progress type="circle" />,
        edit: (
          <Form
            client={clientEdit}
            createClient={this.updateClient}
            loadClient={this.loadClientEdit}
          />
        )
      }[id] || '');

    return (
      <div className="container">
        <Header onClick={this.handleClickOpen} />
        <div className="col s12 border-primary">
          <Table
            data={clients.clients}
            pending={clients.isLoading}
            handleButtonClick={this.optionsClient}
          />
        </div>
        <Drawer openDrawer={openDrawer} handleClickOpen={this.handleClickOpen}>
          <Form
            client={client}
            createClient={this.createClient}
            loadClient={this.loadClient}
          />
        </Drawer>
        <Modal
          id="modalClient"
          header={modal.headerModal}
          content={modalSwitch(
            clients.isLoading ? 'loading' : modal.contentModal
          )}
        />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  clients: state.client,
  clientCredit: state.client.clientCredit
});

export default connect(mapStateToProps)(AppClient);
