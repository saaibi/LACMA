import React, { useMemo } from 'react';
import DataTable from 'react-data-table-component';

import ActionBarGrid from './Partials/ActionBarGrid';
import {
  NoData,
  customStyles,
  Progress,
  paginationOptions
} from '../../Common/Utils/Table';
import ExpandedComponent from './Partials/ExpandedComponent';

export default ({ data, pending, handleButtonClick }) => {
  const columns = useMemo(
    () => [
      {
        name: 'Empresa Solicitante',
        selector: 'company',
        wrap: true,
        sortable: true
      },
      {
        name: 'Contacto',
        selector: 'contact',
        sortable: true,
        center: true
      },
      {
        name: 'Telefono',
        selector: 'phone',
        sortable: true,
        center: true
      },
      {
        name: 'Email',
        selector: 'email',
        wrap: true,
        sortable: true,
        grow: 3
      },
      {
        name: 'Muestras',
        cell: row => row.samples.length,
        sortable: true,
        center: true
      },
      {
        cell: row => (
          <ActionBarGrid row={row} handleButtonClick={handleButtonClick} />
        ),
        name: 'Acciones',
        ignoreRowClick: true,
        allowOverflow: true,
        style: {
          position: 'sticky',
          right: 0
        },
        button: true
      }
    ],
    []
  );

  return (
    <DataTable
      pagination
      highlightOnHover
      striped
      persistTableHead
      expandableRows
      expandOnRowClicked
      columns={columns}
      data={data}
      selectableRows={false}
      progressPending={pending}
      customStyles={customStyles}
      paginationComponentOptions={paginationOptions}
      progressComponent={<Progress />}
      noDataComponent={<NoData />}
      expandableRowsComponent={<ExpandedComponent />}
    />
  );
};
