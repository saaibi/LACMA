import React from 'react';

export default ({ data }) => {
  return (
    <ul className="collection with-header">
      <li className="collection-header">
        <h4>Cliente: {`${data.company}`}</h4>
      </li>
      <li className="collection-item">
        <strong>Email: </strong>
        {data.email} <br></br>
        <strong>Telefono: </strong>
        {data.phone} <br></br>
        <strong>Nit: </strong>
        {data.nit}
        <br></br>
        <strong>Fax: </strong>
        {data.fax}
        <br></br>
        <strong>Extensión: </strong>
        {data.extension}
        <br></br>
        <strong>Dirección: </strong>
        {data.address}
        <br></br>
        <strong>Codigo Muestras: </strong>
        {_.map(data.samples, 'code').join(' , ')}
        <br></br>
      </li>
    </ul>
  );
};
