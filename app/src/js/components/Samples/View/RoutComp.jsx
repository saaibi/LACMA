import React, { Component } from 'react';
import { Helmet } from 'react-helmet';

import AppHeader from '../../AppHeader';
import ViewSample from '.';

class AppViewSample extends Component {
  render() {
    return (
      <div className="container samples-view">
        <Helmet titleTemplate="LACMA - %s">
          <title>VER MUESTRAS</title>
        </Helmet>
        <AppHeader name="VER MUESTRAS" />
        <ViewSample />
      </div>
    );
  }
}

export { AppViewSample };
