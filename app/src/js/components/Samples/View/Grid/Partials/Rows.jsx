import React from 'react';
import PropTypes from 'prop-types';

import Row from './Row';

const Rows = ({ samples, optionsSample }) => (
  <tbody>
    {samples.map((sample, index) => (
      <Row key={index} sample={sample} optionsSample={optionsSample} />
    ))}
  </tbody>
);

Rows.propTypes = {
  clients: PropTypes.array
};

export default Rows;
