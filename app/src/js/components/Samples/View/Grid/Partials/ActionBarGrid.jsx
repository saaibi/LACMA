import React, { Fragment } from 'react';

import Button from '../../../../Common/Button';

export default ({ row, handleButtonClick }) => (
  <Fragment>
    <Button
      className="btn-floating btn-small blue lighten-2 "
      icon="edit"
      classNameIcon="prefix"
      onClick={e => handleButtonClick({ action: 'edit', row })}
    />

    <Button
      className="btn-floating  btn-small red lighten-1"
      icon="delete_forever"
      classNameIcon="prefix"
      onClick={e => handleButtonClick({ action: 'delete', row })}
    />

    <Button
      className="btn-floating  btn-small pink"
      icon="screen_share"
      classNameIcon="prefix"
      onClick={e => handleButtonClick({ action: 'send', row })}
    />
  </Fragment>
);
