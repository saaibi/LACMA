import React, { Component } from 'react';
import { connect } from 'react-redux';

import { sampleActions } from '../../../actions/sample.actions';
import Table from './Table';
import { history } from '../../../store';
import Dialog from '../../Common/Modal/Dialog';
import Form from '../Edit/Form';
import ActionBar from '../Edit/Form/Partials/ActionBar';
import moment from 'moment';
import { swalAlert, alertDelete } from '../../../utils/alert';

export class ViewSample extends Component {
  state = {
    openModal: false,
    isLoadSample: false,
    isLoadEdit: false,
    sample: {},
    sampleEdit: {}
  };

  componentDidMount() {
    const { _id } = JSON.parse(localStorage.getItem('user'));
    this.props.dispatch(sampleActions.getAllSamplesByAnaly({ _id }));
    this.setSampleState('analyst', _id);
  }

  componentDidUpdate() {
    $('select').formSelect();
  }

  static getDerivedStateFromProps(props, state) {
    const { isLoadEdit } = state;
    const {
      samples: { isLoadSample, sample }
    } = props;
    if (!!sample && !_.isEqual(isLoadSample, isLoadEdit)) {
      return {
        sampleEdit: sample,
        isLoadEdit: isLoadSample
      };
    }
    return null;
  }

  setSampleState = (name, value) => {
    this.setState(({ sampleEdit }) => ({
      sampleEdit: {
        ...sampleEdit,
        [name]: value
      }
    }));
  };

  handleClickOpen = () => {
    this.setState(state => ({ openModal: !state.openModal }));
  };

  onSelectFilter = (value, filter) => {
    const { _id } = JSON.parse(localStorage.getItem('user'));
    this.props.dispatch(sampleActions.getAllSamplesByAnaly({ _id, filter }));
  };

  loadDates = (date, a) => {
    this.setSampleState(a, moment(date).format());
  };

  loadSample = e => {
    const { name, value } = e.target;
    this.setSampleState(name, value);
  };

  onSelect = (data, { name }) => {
    this.setSampleState(name, data ? data._id : null);
  };

  createSample = e => {
    e.preventDefault();
    const { sample } = this.state;
    if (!sample.receiptDate) toast({ html: 'Ingresa Fecha y Hora de Recibo' });
    else if (!sample.agreedDeliveryDate)
      toast({ html: 'Ingresa Fecha Pactada de Entrega' });
    else if (!sample.responsibleSample || !sample.coordinator)
      toast({ html: 'Selecciona Responsable Toma de Muestra y Coordinador' });
    else if (!sample.products) toast({ html: 'Selecciona un producto' });
    else if (!sample.client) toast({ html: 'Selecciona un cliente' });
    // else this.props.dispatch(sampleActions.createSample(sample));
  };

  updateSample = e => {
    e.preventDefault();
    const { sampleEdit } = this.state;
    const idSample = sampleEdit._id;
    const dataDelete = [
      '_id',
      'analyst',
      'code',
      'products',
      'client',
      'coordinator',
      'responsibleSample',
      'receiptDate',
      'agreedDeliveryDate',
      'created_at',
      'updated_at'
    ];
    _.each(dataDelete, i => {
      delete sampleEdit[i];
    });
    this.props.dispatch(sampleActions.updateSample(idSample, sampleEdit));
    this.handleClickOpen();
  };

  optionsSample = async e => {
    const { action, row, rows } = e;
    const { dispatch } = this.props;
    switch (action) {
      case 'delete':
        const { value: deleteOne } = await swalAlert(alertDelete());
        if (deleteOne) dispatch(sampleActions.deleteSample([row._id]));
        break;
      case 'deleteAll':
        const { value: deleteMany } = await swalAlert(alertDelete());
        if (deleteMany) dispatch(sampleActions.deleteSample(rows));
        break;
      case 'send':
        // dispatch(sampleActions.getByIdSampleClientProductsMethods(row._id));
        history.push('/results');
        break;
      case 'edit':
        dispatch(sampleActions.getByIdSample(row._id));
        this.handleClickOpen();
        break;
      default:
        break;
    }
  };

  render() {
    const { openModal, sampleEdit } = this.state;
    const { samples } = this.props;

    // if (samples.isLoading || (!samples.samples)) { return (<Progress type="linear" />) }

    return (
      <div className="border-primary overflow-x-hidden">
        <Table
          pending={samples.isLoading}
          samples={samples.samples}
          handleButtonClick={this.optionsSample}
          onSelect={this.onSelectFilter}
        />
        <Dialog
          openModal={openModal}
          header={'Editar Muestra'}
          actions={<ActionBar createSample={this.updateSample} />}
          content={
            <Form
              sample={sampleEdit}
              loadSample={this.loadSample}
              loadDates={this.loadDates}
              onSelect={this.onSelect}
            />
          }
          handleClickOpen={this.handleClickOpen}
        />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  samples: state.sample
});

export default connect(mapStateToProps)(ViewSample);
