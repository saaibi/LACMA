import React, { Component } from 'react';
import { connect } from 'react-redux';
import ReactResizeDetector from 'react-resize-detector';

import { sampleActions } from '../../../actions/sample.actions';
import { screenSize, getElementSize } from '../../../utils/dom-utils';

import Grid from './Grid';
import Table from './Table';

import Progress from '../../Common/Utils/Progress';

export class ViewSample extends Component {
  state = {
    range: 0,
    width: 0
  };

  componentDidMount() {
    this.props.dispatch(sampleActions.getAllSample());
    // $('select').formSelect();
    // const width = screenSize().width;
    // this.setState({ range: 0, width });
  }

  //   componentDidUpdate() {
  //     const { samples } = this.props;
  //     if (samples.samples && !samples.isLoading) {
  //       const widthTable = getElementSize('tableGrid').width;
  //       this.grid.style.width = `${widthTable}px`;
  //     }
  //   }

  optionsSample = e => {
    const { action, row } = e;
    const { dispatch } = this.props;
    switch (action) {
      case 'delete':
        dispatch(sampleActions.deleteSample(row._id));
        break;
      case 'send':
        console.log(row._id);
        // dispatch(sampleActions.getByIdSampleClientProductsMethods(row._id));
        break;
      default:
        break;
    }
  };

  //   onRange = e => {
  //     const { name, value } = e.target;
  //     const widthTable = getElementSize('tableGrid').width;
  //     const rangeGrid = getElementSize('rangeGrid').width;
  //     this.grid.style.transform = `translateX(${value * -1}px)`;
  //     this.setState({ range: value, width: widthTable - rangeGrid });
  //   };

  //   onResize = () => {
  //     const width = screenSize().width;
  //     this.setState({ width });
  //   };

  render() {
    const { samples } = this.props;
    const { range, width } = this.state;

    // if (samples.isLoading || (!samples.samples)) { return (<Progress type="linear" />) }

    return (
      <div className="border-primary overflow-x-hidden">
        <Table
          samples={samples.samples}
          handleButtonClick={this.optionsSample}
        />
        {samples.isLoading && <Progress />}
        {/* <div
          id="divGrid"
          className="carousels"
          ref={node => {
            this.grid = node;
          }}
        >
          <Grid
            samples={samples.samples}
            className="responsive-table"
            optionsSample={this.optionsSample}
          />
         
        </div>
        {width > '992' ? (
          <p className="range-field">
            <input
              type="range"
              id="rangeGrid"
              name="range"
              value={range}
              min="0"
              max={width}
              onChange={e => this.onRange(e)}
            />
          </p>
        ) : (
          ''
        )}
        <ReactResizeDetector
          handleWidth
          handleHeight
          onResize={this.onResize}
        /> */}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  samples: state.sample
});

export default connect(mapStateToProps)(ViewSample);
