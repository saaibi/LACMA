import React, { Fragment } from 'react';
import SelectInput from 'react-select';
import makeAnimated from 'react-select/animated';
import { defaultConfig } from '../../../../Common/Select/selecttInput';

const animatedComponents = makeAnimated();

const customStyles = {
  container: provided => ({
    ...provided,
    zIndex: 2,
    width: '20%'
  })
};

export default ({ filter, onSelect }) => (
  <Fragment>
    <SelectInput
      {...defaultConfig}
      styles={customStyles}
      className="basic-single"
      components={animatedComponents}
      getOptionValue={op => op.value}
      getOptionLabel={op => op.label}
      placeholder="Filtro..."
      name="filter"
      id="filter_1"
      options={[
        { value: 'all', label: 'Todos' },
        { value: 'today', label: 'Para hoy' }
      ]}
      onChange={(data, { name }) => onSelect(name, data ? data.value : 'all')}
    />
  </Fragment>
);
