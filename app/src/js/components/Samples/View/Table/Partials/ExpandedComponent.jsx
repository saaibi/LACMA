import React from 'react';
import moment from 'moment';
import Button from '../../../../Common/Button';

// eslint-disable-next-line react/prop-types
export default ({ data, handleButtonClick }) => (
  <div className="row">
    <div className="col s12 m6">
      <ul className="collection with-header">
        <li className="collection-header">
          <h4>Fechas</h4>
        </li>
        <li className="collection-item">
          <strong>Fecha de Fabricacion: </strong>
          {data.manufacturingDate && moment(data.manufacturingDate).format('L')}
        </li>
        <li className="collection-item">
          <strong>Fecha de Vencimineto: </strong>
          {data.expirationDate && moment(data.expirationDate).format('L')}
        </li>
        <li className="collection-item">
          <strong>Fecha y Hora de Recibo: </strong>
          {moment(data.receiptDate).format('DD/MM/YYYY HH:mm')}
        </li>
        <li className="collection-item">
          <strong>Fecha y Hora Inicio Análisis: </strong>
          {data.startDateAnalysis &&
            moment(data.startDateAnalysis).format('DD/MM/YYYY HH:mm')}
        </li>
        <li className="collection-item">
          <strong>Fecha y Hora Final de Analisis: </strong>
          {data.endDateAnalysis &&
            moment(data.endDateAnalysis).format('DD/MM/YYYY HH:mm')}
        </li>
        <li className="collection-item">
          <strong>Fecha Elaboración de Informe: </strong>
          {data.reportDate && moment(data.reportDate).format('L')}
        </li>
        <li className="collection-item">
          <strong>Fecha Pactada de Entrega: </strong>
          {moment(data.agreedDeliveryDate).format('L')}
        </li>
        <li className="collection-item">
          <strong>Fecha de Entrega: </strong>
          {data.deliveryDate && moment(data.deliveryDate).format('L')}
        </li>
        <li className="collection-item">
          <strong>Fecha y Hora de Toma: </strong>
          {data.takeSampleDate &&
            moment(data.takeSampleDate).format('DD/MM/YYYY HH:mm')}
        </li>
      </ul>
    </div>
    <div className="col s12 m6">
      <ul className="collection with-header">
        <li className="collection-header">
          <h4>Detalles</h4>
        </li>
        <li className="collection-item">
          <strong>Lote: </strong>
          {data.lote}
        </li>
        <li className="collection-item">
          <strong>Cantidad de Muestra: </strong>
          {data.quantity}
        </li>
        <li className="collection-item">
          <strong>
            Condiciones de Llegada al laboratorio Temperatura ºC:{' '}
          </strong>
          {data.temperature}
        </li>
        <li className="collection-item">
          <strong>Tipo de Envase-Empaque: </strong>
          {data.typeContainer}
        </li>
        <li className="collection-header">
          <h4>Características Organolépticas</h4>
        </li>
        <li className="collection-item">
          <strong>Aspecto: </strong>
          {data.appearance}
        </li>
        <li className="collection-item">
          <strong>Color: </strong>
          {data.color}
        </li>
        <li className="collection-item">
          <strong>Olor: </strong>
          {data.smell}
        </li>
      </ul>
    </div>
    <div className="col s12 input-field align-center">
      <Button
        className="waves-effect waves-light pink btn"
        texto="Generar Reporte"
        icon="screen_share"
        classNameIcon="right"
        onClick={() => handleButtonClick({ action: 'send', row: data })}
      />
    </div>
  </div>
);
