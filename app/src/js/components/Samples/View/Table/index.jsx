import React, { useMemo, useEffect, useCallback, useState } from 'react';
import DataTable from 'react-data-table-component';

import memoize from 'memoize-one';

import ActionBarGrid from '../Grid/Partials/ActionBarGrid';
import ExpandedComponent from './Partials/ExpandedComponent';
import ButtonLink from '../../../Common/Button/Button';
import Button from '../../../Common/Button';
import {
  customStyles,
  paginationOptions,
  Progress,
  defaultConfigTable
} from '../../../Common/Utils/Table';
import Filter from './Partials/Filter';

const NoData = () => (
  <div style={{ padding: '24px' }}>
    <ButtonLink text="Crear Nueva Muestra" to="/samples" />
  </div>
);

const contextActions = memoize(deleteHandler => (
  <Button
    className="btn-floating waves-effect waves-light red"
    icon="delete_forever"
    classNameIcon="prefix"
    onClick={deleteHandler}
  />
));

export default ({ samples, pending, handleButtonClick, onSelect }) => {
  const [selectedRows, setSelectedRows] = useState([]);
  const [toggleCleared, setToggleCleared] = useState(false);

  const columns = useMemo(
    () => [
      {
        name: 'Codigo',
        selector: 'code',
        center: true,
        sortable: true
      },
      {
        name: 'Version',
        selector: 'version',
        sortable: true
      },
      {
        name: 'Coordinador',
        cell: row =>
          row.coordinator
            ? `${row.coordinator.firstName || ''} ${row.coordinator.lastName ||
                ''}`
            : '',
        sortable: true
      },

      {
        name: 'Responsable Toma de Muestra',
        cell: row =>
          row.responsibleSample
            ? `${row.responsibleSample.firstName || ''} ${row.responsibleSample
                .lastName || ''}`
            : '',
        sortable: true
      },
      {
        name: 'Tipo de Muestra',
        selector: 'typeSample',
        sortable: true
      },
      {
        cell: row => (
          <ActionBarGrid row={row} handleButtonClick={handleButtonClick} />
        ),
        name: 'Acciones',
        ignoreRowClick: true,
        allowOverflow: true,
        button: true
      }
    ],
    []
  );

  const handleChange = useCallback(
    ({ allSelected, selectedCount, selectedRows }) => {
      const resData = _.map(selectedRows, '_id');
      setSelectedRows(resData);
    },
    []
  );

  const deleteAll = () => {
    handleButtonClick({ action: 'deleteAll', rows: selectedRows });
    setToggleCleared(!toggleCleared);
  };

  return (
    <DataTable
      {...defaultConfigTable}
      pagination
      highlightOnHover
      striped
      persistTableHead
      subHeader
      selectableRows
      expandableRows
      expandOnRowClicked
      columns={columns}
      data={samples}
      onRowDoubleClicked={console.log}
      progressPending={pending}
      defaultSortField="code"
      paginationRowsPerPageOptions={[10, 15, 20, 25, 50, 100]}
      paginationComponentOptions={paginationOptions}
      customStyles={customStyles}
      contextActions={contextActions(deleteAll)}
      clearSelectedRows={toggleCleared}
      onSelectedRowsChange={handleChange}
      expandableRowsComponent={
        <ExpandedComponent handleButtonClick={handleButtonClick} />
      }
      noDataComponent={<NoData />}
      progressComponent={<Progress />}
      subHeaderComponent={<Filter onSelect={onSelect} />}
    />
  );
};
