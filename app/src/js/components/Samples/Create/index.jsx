import React, { Component } from 'react';
import { connect } from 'react-redux';
import moment from 'moment';

import { sampleActions } from '../../../actions/sample.actions';

import Form from './Form';

import initialState from '../../../utils/sample-state';
import { toast } from '../../../utils/dom-utils';
import { userActions } from '../../../actions/user.actions';

class Sample extends Component {
  state = {
    sample: initialState
  };

  componentDidMount() {
    const { _id } = JSON.parse(localStorage.getItem('user'));
    this.setSampleState('analyst', _id);
  }

  setSampleState = (name, value) => {
    const { sample } = this.state;
    this.setState({
      sample: {
        ...sample,
        [name]: value
      }
    });
  };

  loadDates = (date, a) => {
    this.setSampleState(a, moment(date).format());
  };

  loadSample = e => {
    const { name, value } = e.target;
    this.setSampleState(name, value);
  };

  onSelect = (data, { name }) => {
    this.setSampleState(name, data ? data._id : null);
  };

  createSample = e => {
    e.preventDefault();
    const { sample } = this.state;
    if (!sample.receiptDate) toast({ html: 'Ingresa Fecha y Hora de Recibo' });
    else if (!sample.agreedDeliveryDate)
      toast({ html: 'Ingresa Fecha Pactada de Entrega' });
    else if (!sample.responsibleSample || !sample.coordinator)
      toast({ html: 'Selecciona Responsable Toma de Muestra y Coordinador' });
    else if (!sample.products) toast({ html: 'Selecciona un producto' });
    else if (!sample.client) toast({ html: 'Selecciona un cliente' });
    else this.props.dispatch(sampleActions.createSample(sample));
  };

  render() {
    const { sample } = this.state;
    return (
      <Form
        sample={sample}
        createSample={this.createSample}
        loadSample={this.loadSample}
        loadDates={this.loadDates}
        onSelect={this.onSelect}
      />
    );
  }
}

const mapStateToProps = state => ({});
export default connect(mapStateToProps)(Sample);
