import React, { Component } from 'react';
import { Helmet } from 'react-helmet';

import AppHeader from '../../AppHeader';
import Sample from '.';

class AppSample extends Component {
  render() {
    return (
      <div className="container">
        <Helmet titleTemplate="LACMA - %s">
          <title>CREAR MUESTRA</title>
        </Helmet>
        <AppHeader name="CREAR MUESTRA" />
        <Sample />
      </div>
    );
  }
}

export { AppSample };
