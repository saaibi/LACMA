import React from 'react';

import Input from '../../../../../Common/Input';

const Fields = ({ loadSample }) => (
  <div className="row">
    <div className="container">
      {/* <Input
        id="code"
        name="code"
        text="Codigo"
        className="col s12 m6 l4"
        autoFocus={true}
        icon="line_style"
        classNameIcon="prefix"
        onChange={loadSample}
      /> */}
      <Input
        id="version"
        name="version"
        text="Version"
        className="col s12 m6 l4"
        icon="cloud_done"
        classNameIcon="prefix"
        onChange={loadSample}
      />
      {/* <Analista
        id="analyst"
        name="analyst"
        text="Analista"
        className="col s12 m6 l4"
        icon="person"
        classNameIcon="prefix"
        onChange={loadSample}
      /> */}
      <Input
        id="coordinator"
        name="coordinator"
        text="Coordinador"
        className="col s12 m6 l4"
        icon="person"
        classNameIcon="prefix"
        onChange={loadSample}
      />
    </div>
  </div>
);

export default Fields;
