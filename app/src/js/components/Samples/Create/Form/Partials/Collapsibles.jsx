import React from 'react';
import { Collapsible } from 'react-materialize';

import CollapsibleItem from '../../../../Common/Collapsible/CollapsibleItems';
import FieldsInit from './Fields/FieldsInit';
import FieldsFechas from './Fields/FieldsFechas';
import FieldsDetails from './Fields/FieldsDetails';
import SelectProduct from './Fields/SelectProduct';

const Collapsibles = props => {
  return (
    <Collapsible popout defaultActiveKey={0}>
      <CollapsibleItem
        header="Registro y Lectura de Muestras "
        icon="assignment"
        content={<FieldsDetails {...props} />}
      />
      <CollapsibleItem
        header="Fechas"
        icon="insert_invitation"
        content={<FieldsFechas {...props} />}
      />
      {/* <CollapsibleItem
                header="Detalles"
                icon='chrome_reader_mode'
                content={<FieldsDetails {...props}/>}
            /> */}
      <CollapsibleItem
        header="Seleccióna - Producto, Cliente, Responsable Toma de Muestra y Coordinador"
        icon="card_travel"
        content={<SelectProduct onSelect={props.onSelect} />}
      />
    </Collapsible>
  );
};
export default Collapsibles;
