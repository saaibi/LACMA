import React from "react";
import MomentUtils from "@date-io/moment";
import {
  DatePicker,
  DateTimePicker,
  MuiPickersUtilsProvider
} from "@material-ui/pickers";
import moment from "moment";
import { Badge } from "@material-ui/core";

import { holiday } from "../../../../../../utils/dom-utils";

const classes = {
  root: "input-field col s12 m6 l6"
};

const validations = {
  shouldDisableDate: date =>
    moment(date).day() === 0 || moment(date).day() === 6 || holiday(date),
  renderDay: (date, selectedDate, isInCurrentMonth, dayComponent) => {
    if (isInCurrentMonth && holiday(date)) {
      return (
        <Badge color="error" variant="dot">
          {dayComponent}
        </Badge>
      );
    }
    return dayComponent;
  },
}
const defaultOptions = {
  cancelLabel: "Cancelar",
  autoOk: true,
  classes: classes
};

const Fields = ({ sample, loadDates }) => (
  <div className="row">
    <div className="container">
      <MuiPickersUtilsProvider utils={MomentUtils}>
        <DatePicker
          {...defaultOptions}
          // disableFuture
          format="DD/MM/YYYY"
          label="Fecha de Fabricacion"
          value={sample.manufacturingDate || null}
          onChange={e => loadDates(e, "manufacturingDate")}
        />
        <DatePicker
          {...defaultOptions}
          // disablePast
          format="DD/MM/YYYY"
          label="Fecha de Vencimineto"
          value={sample.expirationDate || null}
          onChange={e => loadDates(e, "expirationDate")}
        />
        <DateTimePicker
          {...defaultOptions}
          // disableFuture
          showTodayButton
          ampm={false}
          todayLabel="Hoy"
          label="Fecha y Hora de Recibo"
          value={sample.receiptDate || null}
          onChange={e => loadDates(e, "receiptDate")}
          format="DD/MM/YYYY HH:mm"
        />
        <DateTimePicker
          {...defaultOptions}
          // disablePast
          ampm={false}
          label="Fecha y Hora Inicio Análisis"
          value={sample.startDateAnalysis || null}
          onChange={e => loadDates(e, "startDateAnalysis")}
          format="DD/MM/YYYY HH:mm"
        />
        <DateTimePicker
          {...defaultOptions}
          // disablePast
          ampm={false}
          label="Fecha y Hora Final de Analisis"
          value={sample.endDateAnalysis || null}
          onChange={e => loadDates(e, "endDateAnalysis")}
          format="DD/MM/YYYY HH:mm"
        />
        <DatePicker
          {...defaultOptions}
          // disablePast
          format="DD/MM/YYYY"
          label="Fecha Elaboración de Informe"
          value={sample.reportDate || null}
          onChange={e => loadDates(e, "reportDate")}
        />
        <DatePicker
          {...defaultOptions}
          // disablePast
          format="DD/MM/YYYY"
          label="Fecha Pactada de Entrega"
          value={sample.agreedDeliveryDate || null}
          onChange={e => loadDates(e, "agreedDeliveryDate")}
        />
        <DatePicker
          {...defaultOptions}
          // disablePast
          format="DD/MM/YYYY"
          label="Fecha de Entrega"
          value={sample.deliveryDate || null}
          onChange={e => loadDates(e, "deliveryDate")}
        />
        <DateTimePicker
          {...defaultOptions}
          // disablePast
          ampm={false}
          label="Fecha y Hora de Toma"
          value={sample.takeSampleDate || null}
          onChange={e => loadDates(e, "takeSampleDate")}
          format="DD/MM/YYYY HH:mm"
        />
      </MuiPickersUtilsProvider>
    </div>
  </div>
);

export default Fields;
