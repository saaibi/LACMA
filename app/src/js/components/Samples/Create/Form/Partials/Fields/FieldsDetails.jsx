import React from 'react';
import Input from '../../../../../Common/Input';


const Fields = ({ loadSample }) => (
  <div className="row">
    <div className="container">
      <Input
        id="version"
        name="version"
        text="Version"
        className="col s12 m6"
        icon="cloud_done"
        classNameIcon="prefix"
        onChange={loadSample}
      />
      <Input
        id="type_sample"
        name="typeSample"
        text="Tipo de Muestra"
        className="col s12 m6"
        autoFocus={true}
        icon="library_books"
        classNameIcon="prefix"
        onChange={loadSample}
      />
      <Input
        id="quantity"
        name="quantity"
        text="Cantidad de Muestra"
        className="col s12 m6"
        icon="sort"
        classNameIcon="prefix"
        onChange={loadSample}
      />
      <Input
        id="lote"
        name="lote"
        text="Lote"
        className="col s12 m6"
        icon="line_style"
        classNameIcon="prefix"
        onChange={loadSample}
      />
      <Input
        id="type_container"
        name="typeContainer"
        text="Tipo de Envase-Empaque"
        className="col s12 m6"
        icon="inbox"
        classNameIcon="prefix"
        onChange={loadSample}
      />
      <Input
        id="color"
        name="color"
        text="Color"
        className="col s12 m6"
        icon="format_color_fill"
        classNameIcon="prefix"
        onChange={loadSample}
      />
      <Input
        id="smell"
        name="smell"
        text="Olor"
        className="col s12 m6"
        icon="record_voice_over"
        classNameIcon="prefix"
        onChange={loadSample}
      />
      <Input
        id="appearance"
        name="appearance"
        text="Aspecto"
        className="col s12 m6"
        icon="receipt"
        classNameIcon="prefix"
        onChange={loadSample}
      />
      <Input
        id="temperature"
        name="temperature"
        text="Condiciones de Llegada al laboratorio Temperatura ºC"
        className="col s12"
        icon="done"
        classNameIcon="prefix"
        onChange={loadSample}
      />
    </div>
  </div>
);

export default Fields;
