import React, { Component } from 'react';
import { connect } from 'react-redux';
import SelectInput from 'react-select';

import { productActions } from '../../../../../../actions/product.actions';

import Progress from '../../../../../Common/Utils/Progress';

import { defaultConfig } from '../../../../../Common/Select/selecttInput';
import { clientActions } from '../../../../../../actions/client.actions';
import { userActions } from '../../../../../../actions/user.actions';

const customStyles = {
  container: provided => ({
    ...provided,
    zIndex: 5
  })
};
const customStyles2 = {
  container: provided => ({
    ...provided,
    zIndex: 4
  })
};
const customStyles3 = {
  container: provided => ({
    ...provided,
    zIndex: 3
  })
};

class SelectProduct extends Component {
  componentWillMount() {
    this.props.dispatch(productActions.getAllProduct());
    this.props.dispatch(clientActions.getAllClient());
    this.props.dispatch(userActions.getAllUsers());
  }

  render() {
    const { products, clients, users, onSelect } = this.props;
    const { username } = JSON.parse(localStorage.getItem('user'));

    if (
      products.isLoading ||
      clients.isLoading ||
      users.isLoading ||
      !products.products ||
      !clients.clients ||
      !users.users
    ) {
      return <Progress className="selectproduct" />;
    }

    return (
      <div className="row">
        <div className="container">
          <div className="input-field col s12">
            <SelectInput
              {...defaultConfig}
              styles={customStyles}
              options={products.products}
              onChange={onSelect}
              name="products"
              id="products"
            />
            <label htmlFor="products" className="active">
              Producto
            </label>
          </div>
          <div className="input-field col s12">
            <SelectInput
              {...defaultConfig}
              styles={customStyles2}
              options={clients.clients}
              onChange={onSelect}
              getOptionLabel={op => op.company}
              name="client"
              id="client"
            />
            <label htmlFor="client" className="active">
              Cliente
            </label>
          </div>
          <div className="input-field col s12">
            <SelectInput
              {...defaultConfig}
              styles={customStyles3}
              name="responsibleSample"
              id="responsibleSample_1"
              options={_.filter(users.users, i => i.username != 'admin')}
              getOptionLabel={op => `${op.firstName} ${op.lastName}`}
              onChange={onSelect}
            />
            <label htmlFor="first_name" className="active">
              Responsable Toma de Muestra
            </label>
          </div>
          <div className="input-field col s12">
            <SelectInput
              {...defaultConfig}
              name="coordinator"
              id="coordinator_1"
              options={_.filter(
                users.users,
                i =>
                  i.username != 'admin' &&
                  i.rol === 'coordinador' &&
                  i.username != username
              )}
              getOptionLabel={op => `${op.firstName} ${op.lastName}`}
              onChange={onSelect}
            />
            <label htmlFor="first_name" className="active">
              Coordinador
            </label>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  products: state.product,
  clients: state.client,
  users: state.users
});

export default connect(mapStateToProps)(SelectProduct);
