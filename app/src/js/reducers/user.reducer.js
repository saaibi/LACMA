import {
  USERS_GETALL,
  USER_GETBYID,
  USERS_DELETE,
  USER_UPDATE,
  REGISTER
} from "../constants/user.constans";

const initialState = {
  users: [],
  user: {},
  isLoadUser: false,
  isLoading: false,
  error: ""
};

export function users(state = initialState, action) {
  const { type, payload } = action;
  switch (type) {
    case USERS_GETALL.REQUEST:
    case USERS_GETALL.SUCCESS:
    case USERS_GETALL.FAILURE:
    case USERS_DELETE.REQUEST:
    case USERS_DELETE.FAILURE:
    case USER_GETBYID.REQUEST:
    case USER_GETBYID.FAILURE:
    case USER_UPDATE.REQUEST:
    case USER_UPDATE.FAILURE:
    case REGISTER.REQUEST:
    case REGISTER.FAILURE:
      return {
        ...state,
        ...payload
      };
    case REGISTER.SUCCESS: {
      const { user } = payload;
      return {
        ...state,
        ...payload,
        users: [...state.users, user]
      };
    }
    case USER_GETBYID.SUCCESS: {
      return {
        ...state,
        ...payload,
        isLoadUser: !state.isLoadUser
      };
    }
    case USER_UPDATE.SUCCESS: {
      const { user, isLoading, index } = payload;
      return {
        ...state,
        isLoading,
        users: [
          ...state.users.slice(0, index),
          user,
          ...state.users.slice(index + 1)
        ]
      };
    }
    case USERS_DELETE.SUCCESS:
      const { index, isLoading } = payload;
      return {
        ...state,
        isLoading,
        users: [...state.users.slice(0, index), ...state.users.slice(index + 1)]
      };
    default:
      return state;
  }
}
