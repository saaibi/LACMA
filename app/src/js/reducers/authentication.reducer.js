import { LOGIN, REGISTER, LOGOUT } from '../constants/user.constans';

const user = JSON.parse(localStorage.getItem('user'));
// let users = JSON.parse(localStorage.getItem('users')) || [];

const initialState = user ? { loggedIn: true, user } : { isLoading: false};

export function authentication(state = initialState, action) {
  const { type, user } = action;
  switch (type) {
    case LOGIN.FAILURE:
    case LOGOUT.REQUEST:
      return { loggingIn: false, isLoading: false };
    case LOGIN.REQUEST:
      return { loggingIn: true, isLoading: true };
    case LOGIN.SUCCESS:
      return { loggedIn: true, isLoading: false, user: user };
    case LOGOUT.SUCCESS:
      return { loggingIn: false };
    default:
      return state;
  }
}
