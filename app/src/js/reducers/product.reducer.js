import {
  PRODUCT_GETBYID,
  PRODUCT_GETALL,
  PRODUCT_CREATE,
  PRODUCT_UPDATE,
  PRODUCT_DELETE
} from '../constants/product.constans';

import {
  METHOD_GETALL,
  METHOD_GETBYID,
  METHOD_CREATE,
  METHOD_UPDATE,
  METHOD_DELETE
} from '../constants/method.constans';

const initialState = {
  error: '',
  product: {},
  products: [],
  productSelect: [],
  method: {},
  methods: [],
  isLoading: false,
  isSavemethod: false,
  isLoadProduct: false,
  isLoadMethod: false
};

export function product(state = initialState, action) {
  const { type, payload } = action;
  switch (type) {
    case PRODUCT_GETALL.REQUEST:
    case PRODUCT_GETALL.SUCCESS:
    case PRODUCT_GETALL.FAILURE:
    case PRODUCT_GETBYID.REQUEST:
    case PRODUCT_GETBYID.FAILURE:
    case PRODUCT_CREATE.REQUEST:
    case PRODUCT_CREATE.FAILURE:
    case PRODUCT_UPDATE.REQUEST:
    case PRODUCT_UPDATE.FAILURE:
    case PRODUCT_DELETE.REQUEST:
    case PRODUCT_DELETE.FAILURE:
    case METHOD_GETALL.REQUEST:
    case METHOD_GETALL.SUCCESS:
    case METHOD_GETALL.FAILURE:
    case METHOD_GETBYID.REQUEST:
    case METHOD_GETBYID.FAILURE:
    case METHOD_CREATE.REQUEST:
    case METHOD_CREATE.FAILURE:
    case METHOD_UPDATE.REQUEST:
    case METHOD_UPDATE.FAILURE:
    case METHOD_DELETE.REQUEST:
    case METHOD_DELETE.FAILURE:
      return {
        ...state,
        ...payload
      };
    case METHOD_GETBYID.SUCCESS: {
      return {
        ...state,
        ...payload,
        isLoadMethod: !state.isLoadMethod
      };
    }
    case PRODUCT_GETBYID.SUCCESS: {
      const { product, isLoading } = payload;
      return {
        ...state,
        isLoading,
        product,
        isLoadProduct: !state.isLoadProduct,
        productSelect: [product]
      };
    }
    case PRODUCT_CREATE.SUCCESS: {
      const { product, isLoading } = payload;
      return {
        ...state,
        isLoading,
        products: [...state.products, product]
      };
    }
    case METHOD_CREATE.SUCCESS: {
      const { method, isLoading } = payload;
      return {
        ...state,
        isLoading,
        method,
        isSavemethod: !state.isSavemethod,
        methods: [...state.methods, method]
      };
    }

    case PRODUCT_UPDATE.SUCCESS: {
      const { product, index, isLoading } = payload;
      return {
        ...state,
        isLoading,
        products: [
          ...state.products.slice(0, index),
          product,
          ...state.products.slice(index + 1)
        ]
      };
    }
    case METHOD_UPDATE.SUCCESS: {
      const { method, index,isLoading } = payload;
      return {
        ...state,
        isLoading,
        methods: [
          ...state.methods.slice(0, index),
          method,
          ...state.methods.slice(index + 1)
        ]
      };
    }

    case PRODUCT_DELETE.SUCCESS: {
      const { indexs, isLoading } = payload;
      const resProducts = _.filter(state.products, i => !indexs.includes(i._id));
      return {
        ...state,
        isLoading,
        product: {},
        productSelect: [],
        products: resProducts
      };
    }
    case METHOD_DELETE.SUCCESS: {
      const { indexs, isLoading } = payload;
      const resMethods = _.filter(state.methods, i => !indexs.includes(i._id));
      return {
        ...state,
        isLoading,
        method: {},
        methods: resMethods
      };
    }

    default:
      return state;
  }
}
