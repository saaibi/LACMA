import { RESULT_CREATE_PDF } from '../constants/result.constans';

const initialState = {
  results: [],
  result: {},
  isLoading: false,
  error: {}
};

export function result(state = initialState, action) {
  const { type, payload } = action;
  switch (type) {
    case RESULT_CREATE_PDF.REQUEST:
    case RESULT_CREATE_PDF.SUCCESS:
    case RESULT_CREATE_PDF.FAILURE:
      return {
        ...state,
        ...payload
      };
    default:
      return state;
  }
}
