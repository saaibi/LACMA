import _ from 'lodash';
import Swal from 'sweetalert2/dist/sweetalert2';
import { makeRequestAsync } from '../services';
import {
  PRODUCT_GETBYID,
  PRODUCT_GETALL,
  PRODUCT_CREATE,
  PRODUCT_UPDATE,
  PRODUCT_DELETE
} from '../constants/product.constans';

import { swalAlert } from '../utils/alert';
import { toast } from '../utils/dom-utils';

const request = type => ({
  type,
  payload: {
    isLoading: true
  }
});

const success = (type, data) => ({
  type,
  payload: {
    ...data,
    isLoading: false
  }
});

const failure = (type, error) => ({
  type,
  payload: {
    isLoading: false,
    error
  }
});

const getAllProduct = () => async (dispatch, getState) => {
  dispatch(request(PRODUCT_GETALL.REQUEST));
  try {
    const products = await makeRequestAsync(`/products`, 'GET');
    dispatch(success(PRODUCT_GETALL.SUCCESS, { products: products.data }));
  } catch (error) {
    const message = error.message || error;
    dispatch(failure(PRODUCT_GETALL.FAILURE, { message }));
  }
};

const getAllProductMethods = () => async (dispatch, getState) => {
  dispatch(request(PRODUCT_GETALL.REQUEST));
  try {
    const products = await makeRequestAsync(
      `/products/2/productsmethods`,
      'GET'
    );
    dispatch(success(PRODUCT_GETALL.SUCCESS, { products: products.data }));
  } catch (error) {
    const message = error.message || error;
    dispatch(failure(PRODUCT_GETALL.FAILURE, { message }));
  }
};

const getProductById = id => async (dispatch, getState) => {
  dispatch(request(PRODUCT_GETBYID.REQUEST));
  try {
    const product = await makeRequestAsync(`/products/${id}`, 'GET');
    dispatch(success(PRODUCT_GETBYID.SUCCESS, { product: product.data }));
  } catch (error) {
    const message = error.message || error;
    dispatch(failure(PRODUCT_GETBYID.FAILURE, { message }));
  }
};

const getByIdProductMethods = id => async (dispatch, getState) => {
  dispatch(request(PRODUCT_GETBYID.REQUEST));
  try {
    const product = await makeRequestAsync(`/products/${id}/methods`, 'GET');
    dispatch(success(PRODUCT_GETBYID.SUCCESS, { product: product.data }));
  } catch (error) {
    const message = error.message || error;
    dispatch(failure(PRODUCT_GETBYID.FAILURE, { message }));
  }
};

const createProduct = productCreate => async (dispatch, getState) => {
  dispatch(request(PRODUCT_CREATE.REQUEST));
  try {
    const product = await makeRequestAsync(`/products`, 'POST', productCreate);
    dispatch(
      success(PRODUCT_CREATE.SUCCESS, { product: product.data.product })
    );
    toast({ html: `${product.data.status}` });
  } catch (error) {
    const message = _.map(error.errors, 'message') || error.message || error;
    toast({ html: `${message}` });
    dispatch(failure(PRODUCT_CREATE.FAILURE, { message }));
  }
};

const createProductFile = (data, save) => async dispatch => {
  if (save) {
    dispatch(request(PRODUCT_GETALL.REQUEST));
  }
  try {
    const file = await makeRequestAsync(
      `/products/123/file`,
      'POST',
      data,
      true
    );
    await swalAlert({
      title: 'Productos',
      icon: 'success',
      timer: 3500,
      timerProgressBar: true,
      html: `${file.data.size} Productos ${save ? 'Creados' : 'Para Crear'}`,
      onOpen: toast => {
        toast.addEventListener('mouseenter', Swal.stopTimer);
        toast.addEventListener('mouseleave', Swal.resumeTimer);
      }
    });
    if (save) {
      dispatch(getAllProductMethods());
    }
  } catch (error) {
    const message = error.message || error;
    toast({ html: `${message}` });
  }
};

const updateProduct = (product_id, productEdit) => async (
  dispatch,
  getState
) => {
  dispatch(request(PRODUCT_UPDATE.REQUEST));
  try {
    const { products } = getState().product;
    const index = _.findIndex(products, { _id: product_id });
    if (index === -1)
      return dispatch(
        failure(PRODUCT_UPDATE.FAILURE, { message: 'Producto no encontrado' })
      );

    const product = await makeRequestAsync(
      `/products/${product_id}`,
      'PUT',
      productEdit
    );

    dispatch(
      success(PRODUCT_UPDATE.SUCCESS, {
        index,
        product: product.data.product
      })
    );
    toast({ html: `${product.data.status}` });
  } catch (error) {
    const message = error.message || error;
    dispatch(failure(PRODUCT_UPDATE.FAILURE, { message }));
  }
};

const deleteProduct = product_id => async dispatch => {
  dispatch(request(PRODUCT_DELETE.REQUEST));
  try {
    const { data } = await makeRequestAsync(`/products`, 'DELETE', product_id);
    dispatch(success(PRODUCT_DELETE.SUCCESS, { indexs: product_id }));
    toast({ html: `${data.status}` });
  } catch (error) {
    const message = error.message || error;
    dispatch(failure(PRODUCT_DELETE.FAILURE, { message }));
  }
};

export const productActions = {
  getAllProduct,
  getAllProductMethods,
  getProductById,
  getByIdProductMethods,
  createProduct,
  createProductFile,
  updateProduct,
  deleteProduct
};
