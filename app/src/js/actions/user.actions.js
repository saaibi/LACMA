import findIndex from 'lodash/findIndex';
import {
  REGISTER,
  LOGIN,
  LOGOUT,
  USERS_GETALL,
  USER_GETBYID,
  USER_UPDATE,
  USERS_DELETE
} from '../constants/user.constans';
import { userService } from '../services/user.service';
import { history } from '../store';
import { makeRequestAsync } from '../services';
import { toast } from '../utils/dom-utils';

const request = type => ({
  type,
  payload: {
    isLoading: true
  }
});

const success = (type, data) => ({
  type,
  payload: {
    ...data,
    isLoading: false
  }
});

const failure = (type, error) => ({
  type,
  payload: {
    isLoading: false,
    error
  }
});

const register = userCreate => async (dispatch, getState) => {
  dispatch(request(REGISTER.REQUEST));
  try {
    const user = await makeRequestAsync(
      `/users/create`,
      'POST',
      userCreate,
      true
    );
    // const user = await makeRequestAsync(`/users`, "POST", userCreate);
    dispatch(success(REGISTER.SUCCESS, { user: user.data.user }));
    toast({ html: `${user.data.status}` });
  } catch (error) {
    const errors = _.map(error.errors, 'message').join('</br>');
    const message = errors || error.message || error;
    toast({ html: `${message}` });
    dispatch(failure(REGISTER.FAILURE, { message }));
  }
};

const login = (username, password) => async (dispatch, getState) => {
  dispatch({ type: LOGIN.REQUEST });
  try {
    const user = await makeRequestAsync(`/users/login`, 'POST', {
      username,
      password
    });

    dispatch({ type: LOGIN.SUCCESS, user: user.data.user });
    localStorage.setItem('user', JSON.stringify(user.data.user));
    history.push('/');
    // window.location.reload();
  } catch (error) {
    toast({
      html: `Nombre de usuario o contraseña incorrecta`
    });
    const message = error.message || error;
    dispatch({ type: LOGIN.FAILURE, error: message });
  }
};

const logout = () => {
  userService.logout();
  return { type: LOGOUT.SUCCESS };
};

const getAllUsers = () => async (dispatch, getState) => {
  dispatch(request(USERS_GETALL.REQUEST));
  try {
    const users = await makeRequestAsync(`/users`, 'GET');
    dispatch(success(USERS_GETALL.SUCCESS, { users: users.data }));
  } catch (error) {
    const message = error.message || error;
    dispatch(failure(USERS_GETALL.FAILURE, { message }));
  }
};

const getUserById = id => async (dispatch, getState) => {
  dispatch(request(USER_GETBYID.REQUEST));
  try {
    const user = await makeRequestAsync(`/users/${id}`, 'GET');
    dispatch(success(USER_GETBYID.SUCCESS, { user: user.data }));
  } catch (error) {
    const message = error.message || error;
    dispatch(failure(USER_GETBYID.FAILURE, { message }));
  }
};

const updateUser = (user_id, userEdit) => async (dispatch, getState) => {
  dispatch(request(USER_UPDATE.REQUEST));
  try {
    const { users } = getState().users;
    const index = findIndex(users, { _id: user_id });

    if (index === -1)
      return dispatch(
        failure(USER_UPDATE.FAILURE, { message: 'User Not found' })
      );
    const user = await makeRequestAsync(`/users/${user_id}`, 'PUT', userEdit);
    dispatch(success(USER_UPDATE.SUCCESS, { index, user: user.data.user }));
    toast({ html: `${user.data.status}` });
  } catch (error) {
    const message = error.message || error;
    toast({ html: `${message}` });
    dispatch(failure(USER_UPDATE.FAILURE, { message }));
  }
};

const deleteUser = user_id => async (dispatch, getState) => {
  dispatch(request(USERS_DELETE.REQUEST));
  try {
    const { users } = getState().users;
    const index = findIndex(users, { _id: user_id });

    if (index === -1)
      return dispatch(
        failure(USERS_DELETE.FAILURE, { message: 'User Not found' })
      );
    const product = await makeRequestAsync(`/users/${user_id}`, 'DELETE');
    dispatch(success(USERS_DELETE.SUCCESS, { index }));
    toast({ html: `${product.data.status}` });
  } catch (error) {
    const message = error.message || error;
    dispatch(failure(USERS_DELETE.FAILURE, { message }));
  }
};

const login2 = (username, password) => {
  const request = user => {
    return { type: LOGIN.REQUEST, user };
  };
  const success = user => {
    return { type: LOGIN.SUCCESS, user };
  };
  const failure = error => {
    return { type: LOGIN.FAILURE, error };
  };

  return dispatch => {
    dispatch(request({ username }));
    userService.login(username, password).then(
      user => {
        dispatch(success(user));
        history.push('/');
        window.location.reload();
      },
      error => {
        dispatch(failure(error));
      }
    );
  };
};

const register2 = user => {
  const request = user => {
    return { type: REGISTER.REQUEST, user };
  };
  const success = user => {
    return { type: REGISTER.SUCCESS, user };
  };
  const failure = error => {
    return { type: REGISTER.FAILURE, error };
  };
  return dispatch => {
    dispatch(request(user));

    userService.register(user).then(
      user => {
        dispatch(success());
        history.push('/login');
      },
      error => {
        dispatch(failure(error));
      }
    );
  };
};

export const userActions = {
  login,
  logout,
  register,
  getAllUsers,
  getUserById,
  updateUser,
  deleteUser
};
