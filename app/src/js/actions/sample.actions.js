import findIndex from 'lodash/findIndex';
import { makeRequestAsync } from '../services';
import {
  SAMPLE_GETALL,
  SAMPLE_GETBYID,
  SAMPLE_CREATE,
  SAMPLE_UPDATE,
  SAMPLE_DELETE
} from '../constants/sample.constans';
import { history } from '../store';
import { toast, errors } from '../utils/dom-utils';

const request = (type, data) => ({
  type,
  payload: {
    ...data,
    isLoading: true
  }
});

const success = (type, data) => ({
  type,
  payload: {
    ...data,
    isLoading: false
  }
});

const failure = (type, error) => ({
  type,
  payload: {
    isLoading: false,
    error
  }
});

const getAllSample = () => async (dispatch, getState) => {
  dispatch(request(SAMPLE_GETALL.REQUEST, { samples: [] }));
  try {
    const samples = await makeRequestAsync(`/samples`, 'GET');
    dispatch(success(SAMPLE_GETALL.SUCCESS, { samples: samples.data }));
  } catch (error) {
    const message = error.message || error;
    dispatch(failure(SAMPLE_GETALL.FAILURE, { message }));
  }
};

const getByIdSample = id => async (dispatch, getState) => {
  dispatch(request(SAMPLE_GETBYID.REQUEST));
  try {
    const { data } = await makeRequestAsync(`/samples/${id}`, 'GET');
    dispatch(success(SAMPLE_GETBYID.SUCCESS, { sample: data }));
  } catch (error) {
    const message = error.message || error;
    dispatch(failure(SAMPLE_GETBYID.FAILURE, { message }));
  }
};

const getAllSamplesByAnaly = dataSample => async (dispatch, getState) => {
  dispatch(request(SAMPLE_GETALL.REQUEST, { samples: [] }));
  try {
    const { data } = await makeRequestAsync(
      `/samples/2/analyst`,
      'POST',
      dataSample
    );
    dispatch(success(SAMPLE_GETALL.SUCCESS, { ...data }));
  } catch (error) {
    const message = error.message || error;
    dispatch(failure(SAMPLE_GETALL.FAILURE, { message }));
  }
};

const getByIdSampleClientProductsMethods = id => async (dispatch, getState) => {
  dispatch(request(SAMPLE_GETBYID.REQUEST));
  try {
    const sample = await makeRequestAsync(
      `/samples/${id}/clientProductsMethods`,
      'GET'
    );
    dispatch(success(SAMPLE_GETBYID.SUCCESS, { sample: sample.data }));
  } catch (error) {
    const message = error.message || error;
    dispatch(failure(SAMPLE_GETBYID.FAILURE, { message }));
  }
};

const createSample = sampleCreate => async (dispatch, getState) => {
  dispatch(request(SAMPLE_CREATE.REQUEST));
  try {
    const sample = await makeRequestAsync(`/samples`, 'POST', sampleCreate);
    dispatch(success(SAMPLE_CREATE.SUCCESS, { sample: sample.data.sample }));
    toast({ html: `${sample.data.status}` });
    history.push('/viewsamples');
  } catch (error) {
    const message = errors(error);
    toast({ html: `${message}` });
    dispatch(failure(SAMPLE_CREATE.FAILURE, { message }));
  }
};

const uploadSignature = (formData, id) => async (dispatch, getState) => {
  try {
    const sample = await makeRequestAsync(
      `/samples/${id}/signature`,
      'POST',
      formData,
      true
    );
    toast({ html: `${sample.data.status}` });
  } catch (error) {
    const message = error._message || error.message || error;
    toast({ html: `${message}` });
  }
};

const updateSample = (samples_id, sampleEdit) => async (dispatch, getState) => {
  dispatch(request(SAMPLE_UPDATE.REQUEST));
  try {
    const { samples } = getState().sample;
    const index = findIndex(samples, { _id: samples_id });

    if (index === -1)
      return dispatch(
        failure(SAMPLE_UPDATE.FAILURE, { message: 'Muestra no encontrada' })
      );

    const { data } = await makeRequestAsync(
      `/samples/${samples_id}`,
      'PUT',
      sampleEdit
    );
    dispatch(success(SAMPLE_UPDATE.SUCCESS, { index, sample: data.sample }));
    toast({ html: `${data.status}` });
  } catch (error) {
    const message = error.message || error;
    dispatch(failure(SAMPLE_UPDATE.FAILURE, { message }));
  }
};

const deleteSample = samples_id => async (dispatch, getState) => {
  dispatch(request(SAMPLE_DELETE.REQUEST));
  try {
    const { data } = await makeRequestAsync(`/samples`, 'DELETE', samples_id);
    dispatch(success(SAMPLE_DELETE.SUCCESS, { indexs: samples_id }));
    toast({ html: `${data.status}` });
  } catch (error) {
    const message = error.message || error;
    dispatch(failure(SAMPLE_DELETE.FAILURE, { message }));
  }
};

export const sampleActions = {
  getAllSample,
  getAllSamplesByAnaly,
  getByIdSample,
  getByIdSampleClientProductsMethods,
  createSample,
  uploadSignature,
  updateSample,
  deleteSample
};
