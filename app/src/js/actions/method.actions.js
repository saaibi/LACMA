import findIndex from 'lodash/findIndex';
import { makeRequestAsync } from '../services';
import {
  METHOD_GETALL,
  METHOD_GETBYID,
  METHOD_CREATE,
  METHOD_UPDATE,
  METHOD_DELETE
} from '../constants/method.constans';

const request = type => ({
  type,
  payload: {
    isLoading: true
  }
});

const success = (type, data) => ({
  type,
  payload: {
    ...data,
    isLoading: false
  }
});

const failure = (type, error) => ({
  type,
  payload: {
    isLoading: false,
    error
  }
});

const getAllMethods = () => {
  return async (dispatch, getState) => {
    dispatch(request(METHOD_GETALL.REQUEST));
    try {
      const methods = await makeRequestAsync(`/methods`, 'GET');
      dispatch(success(METHOD_GETALL.SUCCESS, { methods: methods.data }));
    } catch (error) {
      const message = error.message || error;
      dispatch(failure(METHOD_GETALL.FAILURE, { error: message }));
    }
  };
};

const getMethodById = id => async (dispatch, getState) => {
  dispatch(request(METHOD_GETBYID.REQUEST));
  try {
    const method = await makeRequestAsync(`/methods/${id}`, 'GET');
    dispatch(success(METHOD_GETBYID.SUCCESS, { method: method.data }));
  } catch (error) {
    const message = error.message || error;
    dispatch(failure(METHOD_GETBYID.FAILURE, { message }));
  }
};

const createMethod = methodCreate => {
  return async (dispatch, getState) => {
    dispatch(request(METHOD_CREATE.REQUEST));
    try {
      const method = await makeRequestAsync(`/methods`, 'POST', methodCreate);
      dispatch(success(METHOD_CREATE.SUCCESS, { method: method.data.method }));
      M.toast({ html: `${method.data.status}`, classes: 'rounded' });
    } catch (error) {
      const message = _.map(error.errors, 'message') || error.message || error;
      M.toast({ html: `${message}`, classes: 'rounded' });
      dispatch(failure(METHOD_CREATE.FAILURE, { message }));
    }
  };
};

const updateMethod = (method_id, methodtEdit) => {
  return async (dispatch, getState) => {
    dispatch(request(METHOD_UPDATE.REQUEST));
    try {
      const { methods } = getState().product;
      const index = findIndex(methods, { _id: method_id });

      if (index === -1)
        return dispatch(failure(METHOD_UPDATE.FAILURE, 'Method Not found'));

      const method = await makeRequestAsync(
        `/methods/${method_id}`,
        'PUT',
        methodtEdit
      );

      dispatch(
        success(METHOD_UPDATE.SUCCESS, { index, method: method.data.method })
      );
      M.toast({ html: `${method.data.status}`, classes: 'rounded' });
    } catch (error) {
      const message = error.message || error;
      dispatch(failure(METHOD_UPDATE.FAILURE, { message }));
    }
  };
};

const deleteMethod = method_id => {
  return async (dispatch, getState) => {
    dispatch(request(METHOD_DELETE.REQUEST));
    try {
      const { data } = await makeRequestAsync(`/methods`, 'DELETE', method_id);
      dispatch(success(METHOD_DELETE.SUCCESS, { indexs: method_id }));
      M.toast({ html: `${data.status}`, classes: 'rounded' });
    } catch (error) {
      const message = error.message || error;
      dispatch(failure(METHOD_DELETE.FAILURE, message));
    }
  };
};

export const methodActions = {
  getAllMethods,
  getMethodById,
  createMethod,
  updateMethod,
  deleteMethod
};
