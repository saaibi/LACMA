import { makeRequestAsync } from '../services';
import { RESULT_CREATE_PDF } from '../constants/result.constans';
import { toast } from '../utils/dom-utils';

const request = (type, data) => ({
  type,
  payload: {
    ...data,
    isLoading: true
  }
});

const success = (type, data) => ({
  type,
  payload: {
    ...data,
    isLoading: false
  }
});

const failure = (type, error) => ({
  type,
  payload: {
    isLoading: false,
    error
  }
});

const createPDF = dataResult => async (dispatch, getState) => {
  dispatch(request(RESULT_CREATE_PDF.REQUEST));
  try {
    const { data } = await makeRequestAsync(`/results`, 'POST', dataResult);
    document.getElementById('resultObject').data = data.pdf;
    const downloadLink = document.getElementById('downloadLink');
    downloadLink.href = data.pdf;
    downloadLink.removeAttribute('disabled');
    dispatch(success(RESULT_CREATE_PDF.SUCCESS));
    toast({ html: `${data.status}` });
  } catch (error) {
    const message = error.message || error;
    toast({ html: `${message}` });
    dispatch(failure(RESULT_CREATE_PDF.FAILURE, { message }));
  }
};

export const resultActions = {
  createPDF
};
