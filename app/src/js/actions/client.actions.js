import findIndex from 'lodash/findIndex';
import { makeRequestAsync } from '../services';
import {
  CLIENT_GET,
  CLIENT_GETBYID,
  CLIENT_CREATE,
  CLIENT_UPDATE,
  CLIENT_DELETE,
  CLIENT_CREDIT_GETBYID
} from '../constants/client.constans';
import { errors, toast } from '../utils/dom-utils';

const request = type => ({
  type,
  payload: {
    isLoading: true
  }
});

const success = (type, data) => ({
  type,
  payload: {
    ...data,
    isLoading: false
  }
});

const failure = (type, error) => ({
  type,
  payload: {
    isLoading: false,
    error
  }
});

const getAllClient = () => async (dispatch, getState) => {
  dispatch(request(CLIENT_GET.REQUEST));
  try {
    const clients = await makeRequestAsync(`/clients`, 'GET');
    dispatch(success(CLIENT_GET.SUCCESS, { clients: clients.data }));
  } catch (error) {
    const message = error.message || error;
    dispatch(failure(CLIENT_GET.FAILURE, { error: message }));
  }
};

const getById = id => async (dispatch, getState) => {
  dispatch(request(CLIENT_GETBYID.REQUEST));
  try {
    const client = await makeRequestAsync(`/clients/${id}`, 'GET');
    dispatch(success(CLIENT_GETBYID.SUCCESS, { client: client.data }));
  } catch (error) {
    const message = error.message || error;
    dispatch(failure(CLIENT_GETBYID.FAILURE, { message }));
  }
};

const createClient = clientCreate => async (dispatch, getState) => {
  dispatch(request(CLIENT_CREATE.REQUEST));
  try {
    const client = await makeRequestAsync(`/clients`, 'POST', clientCreate);
    dispatch(success(CLIENT_CREATE.SUCCESS, { client: client.data.client }));
    M.toast({ html: `${client.data.status}`, classes: 'rounded' });
  } catch (error) {
    const message = errors(error);
    toast({ html: `${message}` });
    dispatch(failure(CLIENT_CREATE.FAILURE, { message }));
  }
};

const updateClient = (client_id, clientEdit) => async (dispatch, getState) => {
  dispatch(request(CLIENT_UPDATE.REQUEST));
  try {
    const { clients } = getState().client;
    const index = findIndex(clients, { _id: client_id });

    if (index === -1)
      return dispatch(
        failure(CLIENT_UPDATE.FAILURE, { message: 'Client Not found' })
      );

    const client = await makeRequestAsync(
      `/clients/${client_id}/client`,
      'PUT',
      clientEdit
    );

    dispatch(
      success(CLIENT_UPDATE.SUCCESS, { index, client: client.data.client })
    );
    M.toast({ html: `${client.data.status}`, classes: 'rounded' });
  } catch (error) {
    const message = error.message || error;
    dispatch(failure(CLIENT_UPDATE.FAILURE, { message }));
  }
};

const deleteClient = client_id => async (dispatch, getState) => {
  dispatch(request(CLIENT_DELETE.REQUEST));
  try {
    const { clients } = getState().client;
    const index = findIndex(clients, { _id: client_id });

    if (index === -1)
      return dispatch(
        failure(CLIENT_DELETE.FAILURE, { message: 'Client Not found' })
      );

    const client = await makeRequestAsync(`/clients/${client_id}`, 'DELETE');
    dispatch(success(CLIENT_DELETE.SUCCESS, { index }));
    M.toast({ html: `${client.data.status}`, classes: 'rounded' });
  } catch (error) {
    const message = error.message || error;
    dispatch(failure(CLIENT_DELETE.FAILURE, { message }));
  }
};

export const clientActions = {
  getAllClient,
  getById,
  createClient,
  updateClient,
  deleteClient
};
