import React, { Fragment, Component } from 'react';
import { connect } from 'react-redux';
import { Router, Route } from 'react-router-dom';

import Header from '../components/AppHeader/Header';

import { PrivateRoute } from '../helpers/PrivateRoute';
import { history } from '../store';
import { HomePage } from '../components/Home';
import { AppRegister } from '../components/Authentication/Register';
import { AppLogin } from '../components/Authentication/Login';
import AppClient from '../components/Clients';
import { AppSample } from '../components/Samples/Create/RoutComp';
import { AppViewSample } from '../components/Samples/View/RoutComp';
import { AppResult } from '../components/Results/RoutComp';
import { AppResultPDF } from '../components/ResultPDF/RoutComp';
import AppProducts from '../components/Products';
import { AppUsers } from '../components/Users/RoutComp';
import AppMethods from '../components/Methods';

class Routes extends Component {
  render() {
    const { todaySamples, isLoggedIn } = this.props;
    return (
      <Fragment>
        <Router history={history}>
          <Fragment>
            <Header
              name="LACMA"
              todaySamples={todaySamples}
              isLoggedIn={isLoggedIn}
            />
            <PrivateRoute exact path="/" component={HomePage} />
            <Route path="/register" component={AppRegister} />
            <Route path="/login" component={AppLogin} />
            <PrivateRoute path="/clients" component={AppClient} />
            <PrivateRoute path="/resultsPdf" component={AppResult} />
            <PrivateRoute path="/results" component={AppResultPDF} />
            <PrivateRoute path="/samples" component={AppSample} />
            <PrivateRoute path="/viewsamples" component={AppViewSample} />
            <PrivateRoute path="/products" component={AppProducts} />
            <PrivateRoute path="/users" rols="coordinador" component={AppUsers} />
            <PrivateRoute path="/methods" component={AppMethods} />
          </Fragment>
        </Router>
      </Fragment>
    );
  }
}
const mapStateToProps = state => ({
  isLoggedIn: state.authentication.loggedIn,
  todaySamples: state.sample.todaySamples
});

export default connect(mapStateToProps)(Routes);
