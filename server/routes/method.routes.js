const express = require('express');
const router = express.Router();


const methodController = require('../controllers/methods.controller');

router.get('/', methodController.getAllMethods);
router.get('/:id', methodController.getByIdMethod);
router.post('/', methodController.createMethod);
router.put('/:id', methodController.updateMethod);
router.delete('/', methodController.deleteMethod);

module.exports = router;