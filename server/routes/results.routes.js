const express = require('express');

const router = express.Router();

const sampleController = require('../controllers/results.controller');

router.get('/', sampleController.getAllResults);
router.post('/', sampleController.createPdf);
router.delete('/:id', sampleController.deleteResult);

module.exports = router;
