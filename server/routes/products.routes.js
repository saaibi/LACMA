const express = require('express');
const router = express.Router();

const productsController = require('../controllers/products.controller');

router.get('/', productsController.getAllProducts);
router.get('/:id/productsmethods', productsController.getAllProductsAndMethods);
router.get('/:id', productsController.getByIdProduct);
router.get('/:id/methods', productsController.getByIdProductsAndMethods);
router.post('/:id/file', productsController.createProductAndMethodsFile);
router.post('/', productsController.createProduct);
router.put('/:id', productsController.updateProduct);
router.delete('/', productsController.deleteProduct);

module.exports = router;
