const express = require('express');

const router = express.Router();

const sampleController = require('../controllers/sample.controller');

router.get('/', sampleController.getAllSamples);
router.get('/:id', sampleController.getByIdSample);
router.post('/:id/analyst', sampleController.getAllSamplesByAnaly);
router.get('/:id/products', sampleController.getByIdSampleProducts);
router.get('/:id/client', sampleController.getByIdSampleClient);
router.get(
  '/:id/clientProductsMethods',
  sampleController.getByIdSampleClientProductsMethods
);
router.post('/', sampleController.createSample);
router.post('/:id/signature', sampleController.uploadSignature);
router.put('/:id', sampleController.updateSample);
router.delete('/', sampleController.deleteSample);

module.exports = router;
