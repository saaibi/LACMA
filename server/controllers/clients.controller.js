const Client = require('../models/client');

const clientController = {};

clientController.getAllClients = async (req, res) => {
  const clients = await Client.find().populate('samples', ['code']).exec();
  res.json(clients);
};

clientController.getByIdClient = async (req, res) => {
  const client = await Client.findById(req.params.id);
  res.json(client);
};

clientController.getByIdClientCredit = async (req, res) => {
  const client = await Client.findById(req.params.id)
    .populate('credit')
    .exec();
  res.json(client);
};

//  Credit.schema.pre
clientController.createClient = async (req, res) => {
  const client = new Client({...req.body});
  await client.save((err, client) => {
    if (err) return res.json({ error: err });
    res.json({ status: 'Cliente Guardado', client });
  });
};

clientController.updateClient = async (req, res) => {
  await Client.findByIdAndUpdate(
    req.params.id,
    {...req.body},
    { new: true },
    (err, client) => {
      if (err) return res.json({ error: err });
      res.json({ status: 'Cliente Actualizado', client });
    }
  );
};

// Pending Review
clientController.deleteClient = async (req, res) => {
  try {
    await Client.findByIdAndRemove(req.params.id, (err, client) => {
      if (err) return res.json({ error: err });
      res.json({ status: 'Cliente Eliminado' });
    });
  } catch (error) {
    const message = error.message || error;
    res.json({ error: message });
  }
};

module.exports = clientController;
