const Sample = require('../models/sample'),
  Client = require('../models/client'),
  dataModels = require('../utils/dataModels'),
  moment = require('moment'),
  _ = require('lodash');
// const multer = require('multer');

// const pdfModel = require('../models/pdf');

const sampleController = {};

// const storage = multer.diskStorage({
//   destination: (req, file, cb) => {
//     cb(null, 'app/public/Signature/');
//   },
//   filename: (req, file, cb) => {
//     if (req.params.id) {
//       cb(null, `${req.params.id}.jpg`);
//     }
//   }
// });

// const upload = multer({ storage: storage }).single('file');

sampleController.getAllSamples = async (req, res) => {
  const samples = await Sample.find();
  res.json(samples);
};

sampleController.getAllSamplesByAnaly = async (req, res) => {
  const { filter } = req.body;
  const todaySamples = await Sample.find({
    agreedDeliveryDate: {
      $gte: moment()
        .startOf('day')
        .toDate(),
      $lte: moment()
        .endOf('day')
        .toDate()
    }
  })
    .populate('coordinator', ['firstName', 'lastName'])
    .populate('responsibleSample', ['firstName', 'lastName'])
    .exec();
  if (filter === 'today') {
    //agreedDeliveryDate: 2020-02-02T02:00:00.000Z YYYY/MM/DD hh:mm:ss a
    return res.json({
      samples: todaySamples,
      todaySamples: todaySamples.length
    });
  }
  const samples = await Sample.find()
    .populate('coordinator', ['firstName', 'lastName'])
    .populate('responsibleSample', ['firstName', 'lastName'])
    .exec();
  res.json({ samples, todaySamples: todaySamples.length });
};

sampleController.getByIdSample = async (req, res) => {
  const sample = await Sample.findById(req.params.id);
  res.json(sample);
};

sampleController.getByIdSampleProducts = async (req, res) => {
  const sample = await Sample.findById(req.params.id)
    .populate('products')
    .exec();
  res.json(sample);
};

sampleController.getByIdSampleClient = async (req, res) => {
  const sample = await Sample.findById(req.params.id)
    .populate('client')
    .exec();
  res.json(sample);
};

sampleController.getByIdSampleClientProductsMethods = async (req, res) => {
  const sample = await Sample.findById(req.params.id)
    .populate({
      path: 'products',
      populate: { path: 'methods' }
    })
    .populate('client')
    .populate('responsibleSample', ['firstName', 'lastName'])
    .exec();
  res.json(sample);
};

const generateCode = async () => {
  const year = moment().format('YY');
  const rgx = new RegExp(`-${year}$`, 'g');
  const samples = await Sample.find(
    { code: { $regex: rgx } },
    { code: 1, _id: 0 }
  );
  const maxx = _.max(_.map(samples, 'code'));
  const spli = _.split(maxx, '-');
  const format = `0000${Number(spli[0]) + 1}`;
  const paddedSeq = format.substring(format.length - 4, format.length);
  return `${paddedSeq}-${year}`;
};

sampleController.createSample = async (req, res) => {
  const code = await generateCode();
  const newSample = dataModels({ ...req.body, code });
  await newSample.save((error, sample) => {
    if (error) return res.json({ error });
    const query = { _id: req.body.client };
    const update = { $push: { samples: sample._id } };
    Client.findOneAndUpdate(query, update, { new: true }, (err, client) => {
      if (err) return res.json({ error: err });
      res.json({ status: 'Muestra Guardada', sample });
    });
  });
};

sampleController.uploadSignature = async (req, res) => {
  if (!req.files) return res.json({ error: 'Ningun archivo fue cargado.' });
  const file = req.files.file;
  file.mv(`app/public/images/signatures/${req.params.id}.jpg`, err => {
    if (err) return res.json({ error: err });
    res.json({ status: 'Firma Subida' });
  });
};

sampleController.updateSample = async (req, res) => {
  try {
    const query = { _id: req.params.id };
    await Sample.findOneAndUpdate(
      query,
      { ...req.body },
      { new: true },
      async (err, sample) => {
        if (err) return res.json({ error: err });
        const resSample = await Sample.findById(req.params.id)
          .populate('coordinator', ['firstName', 'lastName'])
          .populate('responsibleSample', ['firstName', 'lastName'])
          .exec();
        res.json({ status: 'Muestra Actualizada', sample: resSample });
      }
    );
  } catch (error) {
    const message = error.message || error;
    res.json({ error: message });
  }
};

sampleController.deleteSample = async (req, res) => {
  try {
    await Sample.deleteMany({ _id: { $in: req.body } }, err => {
      if (err) return res.json({ error: err });
      res.json({
        status: req.body.length > 1 ? 'Muestras Eliminadas' : 'Muestra Eliminada'
      });
    });
  } catch (error) {
    const message = error.message || error;
    res.json({ error: message });
  }
};

module.exports = sampleController;
