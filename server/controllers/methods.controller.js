const Method = require('../models/methods');

const methodController = {};

methodController.getAllMethods = async (req, res) => {
  const methods = await Method.find();
  res.json(methods);
};

methodController.getByIdMethod = async (req, res) => {
  const method = await Method.findById(req.params.id);
  res.json(method);
};

methodController.createMethod = async (req, res) => {
  const method = new Method({ ...req.body });
  await method.save((err, method) => {
    if (err) return res.json({ error: err });
    res.json({ status: 'Metodo Guardado', method });
  });
};

methodController.updateMethod = async (req, res) => {
  const options = { new: true, runValidators: true };
  await Method.findByIdAndUpdate(
    req.params.id,
    { ...req.body },
    options,
    (err, method) => {
      if (err) return res.json({ error: err });
      res.json({ status: 'Metodo Actualizado', method });
    }
  );
};

methodController.deleteMethod = async (req, res) => {
  try {
    await Method.deleteMany({ _id: { $in: req.body } }, err => {
      if (err) return res.json({ error: err });
      res.json({
        status: req.body.length > 1 ? 'Metodos Eliminados' : 'Metodo Eliminado'
      });
    });
  } catch (error) {
    const message = error.message || error;
    res.json({ error: message });
  }
};

module.exports = methodController;
