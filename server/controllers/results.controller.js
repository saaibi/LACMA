const path = require('path'),
  Sample = require('../models/sample'),
  _ = require('lodash'),
  moment = require('moment');
const pdfMakePrinter = require('pdfmake/src/printer');

const resultController = {};

resultController.getAllResults = async (req, res) => {
  const samples = await Sample.find();
  res.json(samples);
};

const getSignature = img => {
  if (!img && !img.data && !img.contentType && !img.mimetype) return null;
  const buf = Buffer.from(img.data);
  const base64Analyst = buf.toString('base64');
  return `data:${img.contentType || img.mimetype};base64,${base64Analyst}`;
};

const createPdfBinary = async (req, callback) => {
  const logo = path.resolve('app/public/images', 'logo.jpg');
  const footer = path.resolve('app/public/images', 'footer.jpg');
  const logolacma = path.resolve('app/public/images', 'logolacma.jpg');
  const sigAnalyst = path.resolve(
    'app/public/images/signatures',
    'analista.jpg'
  );
  const sigCoordinator = path.resolve(
    'app/public/images/signatures',
    'coordinador.jpg'
  );

  const { nameA, nameC, conclusion, suggestions, sample } = req.body;
  const { files } = req;

  const resSample = await Sample.findById(sample)
    .populate({
      path: 'products',
      select: ['name', 'methods'],
      populate: { path: 'methods' }
    })
    .populate('client', [
      'phone',
      'fax',
      'address',
      'extension',
      'company',
      'nit',
      'contact',
      'email'
    ])
    .populate('responsibleSample', ['firstName', 'lastName'])
    .populate('analyst', ['firstName', 'lastName', 'img'])
    .populate('coordinator', ['firstName', 'lastName', 'img'])
    .exec();

  const { analyst, coordinator } = resSample;
  const dataAnaly1 = files && files.firmaA ? getSignature(files.firmaA) : null;
  const dataCoordi1 = files && files.firmaC ? getSignature(files.firmaC) : null;
  const dataAnaly = analyst && analyst.img ? getSignature(analyst.img) : null;
  const dataCoordi =
    coordinator && coordinator.img ? getSignature(coordinator.img) : null;

  const imageUrlAmalyst = dataAnaly1 || dataAnaly || sigAnalyst;
  const imageUrlCoordinator = dataCoordi1 || dataCoordi || sigCoordinator;
  const nameAmalyst =
    nameA ||
    `${analyst ? analyst.firstName || '' : ''} ${
      analyst ? analyst.lastName || '' : ''
    }`;
  const nameCoordinator =
    nameC ||
    `${coordinator ? coordinator.firstName || '' : ''} ${
      coordinator ? coordinator.lastName || '' : ''
    }`;

  const dataClient = resSample.client || {};
  const dataProduct = resSample.products || {};
  const dataMethods = _.map(dataProduct.methods || [{}], i => {
    return [
      { text: `${i.area || ''}` },
      { text: `${i.parameter || ''}` },
      { text: `${i.result || ''}` },
      { text: `${i.limitMin || ''}` },
      { text: `${i.limitMax || ''}` },
      { text: `${i.name || ''}` }
    ];
  });

  const receiptDate =
      resSample.receiptDate &&
      moment(resSample.receiptDate).format('DD/MM/YYYY HH:mm'),
    startDateAnalysis =
      resSample.startDateAnalysis &&
      moment(resSample.startDateAnalysis).format('DD/MM/YYYY HH:mm'),
    endDateAnalysis =
      resSample.endDateAnalysis &&
      moment(resSample.endDateAnalysis).format('DD/MM/YYYY HH:mm'),
    reportDate =
      resSample.reportDate && moment(resSample.reportDate).format('L'),
    agreedDeliveryDate =
      resSample.agreedDeliveryDate &&
      moment(resSample.agreedDeliveryDate).format('L'),
    takeSampleDate =
      resSample.takeSampleDate &&
      moment(resSample.takeSampleDate).format('DD/MM/YYYY HH:mm'),
    manufacturingDate =
      resSample.manufacturingDate &&
      moment(resSample.manufacturingDate).format('L'),
    expirationDate =
      resSample.expirationDate && moment(resSample.expirationDate).format('L');

  const responsibleSample =
    resSample.responsibleSample &&
    `${resSample.responsibleSample.firstName} ${resSample.responsibleSample.lastName}`;

  moment.locale('es');
  const nowMoment = moment(Date.now());
  const nowDate = `${nowMoment.date()} de ${nowMoment.format(
    'MMMM'
  )} de ${nowMoment.year()}`;

  const fontDescriptors = {
    Helvetica: {
      normal: 'Helvetica',
      bold: 'Helvetica-Bold',
      italics: 'Helvetica-Oblique',
      bolditalics: 'Helvetica-BoldOblique'
    }
  };

  const blacSpace = [
    { text: '', border: [false, false, false, false] },
    { text: '', border: [false, false, false, false] },
    { text: '', border: [false, false, false, false] },
    { text: '', border: [false, false, false, false] },
    { text: '', border: [false, false, false, false] },
    { text: '', border: [false, false, false, false] }
  ];

  const docDefinition = {
    pageSize: 'LETTER',
    pageMargins: 15,
    background: currentPage => ({
      text: `${currentPage > 1 ? '' : currentPage}`,
      alignment: 'right',
      margin: [0, 163, 65, 0]
    }),
    images: {
      logo: logo,
      logolacma: logolacma,
      footer: footer,
      sigAnalyst: imageUrlAmalyst,
      sigCoordinator: imageUrlCoordinator
    },
    content: [
      {
        image: 'logo',
        alignment: 'center',
        marginRight: 35,
        fit: [700, 700]
      },
      {
        style: 'tableExample',
        table: {
          widths: ['20%', '15%', '15%', '15%', '15%', '20%'],
          body: [
            // Header
            [
              {
                image: 'logolacma',
                alignment: 'center',
                fit: [100, 100]
              },
              {
                text: 'INFORME DE RESULTADOS',
                colSpan: 5,
                style: 'header'
              },
              '',
              '',
              '',
              ''
            ],
            [
              { text: 'CODIGO: ' },
              { text: resSample.code || '\n' },
              { text: 'VERSIÓN: ' },
              { text: resSample.version || '' },
              { text: 'PÁGINA: ' },
              { text: '' }
            ],
            blacSpace,
            [
              {
                text: '',
                colSpan: 4,
                border: [false, false, false, false]
              },
              '',
              '',
              '',
              { text: 'REFERENCIA', border: [false, false, false, false] },
              { text: '' }
            ],
            blacSpace, // Cliente
            [
              {
                text: `Empresa Solicitante\n${dataClient.company || '\n'}`,
                colSpan: 2
              },
              '',
              { text: `Nit\n${dataClient.nit || ''}` },
              { text: `Contacto\n${dataClient.contact || ''}`, colSpan: 3 },
              '',
              ''
            ],
            [
              { text: `Dirección\n${dataClient.address || ''}`, colSpan: 2 },
              '',
              { text: `Teléfono\n${dataClient.phone || ''}` },
              { text: `Fax\n${dataClient.fax || ''}` },
              { text: `E-mail\n${dataClient.email || ''}`, colSpan: 2 },
              ''
            ],
            // Fechas
            [
              { text: `Fecha y Hora de recibo\n\n${receiptDate || ''}` },
              {
                text: `Fecha y Hora inicio de Análisis\n${startDateAnalysis ||
                  ''}`
              },
              {
                text: `Fecha y Hora final de Análisis\n${endDateAnalysis || ''}`
              },
              { text: `Fecha elaboración de informe\n${reportDate || ''}` },
              {
                text: `Fecha pactada de entrega\n\n${agreedDeliveryDate || ''}`,
                colSpan: 2
              },
              ''
            ],
            [
              { text: `Fecha y Hora de Toma\n${takeSampleDate || ''}` },
              {
                text: `Tipo de muestra\n${resSample.typeSample || ''}`,
                colSpan: 5,
                rowSpan: 2
              },
              '',
              '',
              '',
              ''
            ],
            /// Muestra
            [
              {
                text: `Responsable Toma de Muestra:\n${responsibleSample || ''}`
              },
              '',
              '',
              '',
              '',
              ''
            ],
            [
              { text: `Lote\n${resSample.lote || ''}` },
              { text: `Fecha de Fabricación\n${manufacturingDate || ''}` },
              { text: `Fecha de Vencimiento\n${expirationDate || ''}` },
              {
                text: `Cantidad de muestra\n${resSample.quantity || ''}`,
                colSpan: 3
              },
              '',
              ''
            ],
            [
              {
                text: `Tipo de Envase-Empaque\n${resSample.typeContainer ||
                  ''}`,
                colSpan: 2
              },
              '',
              {
                text: `Condiciones de Llegada al laboratorio Temperatura ºC\n${resSample.temperature ||
                  ''}`,
                colSpan: 4
              },
              '',
              '',
              ''
            ],
            blacSpace, // Caracteristicas Muestra
            [
              {
                text: 'CARACTERÍSTICAS ORGANOLÉPTICAS',
                border: [false, false, false, false],
                colSpan: 6
              },
              '',
              '',
              '',
              '',
              ''
            ],
            blacSpace,
            [
              { text: 'ASPECTO' },
              { text: `${resSample.appearance || ''}`, colSpan: 5 },
              '',
              '',
              '',
              ''
            ],
            [
              { text: 'COLOR' },
              { text: `${resSample.color || ''}`, colSpan: 5 },
              '',
              '',
              '',
              ''
            ],
            [
              { text: 'OLOR' },
              { text: `${resSample.smell}`, colSpan: 5 },
              '',
              '',
              '',
              ''
            ],
            blacSpace, // Producto
            [
              { text: 'PRODUCTO' },
              { text: `${dataProduct.name || ''}`, colSpan: 3 },
              '',
              '',
              { text: '', border: [false, false, false, false] },
              { text: '', border: [false, false, false, false] }
            ],
            blacSpace,
            [
              { text: 'AREA' },
              { text: 'PARÁMETRO' },
              { text: 'RESULTADO' },
              { text: 'LIMITE MINIMO' },
              { text: 'LIMITE MAXIMO' },
              { text: 'METODO' }
            ],
            [
              {
                table: {
                  widths: ['20%', '15%', '15%', '15%', '15%', '20%'],
                  body: dataMethods
                },
                margin: [-5, -3],
                colSpan: 6,
                border: [false, false, false, false]
              },
              '',
              '',
              '',
              '',
              ''
            ],
            blacSpace, // Conclusion y Sugerencias
            blacSpace,
            [
              { text: '', border: [false, false, false, false] },
              { text: 'CONCLUSION', style: 'conSuge', colSpan: 4 },
              '',
              '',
              '',
              { text: '', border: [false, false, false, false] }
            ],
            [
              { text: '\n', border: [false, false, false, false] },
              { text: `${conclusion}`, colSpan: 4, rowSpan: 2 },
              '',
              '',
              '',
              { text: '', border: [false, false, false, false] }
            ],
            [
              { text: '\n', border: [false, false, false, false] },
              '',
              '',
              '',
              '',
              { text: '', border: [false, false, false, false] }
            ],
            suggestions
              ? [
                  { text: '', border: [false, false, false, false] },
                  { text: 'SUGERENCIA', style: 'conSuge', colSpan: 4 },
                  '',
                  '',
                  '',
                  { text: '', border: [false, false, false, false] }
                ]
              : blacSpace,
            suggestions
              ? [
                  { text: '\n', border: [false, false, false, false] },
                  { text: `${suggestions || ''}`, colSpan: 4, rowSpan: 2 },
                  '',
                  '',
                  '',
                  { text: '', border: [false, false, false, false] }
                ]
              : blacSpace,
            suggestions
              ? [
                  { text: '\n', border: [false, false, false, false] },
                  '',
                  '',
                  '',
                  '',
                  { text: '', border: [false, false, false, false] }
                ]
              : blacSpace,
            blacSpace, // firmas
            [
              {
                image: 'sigAnalyst',
                alignment: 'center',
                colSpan: 2,
                width: 200,
                height: 60,
                border: [false, false, false, false]
              },
              { text: '' },
              { text: '', border: [false, false, false, false] },
              { text: '', border: [false, false, false, false] },
              {
                image: 'sigCoordinator',
                alignment: 'center',
                colSpan: 2,
                width: 200,
                height: 60,
                border: [false, false, false, false]
              },
              { text: '' }
            ],
            [
              {
                text: `${nameAmalyst}\nAnalista (Microbiología ó Fisicoquímico)\nRealiza`,
                colSpan: 2,
                alignment: 'center',
                border: [false, false, false, false]
              },
              { text: '' },
              { text: '', border: [false, false, false, false] },
              { text: '', border: [false, false, false, false] },
              {
                text: `${nameCoordinator}\nCoordinadora LACMA\nAprueba`,
                colSpan: 2,
                border: [false, false, false, false],
                alignment: 'center'
              },
              { text: '' }
            ],
            [
              { text: '', border: [false, false, false, false] },
              { text: '', border: [false, false, false, false] },
              {
                text: `Medellín, ${nowDate}\n-------- Fin de reporte ----`,
                alignment: 'center',
                colSpan: 2,
                border: [false, false, false, false]
              },
              { text: '', border: [false, false, false, false] },
              { text: '', border: [false, false, false, false] },
              { text: '', border: [false, false, false, false] }
            ],
            [
              { text: '', border: [false, false, false, false] },
              { text: '', border: [false, false, false, false] },
              { text: '', border: [false, false, false, false] },
              { text: '', border: [false, false, false, false] },
              { text: '', border: [false, false, false, false] },
              {
                text: 'FECHA PUBLICACION\n27 de Septiembre de 2013',
                fontSize: 6,
                border: [false, false, false, false]
              }
            ],
            blacSpace
          ]
        },
        layout: {
          fillColor: (rowIndex, node, columnIndex) => {
            if (rowIndex === 21) {
              return '#CCCCCC';
            } else if (
              rowIndex === 19 &&
              columnIndex != 5 &&
              columnIndex != 4
            ) {
              return '#CCCCCC';
            }
          }
        }
      },
      {
        image: 'footer',
        alignment: 'center',
        width: 582
      }
    ],
    styles: {
      header: {
        fontSize: 18,
        bold: true,
        alignment: 'center',
        marginTop: 15
      },
      subheader: {
        fontSize: 16,
        bold: true,
        alignment: 'center',
        margin: [0, 5, 0, 5]
      },
      conSuge: {
        // fontSize: 16,
        bold: true,
        marginTop: 2,
        alignment: 'center'
      },
      text: {
        marginTop: 2
      },
      bNone: {
        border: [false, false, false, false]
      },
      tableExample: {
        fontSize: 9
        // margin: [0, 5, 0, 5]
      }
    },
    defaultStyle: {
      font: 'Helvetica'
    }
  };

  const printer = new pdfMakePrinter(fontDescriptors);
  const doc = printer.createPdfKitDocument(docDefinition);
  const chunks = [];
  doc.on('data', chunk => {
    chunks.push(chunk);
  });
  doc.on('end', () => {
    const result = Buffer.concat(chunks);
    const base64String = result.toString('base64');
    const data = `data:application/pdf;base64,${base64String}`;
    callback(data);
  });
  doc.end();
};

resultController.createPdf = async (req, res) => {
  try {
    createPdfBinary(req, binary => {
      res.contentType('application/pdf');
      res.send({ status: 'PDF Creado', pdf: binary });
    });
  } catch (error) {
    const message = error.message || error;
    res.json({ error: message });
  }
};

resultController.updateSample = async (req, res) => {
  try {
  } catch (error) {
    const message = error.message || error;
    res.json({ error: message });
  }
};

resultController.deleteResult = async (req, res) => {
  try {
    await Sample.findByIdAndRemove(req.params.id, (err, sample) => {
      if (err) return res.json({ error: err });
      res.json({ status: 'Muestra Eliminada' });
    });
  } catch (error) {
    const message = error.message || error;
    res.json({ error: message });
  }
};

module.exports = resultController;
