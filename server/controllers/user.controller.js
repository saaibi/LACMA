const User = require('../models/user'),
  fs = require('fs');

const userController = {};

userController.getAllUsers = async (req, res) => {
  const user = await User.find();
  res.json(user);
};

userController.getByIdUser = async (req, res) => {
  const user = await User.findById(req.params.id);
  res.json(user);
};

userController.getByIdUserAccount = async (req, res) => {
  const user = await User.findById(req.params.id)
    .populate('accounts')
    .exec();
  res.json(user);
};

userController.createUser = async (req, res) => {
  const user = new User({ ...req.body });
  await user.save((err, user) => {
    if (err) return res.json({ error: err });
    res.json({ status: 'Usuario Guardado', user });
  });
};

userController.createUserImg = async (req, res, next) => {
  const user = new User({ ...req.body });
  if (req.files && req.files.signature) {
    const { signature } = req.files;
    user.img.data = signature.data;
    user.img.contentType = signature.mimetype;
  }
  await user.save((err, doc) => {
    if (err) return res.json({ error: err });
    // res.contentType(doc.img.contentType);
    // res.send(doc.img.data);
    // res.send(doc);
    res.json({ status: 'Usuario Guardado', user: doc });
  });
};

userController.login = async (req, res) => {
  const { username, password } = req.body;
  await User.findOne({ username, password }, (err, user) => {
    if (err) return res.json({ error: err });
    if (user) res.json({ status: 'Usuario Logeado', user });
    else res.status(500).send('Email or password is incorrect!');
    //    throw res.json({ status: "Email or password is incorrect" });
  });
};

userController.updateUser = async (req, res) => {
  await User.findByIdAndUpdate(
    req.params.id,
    { ...req.body },
    { new: true },
    (err, user) => {
      if (err) return res.json({ error: err });
      res.json({ status: 'Usuario Actualizado', user });
    }
  );
};

userController.deleteUser = async (req, res) => {
  try {
    await User.findByIdAndRemove(req.params.id, (err, product) => {
      if (err) return res.json({ error: err });
      res.json({ status: 'Usuario Eliminado' });
    });
  } catch (error) {
    const message = error.message || error;
    res.json({ error: message });
  }
};

module.exports = userController;
