const XLSX = require('node-xlsx'),
  fs = require('fs'),
  _ = require('lodash');

const Product = require('../models/products');
const Method = require('../models/methods');

const productController = {};

productController.getAllProducts = async (req, res) => {
  const products = await Product.find();
  res.json(products);
};

productController.getAllProductsAndMethods = async (req, res) => {
  const products = await Product.find()
    .populate('methods')
    .exec();
  res.json(products);
};

productController.getByIdProductsAndMethods = async (req, res) => {
  const products = await Product.findById(req.params.id)
    .populate('methods')
    .exec();
  res.json(products);
};

productController.getByIdProduct = async (req, res) => {
  const product = await Product.findById(req.params.id);
  res.json(product);
};

const format = word => {
  return word
    ? word
        .toString()
        // .replace(/\n/g, " - ")
        // .replace(/\r/g, "-")
        .replace(/\s\s+/g, ' ')
    : '';
};

// const file = `${__dirname}/docs/DB2.xlsx`;
productController.createProductAndMethodsFile = async (req, res) => {
  if (!req.files) return res.json({ error: 'Ningun archivo fue cargado.' });
  const { fileName } = req.files;
  const spliter = _.split(fileName.name, '.');
  if (spliter[1] !== 'xlsx')
    return res.json({ error: 'El archivo no es excel.' });
  if (!fileName.data) return res.json({ error: 'Archivo sin data' });
  const wb = XLSX.parse(fileName.data);
  const tabDB = _.filter(wb, { name: 'Base de datos' })[0];
  if (!tabDB) return res.json({ error: 'No hay Base de datos' });
  const data = _.filter(tabDB.data, item => !_.isEmpty(item));
  const methods = _.map(data, (dat, idx) => ({
    name: format(dat[6]) || `Metodo ${idx + 1}`,
    area: dat[0],
    product: format(dat[1]),
    parameter: format(dat[2]),
    result: format(dat[3]),
    limitMin: format(dat[4]),
    limitMax: format(dat[5]),
    description: ''
  }));
  const products = [];
  _.each(methods, item => {
    if (
      !item.product ||
      _.lowerCase(item.product) === 'producto' ||
      item.product === '.'
    ) {
      return;
    }
    const product = _.find(products, { name: item.product });
    if (product) {
      delete item.product;
      product.methods.push(item);
    } else {
      products.push({
        name: item.product || `Producto ${products.length + 1}`,
        code: `Codigo ${products.length + 1}`,
        methods: [item]
      });
    }
  });

  if (req.body.save === 'true') {
    req.body = products;
    productController.createProductAndMethods(req, res);
  }
  res.json({ status: 'Productos Guardados', size: products.length, products });
};

productController.createProductAndMethods = async (req, res) => {
  _.each(req.body, data => {
    if (!_.isEmpty(data.methods)) {
      const methods = [];
      _.each(data.methods, (item, idx, array) => {
        const method = new Method({ ...item });
        method.save((err, method) => {
          if (err) {
            console.log(err);
          } else {
            methods.push(method._id);
            if (methods.length === array.length) {
              const productSave = {
                name: data.name,
                code: data.code,
                methods
              };
              const product = new Product(productSave);
              product.save((err, product) => {
                if (err) {
                  console.log(err);
                }
              });
            }
          }
        });
      });
    }
  });
};

productController.createProduct = async (req, res) => {
  const product = new Product({ ...req.body });
  await product.save((err, product) => {
    if (err) return res.json({ error: err });
    res.json({ status: 'Producto Guardado', product });
  });
};

// query = { _id: req.params.id };
// console.log(moment(credit.startDate).format("DD/MM/YYYY HH:mm"))
productController.updateProduct = async (req, res) => {
  const options = { new: true, runValidators: true };
  await Product.findByIdAndUpdate(
    req.params.id,
    { ...req.body },
    options,
    async (err, product) => {
      if (err) return res.json({ error: err });
      const resProduct = await Product.findById(req.params.id)
        .populate('methods')
        .exec();
      res.json({ status: 'Producto Actualizado', product: resProduct });
    }
  );
};

productController.deleteProduct = async (req, res) => {
  try {
    await Product.deleteMany({ _id: { $in: req.body } }, err => {
      if (err) return res.json({ error: err });
      res.json({
        status: req.body.length > 1 ? 'Productos Eliminados' : 'Producto Eliminado'
      });
    });
  } catch (error) {
    const message = error.message || error;
    res.json({ error: message });
  }
};

module.exports = productController;
