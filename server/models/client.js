const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');
const { Schema } = mongoose;

// Create Schema
const clientSchema = new Schema(
  {
    company: { type: String, required: true },
    nit: { type: String, required: true, unique: true },
    contact: { type: String, required: true },
    email: { type: String, required: true, unique: true },
    phone: { type: String, default: '' },
    fax: { type: String, default: '' },
    address: { type: String, default: '' },
    extension: { type: String, default: '' },
    samples: [
      {
        type: Schema.Types.ObjectId,
        ref: 'Sample',
        default: []
      }
    ]
  },
  {
    versionKey: false,
    timestamps: {
      createdAt: 'created_at',
      updatedAt: 'updated_at'
    }
  }
);

clientSchema.plugin(uniqueValidator);

module.exports = mongoose.model('Client', clientSchema);
