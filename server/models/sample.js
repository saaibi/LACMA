const mongoose = require('mongoose'),
  uniqueValidator = require('mongoose-unique-validator');
const { Schema } = mongoose;

// Create Schema
const sampleSchema = new Schema(
  {
    code: { type: String, unique: true },
    version: { type: String, default: '' },
    typeSample: { type: String, default: '' },
    quantity: { type: String, default: '' },
    lote: { type: String, default: '' },
    typeContainer: { type: String, default: '' },
    color: { type: String, default: '' },
    smell: { type: String, default: '' },
    appearance: { type: String, default: '' },
    temperature: { type: String, default: '' },
    manufacturingDate: { type: Date },
    expirationDate: { type: Date },
    receiptDate: { type: Date, require: true },
    startDateAnalysis: { type: Date },
    endDateAnalysis: { type: Date },
    reportDate: { type: Date },
    agreedDeliveryDate: { type: Date, require: true },
    deliveryDate: { type: Date },
    takeSampleDate: { type: Date },
    products: {
      type: Schema.Types.ObjectId,
      ref: 'Product'
    },
    client: {
      type: Schema.Types.ObjectId,
      ref: 'Client'
    },
    analyst: {
      type: Schema.Types.ObjectId,
      ref: 'User'
    },
    coordinator: {
      type: Schema.Types.ObjectId,
      ref: 'User'
    },
    responsibleSample: {
      type: Schema.Types.ObjectId,
      ref: 'User'
    }
    // products: [
    //   {
    //     type: Schema.Types.ObjectId,
    //     ref: 'Product',
    //     default: []
    //   }
    // ]
  },
  {
    versionKey: false,
    timestamps: {
      createdAt: 'created_at',
      updatedAt: 'updated_at'
    }
  }
);

sampleSchema.plugin(uniqueValidator);

module.exports = mongoose.model('Sample', sampleSchema);
