const mongoose = require('mongoose');
const { Schema } = mongoose;

const methodSchema = new Schema(
  {
    name: { type: String, required: [true, 'Nombre del Metodo es requerido'] },
    area: { type: String, default: '' },
    parameter: { type: String, default: '' },
    result: { type: String, default: '' },
    limitMin: { type: String, default: '' },
    limitMax: { type: String, default: '' },
    description: { type: String, default: '' },
  },
  {
    versionKey: false,
    timestamps: {
      createdAt: 'created_at',
      updatedAt: 'updated_at'
    }
  }
);

module.exports = mongoose.model('Method', methodSchema);