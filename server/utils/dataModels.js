const Sample = require('../models/sample');

const dataModel = data => {
  const {
    manufacturingDate,
    expirationDate,
    receiptDate,
    startDateAnalysis,
    endDateAnalysis,
    reportDate,
    agreedDeliveryDate,
    deliveryDate,
    takeSampleDate
  } = data;

  const createSample = {
    ...data,
    receiptDate: new Date(receiptDate),
    agreedDeliveryDate: new Date(agreedDeliveryDate),
    manufacturingDate: manufacturingDate ? new Date(manufacturingDate) : null,
    expirationDate: expirationDate ? new Date(expirationDate) : null,
    startDateAnalysis: startDateAnalysis ? new Date(startDateAnalysis) : null,
    endDateAnalysis: endDateAnalysis ? new Date(endDateAnalysis) : null,
    reportDate: reportDate ? new Date(reportDate) : null,
    deliveryDate: deliveryDate ? new Date(deliveryDate) : null,
    takeSampleDate: takeSampleDate ? new Date(takeSampleDate) : null
  };

  const sample = new Sample(createSample);

  return sample;
};

module.exports = dataModel;
